package mk.ukim.finki.attendance.repository;

import mk.ukim.finki.attendance.model.Professor;
import mk.ukim.finki.attendance.model.ProfessorClassSession;
import mk.ukim.finki.attendance.model.ScheduledClassSession;
import mk.ukim.finki.attendance.model.dto.ProfessorAttendanceSingleExportDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface ProfessorClassSessionRepository extends JpaRepository<ProfessorClassSession,Long>, JpaSpecificationExecutor {

    boolean existsByDateAndProfessorAndScheduledClassSession(LocalDate date, Professor professor, ScheduledClassSession scheduledClassSession);

    @Query("SELECT new mk.ukim.finki.attendance.model.dto.ProfessorAttendanceSingleExportDTO(p.name, js.abbreviation, cu.studyYear, js.name, cu.english, cu.semester.code, scs.room.name, scs.classType, pcs.date, scs.startTime, scs.endTime, pcs.professorArrivalTime, s.name, s.lastName, s.studentIndex,pcs.attendanceToken,sa.arrivalTime) " +
            "FROM ProfessorClassSession pcs " +
            "JOIN pcs.professor p " +
            "JOIN pcs.scheduledClassSession scs " +
            "JOIN scs.course cu " +
            "JOIN JoinedSubject js ON cu.joinedSubject.abbreviation = js.abbreviation " +
            "INNER JOIN StudentAttendance sa ON sa.professorClassSession.id = pcs.id " +
            "INNER JOIN Student s ON sa.student.studentIndex = s.studentIndex " +
            "WHERE pcs.id = :professorClassSessionId " +
            "GROUP BY p.name, js.abbreviation, cu.studyYear, js.name, cu.english, cu.semester.code, scs.room.name, scs.classType, pcs.date, scs.startTime, scs.endTime, pcs.professorArrivalTime, s.name, s.lastName, s.studentIndex,pcs.attendanceToken,sa.arrivalTime")
    List<ProfessorAttendanceSingleExportDTO> findForSingleProfessorClassSession(@Param("professorClassSessionId") Long id);



}
