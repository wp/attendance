package mk.ukim.finki.attendance.repository;

import mk.ukim.finki.attendance.model.Holiday;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface HolidayRepository extends JpaRepository<Holiday, LocalDate>, JpaSpecificationExecutor {

    @Query("SELECT h FROM Holiday h " +
            "WHERE " +
            "(:year is null or YEAR(h.date) = :year) AND " +
            "(:month is null or MONTH(h.date) = :month)")
    List<Holiday> findAllByYearAndMonth(@Param("year") Integer year, @Param("month") Integer month);
}
