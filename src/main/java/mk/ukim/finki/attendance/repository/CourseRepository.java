package mk.ukim.finki.attendance.repository;

import mk.ukim.finki.attendance.model.Course;
import mk.ukim.finki.attendance.model.Semester;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Optional;
import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long>, JpaSpecificationExecutor {
    List<Course> findBySemester(Semester semester);
    @Query("SELECT c FROM Course c " +
            "WHERE " +
            "(:studyYear is null OR c.studyYear = :studyYear) AND " +
            "(:semester is null OR c.semester.code ILIKE %:semester%) AND" +
            "(:english is null OR c.english = :english) AND" +
            "(:joinedSubject is null OR :joinedSubject = '' OR c.joinedSubject.abbreviation ILIKE %:joinedSubject%) AND" +
            "(:professor is null OR c.professor.id ILIKE %:professor% OR c.professors ILIKE %:professor%) AND" +
            "(:assistant is null OR c.assistant.id ILIKE %:assistant OR c.assistants ILIKE %:assistant%)")
    Page<Course> findAllFiltered(
            @Param("studyYear")  Short studyYear, @Param("semester") String semesterId, @Param("english") Boolean english,
            @Param("joinedSubject") String joinedSubjectId,
            @Param("professor") String professorId, @Param("assistant") String assistantId,
             Pageable pageable
    );

    @Query("SELECT c FROM Course c WHERE c.lastNameRegex = :lastNameRegex")
    Optional<Course> findByLastNameRegex(@Param("lastNameRegex") String lastNameRegex);

}
