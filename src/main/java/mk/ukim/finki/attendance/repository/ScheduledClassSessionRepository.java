package mk.ukim.finki.attendance.repository;

import mk.ukim.finki.attendance.model.ScheduledClassSession;
import mk.ukim.finki.attendance.model.Semester;
import mk.ukim.finki.attendance.model.enumeration.ClassType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.DayOfWeek;
import java.util.List;

@Repository
public interface ScheduledClassSessionRepository extends JpaRepository<ScheduledClassSession, Long>, JpaSpecificationExecutor {

    @Query("SELECT scs FROM ScheduledClassSession scs " +
            "WHERE " +
            "(:dayOfWeek is null or scs.dayOfWeek = :dayOfWeek) AND " +
            "(:roomName is null or scs.room.name LIKE :roomName) AND" +
            "(:classType is null or scs.classType = :classType) AND" +
            "(:courseId is null or scs.course.id = :courseId)"
    )
    Page<ScheduledClassSession> findFiltered(
            @Param("dayOfWeek") DayOfWeek dayOfWeek,
            @Param("roomName") String roomName,
            @Param("classType") ClassType classType,
            @Param("courseId") Long courseId,
            Pageable pageable
    );
    @Query("SELECT scs FROM ScheduledClassSession scs " +
            "JOIN scs.course c " +
            "JOIN c.semester s " +
            "WHERE s = :semester")
    List<ScheduledClassSession> findBySemester(@Param("semester") Semester semester);
}
