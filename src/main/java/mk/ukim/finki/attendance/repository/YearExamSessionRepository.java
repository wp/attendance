package mk.ukim.finki.attendance.repository;

import mk.ukim.finki.attendance.model.YearExamSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;

@Repository
public interface YearExamSessionRepository extends JpaRepository<YearExamSession, String> {
    @Query("SELECT CASE WHEN COUNT(ye) > 0 THEN TRUE ELSE FALSE END " +
            "FROM YearExamSession ye " +
            "WHERE :date BETWEEN ye.startDate AND ye.endDate")
    boolean existsByDateBetweenStartDateAndEndDate(@Param("date") LocalDate date);
}