package mk.ukim.finki.attendance.repository;

import mk.ukim.finki.attendance.model.JoinedSubject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface JoinedSubjectRepository extends JpaRepository<JoinedSubject, String>, JpaSpecificationExecutor {}
