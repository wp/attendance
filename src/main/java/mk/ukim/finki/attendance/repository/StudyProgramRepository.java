package mk.ukim.finki.attendance.repository;

import mk.ukim.finki.attendance.model.StudyProgram;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface StudyProgramRepository extends JpaRepository<StudyProgram, String>, JpaSpecificationExecutor {
    Optional<StudyProgram> findByCode(String code);

    boolean existsByCode(String code);

    StudyProgram getReferenceByCode(String studyProgramCode);
}
