package mk.ukim.finki.attendance.repository;

import mk.ukim.finki.attendance.model.ProfessorClassSession;
import mk.ukim.finki.attendance.model.StudentAttendance;
import mk.ukim.finki.attendance.model.dto.StudentAttendanceDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface StudentAttendanceRepository extends JpaRepository <StudentAttendance, Long>, JpaSpecificationExecutor {
    @Override
    Optional<StudentAttendance> findById(Long id);

    @Query("SELECT new mk.ukim.finki.attendance.model.dto.StudentAttendanceDTO(" +
            " s.studentIndex, s.name , js.abbreviation,js.name,c.english,sem.code,scs.room.name,scs.classType, sem.year ,scs.startTime,scs.endTime " +
            ", sa.arrivalTime)" +
            "FROM StudentAttendance sa " +
            "JOIN ProfessorClassSession pcs ON sa.professorClassSession.id = pcs.id " +
            "JOIN ScheduledClassSession scs ON pcs.scheduledClassSession.id = scs.id " +
            "JOIN Course c ON scs.course.id = c.id " +
            "JOIN Semester sem ON c.semester.code =  sem.code " +
            "JOIN Student s ON sa.student.studentIndex = s.studentIndex " +
            "JOIN JoinedSubject js ON c.joinedSubject.abbreviation = js.abbreviation " +
            "JOIN Subject sub ON js.mainSubject.id = sub.id " +
            "WHERE s.studentIndex = :studentIndex AND (sem.code = :semesterCode OR :semesterCode = '')" +
            " AND pcs.attendanceToken != '' AND pcs.attendanceToken IS NOT NULL" +
            " AND DATE(sa.arrivalTime) = DATE(pcs.professorArrivalTime) " +
            "AND TO_CHAR(sa.arrivalTime, 'HH24:MI:SS') BETWEEN TO_CHAR(scs.startTime, 'HH24:MI:SS') AND TO_CHAR(scs.endTime, 'HH24:MI:SS')"
    )
    List<StudentAttendanceDTO> findAttendanceForStudentInSemester(@Param("studentIndex") String studentIndex,
                                                                  @Param("semesterCode") String semesterCode);

    List<StudentAttendance> findAllByProfessorClassSession(ProfessorClassSession session);
}
