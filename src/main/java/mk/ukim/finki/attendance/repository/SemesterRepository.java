package mk.ukim.finki.attendance.repository;

import mk.ukim.finki.attendance.model.Semester;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface SemesterRepository extends JpaRepository<Semester, String>, JpaSpecificationExecutor {

    @Query(value = "SELECT * FROM Semester s WHERE s.state IN (" +
            "'STARTED', " +
            "'SCHEDULE_PREPARATION', " +
            "'TEACHER_SUBJECT_ALLOCATION', " +
            "'STUDENTS_ENROLLMENT', " +
            "'DATA_COLLECTION')" +
            "LIMIT 1", nativeQuery = true)
    Semester findActiveSemester();
}
