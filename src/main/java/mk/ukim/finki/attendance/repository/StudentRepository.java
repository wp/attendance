package mk.ukim.finki.attendance.repository;

import mk.ukim.finki.attendance.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, String>, JpaSpecificationExecutor {
    Optional<Student> findByStudentIndex(String index);

    boolean existsByStudentIndex(String index);

    boolean existsByEmail(String email);
}
