package mk.ukim.finki.attendance.repository;

import mk.ukim.finki.attendance.model.Room;
import mk.ukim.finki.attendance.model.enumeration.RoomType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoomRepository extends JpaRepository<Room, String>, JpaSpecificationExecutor {

    @Query("SELECT r FROM Room r WHERE " +
            "(:name IS NULL OR r.name ILIKE %:name%) AND " +
            "(:roomType IS NULL OR r.roomType = :roomType) AND " +
            "(:capacity IS NULL OR r.capacity >= :capacity)")
    Page<Room> findRooms(@Param("name") String name,
                         @Param("roomType") RoomType roomType,
                         @Param("capacity") Long capacity,
                         Pageable pageable);

    Optional<Room> findByName(String roomName);
}


