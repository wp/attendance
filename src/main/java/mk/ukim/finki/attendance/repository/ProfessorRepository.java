package mk.ukim.finki.attendance.repository;

import mk.ukim.finki.attendance.model.Professor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface ProfessorRepository extends JpaRepository<Professor, String>, JpaSpecificationExecutor {
    Optional<Professor> findProfessorByEmail(String email);


}
