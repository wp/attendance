package mk.ukim.finki.attendance.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.DayOfWeek;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"startTime", "endTime", "dayOfWeek", "roomId", "classTypeName", "courseId"})
public class ScheduledClassSessionExportDTO {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    private LocalTime startTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    private LocalTime endTime;

    private DayOfWeek dayOfWeek;

    private String roomId;

    private String classTypeName;

    private String courseId;

}
