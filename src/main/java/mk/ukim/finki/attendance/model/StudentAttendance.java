package mk.ukim.finki.attendance.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonPropertyOrder({"studentIndex", "classSession", "arrivalTime"})
public class StudentAttendance {

    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    @ManyToOne
    @JoinColumn(name = "student_student_index")
    @JsonIgnore
    private Student student;

    @ManyToOne
    @JoinColumn(name = "professor_class_session_id")
    @JsonIgnore
    private ProfessorClassSession professorClassSession;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime arrivalTime;

    @Transient
    private String studentIndex;

    @Transient
    private Long classSession;

    public StudentAttendance(Student student, ProfessorClassSession professorClassSession, LocalDateTime arrivalTime) {
        this.student = student;
        this.professorClassSession = professorClassSession;
        this.arrivalTime = arrivalTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        StudentAttendance studentAttendance = (StudentAttendance) o;
        return getId() != null && Objects.equals(getId(), studentAttendance.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public String toString() {
        return arrivalTime.toString();
    }

}
