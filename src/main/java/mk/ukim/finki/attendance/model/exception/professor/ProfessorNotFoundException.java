package mk.ukim.finki.attendance.model.exception.professor;

public class ProfessorNotFoundException extends RuntimeException{
    public ProfessorNotFoundException(String id) {
        super("Невалиден професор со ID:  "+ id);
    }
}
