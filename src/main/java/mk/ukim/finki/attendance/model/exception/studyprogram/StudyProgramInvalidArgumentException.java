package mk.ukim.finki.attendance.model.exception.studyprogram;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.UNPROCESSABLE_ENTITY, reason="Bad arguments provided for StudyProgram")
public class StudyProgramInvalidArgumentException extends StudyProgramCrudException {
    public StudyProgramInvalidArgumentException(String message) {
        super(message);
    }
}
