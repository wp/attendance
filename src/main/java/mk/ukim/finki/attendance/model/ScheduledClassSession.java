package mk.ukim.finki.attendance.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mk.ukim.finki.attendance.model.enumeration.ClassType;

import java.time.DayOfWeek;
import java.time.LocalTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"startTime", "endTime", "dayOfTheWeekName", "roomId", "classTypeName", "courseId"})
public class ScheduledClassSession {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "start_time")
    private LocalTime startTime;

    @Column(name = "end_time")
    private LocalTime endTime;

    @Column(name = "day_of_week")
    private DayOfWeek dayOfWeek;

    @ManyToOne
    @JoinColumn(name = "room_name")
    @JsonIgnore
    private Room room;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "type")
    @JsonIgnore
    private ClassType classType;

    @ManyToOne
    @JsonIgnore
    private Course course;

    @Transient
    private String roomId;

    @Transient
    private String classTypeName;

    @Transient
    private Long courseId;

    @Transient
    private String dayOfTheWeekName;

    public ScheduledClassSession(LocalTime startTime, LocalTime endTime, DayOfWeek dayOfWeek, Room room, ClassType classType, Course course) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.dayOfWeek = dayOfWeek;
        this.room = room;
        this.classType = classType;
        this.course = course;
    }
}
