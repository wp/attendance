package mk.ukim.finki.attendance.model.enumeration;


public enum RoomType {
    CLASSROOM, LAB, MEETING_ROOM, OFFICE, VIRTUAL
}
