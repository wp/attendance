package mk.ukim.finki.attendance.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mk.ukim.finki.attendance.model.enumeration.ExamSession;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class YearExamSession {
    @Id
    private String name;

    @Enumerated(value = EnumType.STRING)
    private ExamSession session;

    @ManyToOne
    @JoinColumn(name="semester_code")
    private Semester semester;

    @Column(name = "session_start")
    private LocalDate startDate;

    @Column(name = "session_end")
    private LocalDate endDate;

    @Column(name = "enrollment_start_date")
    private LocalDate enrollmentStartDate;

    @Column(name = "enrollment_end_date")
    private LocalDate enrollmentEndDate;

    private String year;

    public YearExamSession(ExamSession session, Semester semester, LocalDate startDate, LocalDate endDate, LocalDate enrollmentStartDate, LocalDate enrollmentEndDate, String year) {
        this.name = String.format("%s-%s", year, session.name());
        this.session = session;
        this.semester = semester;
        this.startDate = startDate;
        this.endDate = endDate;
        this.enrollmentStartDate = enrollmentStartDate;
        this.enrollmentEndDate = enrollmentEndDate;
        this.year = year;
    }
}
