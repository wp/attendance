package mk.ukim.finki.attendance.model.exception.professorclasssession;

public class ProfessorClassSessionNotFoundException extends ProfessorClassSessionCrud{


    public ProfessorClassSessionNotFoundException(String message) {
        super(message);
    }
}
