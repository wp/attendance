package mk.ukim.finki.attendance.model.exception.scheduledClassSession;

public class ScheduledClassSessionNotFoundException extends RuntimeException{
    public ScheduledClassSessionNotFoundException() {
    }

    public ScheduledClassSessionNotFoundException(Long id){
        super("Не е пронајдена сесија со наведеното ID " + id);
    }
}
