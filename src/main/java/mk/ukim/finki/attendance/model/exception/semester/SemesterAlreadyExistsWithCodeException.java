package mk.ukim.finki.attendance.model.exception.semester;


public class SemesterAlreadyExistsWithCodeException extends RuntimeException {

    public SemesterAlreadyExistsWithCodeException(String message) {
        super(message);
    }

    public SemesterAlreadyExistsWithCodeException(String message, Throwable cause) {
        super(message, cause);
    }
}
