package mk.ukim.finki.attendance.model.dto;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;
import mk.ukim.finki.attendance.model.enumeration.ClassType;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Getter
@Setter
@JsonPropertyOrder({"index", "studentName", "joinedSubjectAbbreviation", "studyYear", "joinedSubjectName", "courseEnglish", "semesterCode", "scheduledClassSessionRoom", "scheduledClassSessionType", "startTime", "endTime", "studentArrivalTime"})
public class StudentAttendanceDTO {

    private String index;

    private String studentName;

    private String joinedSubjectAbbreviation;

    private String joinedSubjectName;

    private Boolean courseEnglish;

    private String semesterCode;

    private String scheduledClassSessionRoom;

    private ClassType scheduledClassSessionType;

    private String studyYear;

    private LocalTime startTime;

    private LocalTime endTime;

    private LocalDateTime studentArrivalTime;

    public StudentAttendanceDTO(String index, String studentName, String joinedSubjectAbbreviation, String joinedSubjectName, Boolean courseEnglish, String semesterCode, String scheduledClassSessionRoom, ClassType scheduledClassSessionType, String studyYear, LocalTime startTime, LocalTime endTime, LocalDateTime studentArrivalTime) {
        this.index = index;
        this.studentName = studentName;
        this.joinedSubjectAbbreviation = joinedSubjectAbbreviation;
        this.joinedSubjectName = joinedSubjectName;
        this.courseEnglish = courseEnglish;
        this.semesterCode = semesterCode;
        this.scheduledClassSessionRoom = scheduledClassSessionRoom;
        this.scheduledClassSessionType = scheduledClassSessionType;
        this.studyYear = studyYear;
        this.startTime = startTime;
        this.endTime = endTime;
        this.studentArrivalTime = studentArrivalTime;
    }
}
