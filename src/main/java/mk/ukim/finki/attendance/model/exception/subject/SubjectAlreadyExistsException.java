package mk.ukim.finki.attendance.model.exception.subject;

public class SubjectAlreadyExistsException extends RuntimeException {
    public SubjectAlreadyExistsException(String id) {
        super(String.format("Subject with id %s already exists", id));
    }
}