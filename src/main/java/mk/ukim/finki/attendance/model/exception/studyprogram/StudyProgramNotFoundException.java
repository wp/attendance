package mk.ukim.finki.attendance.model.exception.studyprogram;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="No such StudyProgram")
public class StudyProgramNotFoundException extends StudyProgramCrudException {
    public StudyProgramNotFoundException(String message) {
        super(message);
    }
}
