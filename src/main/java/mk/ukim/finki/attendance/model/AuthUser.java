package mk.ukim.finki.attendance.model;

import jakarta.persistence.*;
import lombok.*;

import mk.ukim.finki.attendance.model.enumeration.UserRole;
import org.hibernate.Hibernate;

import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "auth_user")
public class AuthUser {

    @Id
    private String id;

    private String name;

    private String email;

    @Enumerated(EnumType.STRING)
    private UserRole role;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        AuthUser authUser = (AuthUser) o;
        return getId() != null && Objects.equals(getId(), authUser.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
