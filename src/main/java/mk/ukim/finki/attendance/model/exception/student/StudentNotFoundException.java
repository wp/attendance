package mk.ukim.finki.attendance.model.exception.student;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Student not found")
public class StudentNotFoundException extends StudentCrudException{
    public StudentNotFoundException(String message) {
        super(message);
    }
}
