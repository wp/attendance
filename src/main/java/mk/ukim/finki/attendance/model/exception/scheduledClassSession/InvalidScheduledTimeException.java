package mk.ukim.finki.attendance.model.exception.scheduledClassSession;

public class InvalidScheduledTimeException extends RuntimeException{

    public InvalidScheduledTimeException() {
    }

    public InvalidScheduledTimeException(String message) {
        super(message);
    }
}
