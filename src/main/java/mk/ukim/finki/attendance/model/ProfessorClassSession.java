package mk.ukim.finki.attendance.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;
import org.springframework.data.repository.cdi.Eager;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonPropertyOrder({"professor_id", "scheduled_class_session_id_name", "date",
        "professorArrivalTime", "attendanceToken"})
public class ProfessorClassSession {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name="professor_id")
    private Professor professor;

    @ManyToOne
    @JoinColumn(name="scheduled_class_session_id")
    private ScheduledClassSession scheduledClassSession;

    private LocalDate date;

    @Column(name="professor_arrival_time")
    private LocalDateTime professorArrivalTime;

    @Column(name="attendance_token")
    private String attendanceToken;

    @Transient
    private String professor_id;

    @Transient
    private String scheduled_class_session_id_name;

    @OneToMany(mappedBy = "professorClassSession", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<StudentAttendance> studentAttendances;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProfessorClassSession professorClassSession = (ProfessorClassSession) o;
        return getId() != null && Objects.equals(getId(), professorClassSession.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public ProfessorClassSession(Professor professor, ScheduledClassSession scheduledClassSession, LocalDate date, LocalDateTime professorArrivalTime, String attendanceToken) {
        this.professor = professor;
        this.scheduledClassSession = scheduledClassSession;
        this.date = date;
        this.professorArrivalTime = professorArrivalTime;
        this.attendanceToken = attendanceToken;
    }

    public ProfessorClassSession(Long id) {
        this.id = id;
    }
}
