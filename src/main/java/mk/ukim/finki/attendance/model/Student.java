package mk.ukim.finki.attendance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@JsonPropertyOrder({"studentIndex", "email", "lastName", "name", "parentName", "studyProgramCode"})

public class Student {

    @Id
    @Column(name="student_index")
    private String studentIndex;

    private String email;

    @Column(name="last_name")
    private String lastName;

    private String name;

    @Column(name="parent_name")
    private String parentName;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="study_program_code", nullable = false)
    private StudyProgram studyProgram;

    @Column(name = "study_program_code", insertable = false, updatable = false)
    private String studyProgramCode;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Student student = (Student) o;
        return getStudentIndex() != null && Objects.equals(getStudentIndex(), student.getStudentIndex());
    }

    public Student(String studentIndex, String email, String lastName, String name, String parentName, StudyProgram studyProgram) {
        this.studentIndex = studentIndex;
        this.email = email;
        this.lastName = lastName;
        this.name = name;
        this.parentName = parentName;
        this.studyProgram = studyProgram;
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public Student(String studentIndex) {
        this.studentIndex = studentIndex;
    }
}
