package mk.ukim.finki.attendance.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mk.ukim.finki.attendance.model.enumeration.SemesterType;
import org.hibernate.Hibernate;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonPropertyOrder({"id","name","semesterType","abbreviation","weeklyAuditoriumClasses","weeklyLabClasses","weeklyLecturesClasses"})
public class Subject {
    @Id
    private String id;

    @Column(nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    private SemesterType semester;

    @Column(name = "weekly_auditorium_classes")
    private Integer weeklyAuditoriumClasses;

    @Column(name="weekly_lab_classes")
    private Integer weeklyLabClasses;

    @Column(name = "weekly_lectures_classes")
    private Integer weeklyLecturesClasses;

    @Transient
    private String semesterType;

    private String  abbreviation;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Subject subject = (Subject) o;
        return getId() != null && Objects.equals(getId(), subject.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
