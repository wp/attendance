package mk.ukim.finki.attendance.model.exception.subject;

public class SubjectNotFoundException extends RuntimeException {
    public SubjectNotFoundException(String id) {
        super(String.format("Subject with id %s does not exist", id));
    }
}