package mk.ukim.finki.attendance.model.exception.scheduledClassSession;

import mk.ukim.finki.attendance.model.Room;
import java.time.DayOfWeek;
import java.time.LocalTime;
public class ConflictingScheduledClassSessionException extends RuntimeException{
    public ConflictingScheduledClassSessionException(DayOfWeek dayOfWeek, LocalTime startTime, LocalTime endTime, Room room) {
        super(String.format("Веќе има закажана сесија во %s од %s до %s во %s", dayOfWeek,startTime,endTime,room));
    }
}
