package mk.ukim.finki.attendance.model.exception.student;

public abstract class StudentCrudException extends RuntimeException{
    public StudentCrudException(String message) {
        super(message);
    }
}
