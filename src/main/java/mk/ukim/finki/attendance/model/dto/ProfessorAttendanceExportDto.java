package mk.ukim.finki.attendance.model.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.*;
import mk.ukim.finki.attendance.model.enumeration.ClassType;

import java.time.LocalDate;
import java.time.LocalTime;

@ToString
@Getter
@Setter
@Data
@NoArgsConstructor
@JsonPropertyOrder({
        "professorName",
        "joinedSubjectAbbreviation",
        "studyYear",
        "joinedSubjectName",
        "courseEnglish",
        "semesterCode",
        "scheduledClassSessionRoom",
        "scheduledClassSessionType",
        "date",
        "startTime",
        "endTime",
        "professorAttended",
        "numberOfStudents"
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProfessorAttendanceExportDto {

    String professorName;
    String joinedSubjectAbbreviation;
    Short studyYear;
    String joinedSubjectName;
    Boolean courseEnglish;
    String semesterCode;
    String scheduledClassSessionRoom;
    ClassType scheduledClassSessionType;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    LocalDate date;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    LocalTime startTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    LocalTime endTime;
    Long numberOfStudents;
    Boolean professorAttended;

    public ProfessorAttendanceExportDto(String professorName, String joinedSubjectAbbreviation, Short studyYear, String joinedSubjectName, Boolean courseEnglish, String semesterCode, String scheduledClassSessionRoom, ClassType scheduledClassSessionType, LocalDate date, LocalTime startTime, LocalTime endTime, Boolean professorAttended, Long numberOfStudents ) {
        this.professorName = professorName;
        this.joinedSubjectAbbreviation = joinedSubjectAbbreviation;
        this.studyYear = studyYear;
        this.joinedSubjectName = joinedSubjectName;
        this.courseEnglish = courseEnglish;
        this.semesterCode = semesterCode;
        this.scheduledClassSessionRoom = scheduledClassSessionRoom;
        this.scheduledClassSessionType = scheduledClassSessionType;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.numberOfStudents = numberOfStudents;
        this.professorAttended = professorAttended;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("numberOfStudents")
    public Long getNumberOfStudents() {
        return professorAttended ? numberOfStudents : null;
    }
}

