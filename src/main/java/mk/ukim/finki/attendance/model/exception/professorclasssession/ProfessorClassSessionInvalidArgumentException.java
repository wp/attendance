package mk.ukim.finki.attendance.model.exception.professorclasssession;

public class ProfessorClassSessionInvalidArgumentException extends ProfessorClassSessionCrud{
    public ProfessorClassSessionInvalidArgumentException(String message) {
        super(message);
    }
}
