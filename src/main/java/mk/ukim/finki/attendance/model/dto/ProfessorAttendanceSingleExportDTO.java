package mk.ukim.finki.attendance.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;
import mk.ukim.finki.attendance.model.enumeration.ClassType;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@ToString
@Getter
@Setter
@Data
@NoArgsConstructor
@JsonPropertyOrder({
        "professorName",
        "joinedSubjectAbbreviation",
        "studyYear",
        "joinedSubjectName",
        "courseEnglish",
        "semesterCode",
        "scheduledClassSessionRoom",
        "scheduledClassSessionType",
        "date",
        "startTime",
        "endTime",
        "professorAttended",
        "studentAttended",
        "studentName",
        "studentLastName",
        "studentIndex"
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProfessorAttendanceSingleExportDTO {

    String professorName;

    String joinedSubjectAbbreviation;
    Short studyYear;
    String joinedSubjectName;
    Boolean courseEnglish;
    String semesterCode;
    String scheduledClassSessionRoom;
    ClassType scheduledClassSessionType;
    LocalDate date;
    LocalTime startTime;
    LocalTime endTime;
    @JsonIgnore
    LocalDateTime whenProfessorCame;
    String studentName;
    String studentLastName;
    String studentIndex;
    Boolean professorAttended;
    @JsonIgnore
    String attendanceToken;
    Boolean studentAttended;
    @JsonIgnore
    LocalDateTime studentArrivalTime;

    public ProfessorAttendanceSingleExportDTO(String professorName, String joinedSubjectAbbreviation, Short studyYear, String joinedSubjectName, Boolean courseEnglish, String semesterCode, String scheduledClassSessionRoom, ClassType scheduledClassSessionType, LocalDate date, LocalTime startTime, LocalTime endTime, LocalDateTime whenProfessorCame, String studentName, String studentLastName, String studentIndex, String attendanceToken,LocalDateTime studentArrivalTime) {
        this.professorName = professorName;
        this.joinedSubjectAbbreviation = joinedSubjectAbbreviation;
        this.studyYear = studyYear;
        this.joinedSubjectName = joinedSubjectName;
        this.courseEnglish = courseEnglish;
        this.semesterCode = semesterCode;
        this.scheduledClassSessionRoom = scheduledClassSessionRoom;
        this.scheduledClassSessionType = scheduledClassSessionType;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.whenProfessorCame = whenProfessorCame;
        this.studentName = studentName;
        this.studentLastName = studentLastName;
        this.studentIndex = studentIndex;
        this.attendanceToken = attendanceToken;
        this.professorAttended = hasProfessorAttended(attendanceToken, whenProfessorCame, startTime, endTime, date);
        this.studentArrivalTime=studentArrivalTime;
        this.studentAttended = hasStudentAttended(studentArrivalTime,startTime,endTime,date);

    }

    public boolean hasProfessorAttended(String attendanceToken, LocalDateTime whenProfessorCame, LocalTime startTime, LocalTime endTime, LocalDate date) {

        if( whenProfessorCame==null || startTime==null || endTime==null || date==null){
            return false;
        }

        LocalTime professorArrivalLocalTime = whenProfessorCame.toLocalTime();

        LocalDate professorArrivalLocalDate = whenProfessorCame.toLocalDate();

        return attendanceToken != null &&
                !attendanceToken.isBlank() &&
                professorArrivalLocalDate.equals(date) &&
                ((professorArrivalLocalTime.isAfter(startTime) &&
                professorArrivalLocalTime.isBefore(endTime)) ||
                (professorArrivalLocalTime.equals(startTime) ||
                professorArrivalLocalTime.equals(endTime)));
    }
    public boolean hasStudentAttended(LocalDateTime studentArrivalTime, LocalTime startTime, LocalTime endTime, LocalDate date){

        if( studentArrivalTime==null || startTime==null || endTime==null || date==null){
            return false;
        }
        LocalTime studentArrivalLocalTime = studentArrivalTime.toLocalTime();

        LocalDate studentArrivalLocalDate = studentArrivalTime.toLocalDate();


        return  studentArrivalLocalDate.equals(date) &&
                ((studentArrivalLocalTime.isAfter(startTime) &&
                studentArrivalLocalTime.isBefore(endTime)) ||
                (studentArrivalLocalTime.equals(startTime) ||
                studentArrivalLocalTime.equals(endTime)));
    }
}

