package mk.ukim.finki.attendance.model.exception.joinedsubject;

public class JoinedSubjectNotFoundException extends RuntimeException {
    public JoinedSubjectNotFoundException(String abbreviation) {

        super(String.format("JoinedSubject with id %s not found", abbreviation));
    }

}
