package mk.ukim.finki.attendance.model.exception.professorclasssession;

public abstract class ProfessorClassSessionCrud extends RuntimeException{
    public ProfessorClassSessionCrud(String message) {
        super(message);
    }
}