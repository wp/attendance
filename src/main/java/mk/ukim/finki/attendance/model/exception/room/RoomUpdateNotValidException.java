package mk.ukim.finki.attendance.model.exception.room;

public class RoomUpdateNotValidException extends RuntimeException{
    public RoomUpdateNotValidException() {
        super("Some/All value are not valid.");
    }
}
