package mk.ukim.finki.attendance.model.exception.student;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.UNPROCESSABLE_ENTITY, reason="Bad arguments provided for Student")
public class InvalidStudentException extends StudentCrudException {
    public InvalidStudentException(String message) {
        super(message);
    }
}
