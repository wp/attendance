package mk.ukim.finki.attendance.model;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Course {

    @Id
    @GeneratedValue
    private Long id;

    private Short studyYear;

    private String lastNameRegex;

    @ManyToOne
    private JoinedSubject joinedSubject;

    @ManyToOne
    private Semester semester;

    @ManyToOne
    private Professor professor;

    @ManyToOne
    private Professor assistant;

    private Integer numberOfFirstEnrollments;

    private Integer numberOfReEnrollments;

    private Double groupPortion;

    private String professors;

    private String groups;

    private String assistants;


    private Boolean english;

    public Course(Short studyYear, String lastNameRegex, JoinedSubject joinedSubject, Semester semester, Professor professor, Professor assistant, Integer numberOfFirstEnrollments, Integer numberOfReEnrollments, Double groupPortion, String professors, String groups, String assistants, Boolean english) {
        this.studyYear = studyYear;
        this.lastNameRegex = lastNameRegex;
        this.joinedSubject = joinedSubject;
        this.semester = semester;
        this.professor = professor;
        this.assistant = assistant;
        this.numberOfFirstEnrollments = numberOfFirstEnrollments;
        this.numberOfReEnrollments = numberOfReEnrollments;
        this.groupPortion = groupPortion;
        this.professors = professors;
        this.groups = groups;
        this.assistants = assistants;
        this.english = english;
    }
}
