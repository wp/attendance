package mk.ukim.finki.attendance.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mk.ukim.finki.attendance.model.enumeration.SemesterType;
import mk.ukim.finki.attendance.model.enumeration.StudyCycle;
import org.hibernate.Hibernate;
import java.time.LocalDateTime;
import java.util.Objects;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "joined_subject")
@JsonPropertyOrder({"abbreviation", "name", "codes","semType","mainSubId","weeklyAuditoriumClasses","weeklyLabClasses","weeklyLecturesClasses","studyCycle","validationMessage"})
public class JoinedSubject {

    @Id
    private String abbreviation;

    @Column(nullable = false)
    private String name;

    private String codes;

    @Enumerated(value = EnumType.STRING)
    @JsonIgnore
    @Column(name = "semester_type")
    private SemesterType semesterType;

    @Transient
    private String semType;

    @ManyToOne
    @JoinColumn(name = "main_subject_id")
    @JsonIgnore
    private Subject mainSubject;

    @Transient
    private String mainSubId;

    @Column(name = "weekly_auditorium_classes")
    private Integer weeklyAuditoriumClasses;

    @Column(name = "weekly_lab_classes")
    private Integer weeklyLabClasses;

    @Column(name = "weekly_lectures_classes")
    private Integer weeklyLecturesClasses;

    @Enumerated(value = EnumType.STRING)
    @JsonIgnore
    private StudyCycle cycle;

    @Transient
    private String studyCycle;


    @Column(name = "last_update_time")
    private LocalDateTime lastUpdateTime;

    @ManyToOne
    @JoinColumn(name = "last_update_user", referencedColumnName = "id")
    private AuthUser lastUpdateUser;

    @Column(name = "validation_message")
    private String validationMessage;

    public JoinedSubject(String abbreviation, String name, String codes, SemesterType semesterType, Subject subject, Integer weeklyAuditoriumClasses, Integer weeklyLabClasses, Integer weeklyLecturesClasses, StudyCycle cycle, LocalDateTime localDateTime, AuthUser lastUpdateUser, String validationMessage) {
      this.abbreviation = abbreviation;
      this.name = name;
      this.codes = codes;
      this.semesterType = semesterType;
      this.mainSubject = subject;
      this. weeklyAuditoriumClasses = weeklyAuditoriumClasses;
      this.weeklyLabClasses = weeklyLabClasses;
      this.weeklyLecturesClasses = weeklyLecturesClasses;
      this.cycle = cycle;
       this.lastUpdateUser = lastUpdateUser;
       this.lastUpdateTime = localDateTime;
       this.validationMessage = validationMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        JoinedSubject subject = (JoinedSubject) o;
        return getAbbreviation() != null && Objects.equals(getAbbreviation(), subject.getAbbreviation());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
