package mk.ukim.finki.attendance.model.exception.yearExamSession;

public class YearExamSessionNotFoundException extends RuntimeException {
    public YearExamSessionNotFoundException(String id){
        super("Не е пронајдена годишна испитна сесија со наведеното ID " + id);
    }
}
