package mk.ukim.finki.attendance.model.exception.scheduledClassSession;

public class InvalidScheduledClassSessionUpdate extends RuntimeException {

    public InvalidScheduledClassSessionUpdate(){
        super("Грешка при зачувување.");
    }
}
