package mk.ukim.finki.attendance.model.exception.studyprogram;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.CONFLICT, reason="StudyProgram already exists")
public class StudyProgramAlreadyExistsException extends StudyProgramCrudException {
    public StudyProgramAlreadyExistsException(String message) {
        super(message);
    }
}
