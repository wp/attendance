package mk.ukim.finki.attendance.model.exception.holiday;

import java.time.LocalDate;

public class InvalidHolidayIdException extends RuntimeException {
    public InvalidHolidayIdException(LocalDate date) {
        super("Невалиденн празник");
    }
}
