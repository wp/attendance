package mk.ukim.finki.attendance.model.exception.studyprogram;

public abstract class StudyProgramCrudException extends RuntimeException{
    public StudyProgramCrudException(String message) {
        super(message);
    }
}
