package mk.ukim.finki.attendance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mk.ukim.finki.attendance.model.enumeration.ProfessorTitle;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"id", "email", "name", "roomName", "orderingRank", "titleName"})
public class Professor {

    @Id
    private String id;

    private String email;

    private String name;

    @JsonIgnore
    @Enumerated(value = EnumType.STRING)
    private ProfessorTitle title;

    @Transient
    private String titleName;

    @Transient
    private String roomName;

    @Column(name = "ordering_rank")
    private Short orderingRank;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "office_name")
    private Room room;


}
