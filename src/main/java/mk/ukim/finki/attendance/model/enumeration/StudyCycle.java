package mk.ukim.finki.attendance.model.enumeration;

import lombok.Getter;

@Getter
public enum StudyCycle {
    UNDERGRADUATE, MASTER, PHD
}
