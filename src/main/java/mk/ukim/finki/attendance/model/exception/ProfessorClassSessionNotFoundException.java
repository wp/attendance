package mk.ukim.finki.attendance.model.exception;

public class ProfessorClassSessionNotFoundException extends RuntimeException{
    public ProfessorClassSessionNotFoundException() {
    }

    public ProfessorClassSessionNotFoundException(Long id) {
        super("Professor Class Session with ID is not found " + id);
    }
}
