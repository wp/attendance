package mk.ukim.finki.attendance.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mk.ukim.finki.attendance.model.enumeration.RoomType;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"name", "capacity", "roomTypeName", "equipmentDescription", "locationDescription"})
public class Room {

    @Id
    private String name;

    private Long capacity;

    @Column(name = "equipment_description")
    private String equipmentDescription;

    @Column(name = "location_description")
    private String locationDescription;


    @Enumerated(value = EnumType.STRING)
    @Column(name = "type")
    private RoomType roomType;

    @Transient
    private String roomTypeName;

    public Room(String name) {
        this.name = name;
    }
}
