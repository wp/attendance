package mk.ukim.finki.attendance.model.exception.studentattendance;

public class StudentAttendanceNotFoundException extends RuntimeException {
    public StudentAttendanceNotFoundException(Long id) {
        super(String.format("StudentAttendance with id %d not found", id));
    }
}
