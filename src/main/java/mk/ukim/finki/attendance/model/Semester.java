package mk.ukim.finki.attendance.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.persistence.*;
import lombok.*;
import mk.ukim.finki.attendance.model.enumeration.SemesterState;
import mk.ukim.finki.attendance.model.enumeration.SemesterType;
import org.hibernate.Hibernate;

import java.time.LocalDate;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@JsonPropertyOrder({"code", "year", "semesterTypeName",
        "startDate", "endDate", "enrollmentStartDate", "enrollmentEndDate", "stateName"})
public class Semester {

    @Id
    private String code;

    private String year;

    @Enumerated(EnumType.STRING)
    private SemesterType semesterType;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "enrollment_start_date")
    private LocalDate enrollmentStartDate;

    @Column(name = "enrollment_end_date")
    private LocalDate enrollmentEndDate;

    @Enumerated(EnumType.STRING)
    private SemesterState state;

    @Transient
    private String semesterTypeName;

    @Transient
    private String stateName;

    public Semester(String code, String year, SemesterType semesterType, LocalDate startDate, LocalDate endDate, LocalDate enrollmentStartDate, LocalDate enrollmentEndDate, SemesterState state) {
        this.code = code;
        this.year = year;
        this.semesterType = semesterType;
        this.startDate = startDate;
        this.endDate = endDate;
        this.enrollmentStartDate = enrollmentStartDate;
        this.enrollmentEndDate = enrollmentEndDate;
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Semester semester = (Semester) o;
        return getCode() != null && Objects.equals(getCode(), semester.getCode());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public int yearsPassed(LocalDate date) {
        int year = Integer.parseInt(this.year.split("/")[0]);
        return date.getYear() - year;
    }

    public int yearsPassed() {
        return this.yearsPassed(LocalDate.now());
    }

}
