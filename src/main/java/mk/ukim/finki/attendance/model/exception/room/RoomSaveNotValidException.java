package mk.ukim.finki.attendance.model.exception.room;

public class RoomSaveNotValidException extends RuntimeException{
    public RoomSaveNotValidException() {
        super("Some/All value are not valid.");
    }
}
