package mk.ukim.finki.attendance.model.exception.semester;

public class SemesterNotFoundException extends RuntimeException{
    public SemesterNotFoundException() {
        super("Semester Not Found");
    }
}
