package mk.ukim.finki.attendance.model.exception.room;

public class NoRoomFoundException extends RuntimeException {

    public NoRoomFoundException() {
    }

    public NoRoomFoundException(String name) {
        super("No Room Found exception by id: " + name);
    }
}
