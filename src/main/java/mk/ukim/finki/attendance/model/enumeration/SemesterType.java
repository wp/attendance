package mk.ukim.finki.attendance.model.enumeration;

import lombok.Getter;

@Getter
public enum SemesterType {
    WINTER,
    SUMMER
}
