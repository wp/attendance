package mk.ukim.finki.attendance.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class StudyProgram {

    @Id
    private String code;

    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        StudyProgram studyProgram = (StudyProgram) o;
        return getCode() != null && Objects.equals(getCode(), studyProgram.getCode());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public StudyProgram(String code, String name) {
        this.code = code;
        this.name = name;
    }
}
