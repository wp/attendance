package mk.ukim.finki.attendance.model.enumeration;

public enum ClassType {
    LECTURE,
    AUDITORY_EXERCISE,
    LABORATORY

}
