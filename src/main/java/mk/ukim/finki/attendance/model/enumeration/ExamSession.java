package mk.ukim.finki.attendance.model.enumeration;

public enum ExamSession {
    FIRST_MIDTERM, SECOND_MIDTERM, JANUARY, JUNE, SEPTEMBER
}

