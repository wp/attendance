package mk.ukim.finki.attendance.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"index", "studentName", "joinedSubjectAbbreviation", "joinedSubjectName", "courseEnglish", "semesterCode", "scheduledClassSessionRoom", "scheduledClassSessionType", "date", "startTime", "endTime", "studentArrivalTime"})
public class StudentAttendanceExportDTO {

    private String index;
    private String studentName;
    private String joinedSubjectAbbreviation;
    private String joinedSubjectName;
    private String courseEnglish;
    private String semesterCode;
    private String scheduledClassSessionRoom;
    private String scheduledClassSessionType;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate date;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    private LocalTime startTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    private LocalTime endTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime studentArrivalTime;
}
