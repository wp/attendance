package mk.ukim.finki.attendance.config;

import mk.ukim.finki.attendance.model.enumeration.UserRole;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.expression.WebExpressionAuthorizationManager;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.csrf(AbstractHttpConfigurer::disable);

        SavedRequestAwareAuthenticationSuccessHandler successHandler =
                new SavedRequestAwareAuthenticationSuccessHandler();
        successHandler.setDefaultTargetUrl("/");

        http.formLogin(formLogin -> formLogin
                        .successHandler(successHandler)
                )
                .authorizeHttpRequests((requests) -> requests
                        .requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
                        .requestMatchers("static/styles/**", "/").permitAll()
                        .requestMatchers("/**").hasAnyRole(
                                UserRole.ACADEMIC_AFFAIR_VICE_DEAN.name(),
                                UserRole.SCIENCE_AND_COOPERATION_VICE_DEAN.name(),
                                UserRole.FINANCES_VICE_DEAN.name(),
                                UserRole.DEAN.name(),
                                UserRole.PROFESSOR.name()
                        )
                        .requestMatchers("/thesis/{professorId}/**").access(
                                new WebExpressionAuthorizationManager("#professorId == authentication.name")
                        )
                        .requestMatchers("/admin/**").hasAnyRole(
                                UserRole.ACADEMIC_AFFAIR_VICE_DEAN.name(),
                                UserRole.STUDENT_ADMINISTRATION.name(),
                                UserRole.PROFESSOR.name()
                        )
                        .anyRequest().permitAll()
                )
                .httpBasic((basic) -> basic.realmName("wp.finki.ukim.mk"))
                .logout((logout) -> logout
                        .logoutUrl("/logout")
                        .invalidateHttpSession(true)
                        .clearAuthentication(true)
                        .logoutSuccessUrl("/")
                        .permitAll());

        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
