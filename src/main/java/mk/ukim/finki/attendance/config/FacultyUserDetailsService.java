package mk.ukim.finki.attendance.config;

import mk.ukim.finki.attendance.model.AuthUser;
import mk.ukim.finki.attendance.model.Professor;
import mk.ukim.finki.attendance.model.Student;
import mk.ukim.finki.attendance.model.exception.auth.InvalidUsernameException;
import mk.ukim.finki.attendance.model.exception.professor.ProfessorNotFoundException;
import mk.ukim.finki.attendance.model.exception.student.StudentNotFoundException;
import mk.ukim.finki.attendance.repository.AuthUserRepository;
import mk.ukim.finki.attendance.repository.ProfessorRepository;
import mk.ukim.finki.attendance.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class FacultyUserDetailsService implements UserDetailsService {

    @Value("${system.authentication.password}")
    private String systemAuthenticationPassword;

    private final AuthUserRepository userRepository;

    private final ProfessorRepository professorRepository;
    private final StudentRepository studentRepository;

    private final PasswordEncoder passwordEncoder;

    public FacultyUserDetailsService(AuthUserRepository userRepository,
                                     ProfessorRepository professorService,
                                     StudentRepository studentRepository,
                                     PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.professorRepository = professorService;
        this.studentRepository = studentRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AuthUser user = userRepository.findById(username).orElseThrow(InvalidUsernameException::new);
        if (user.getRole().isProfessor()) {
            Professor professor = professorRepository.findById(username).orElseThrow(()-> new ProfessorNotFoundException(user.getId()));
            return new FacultyUserDetails(user, professor, passwordEncoder.encode(systemAuthenticationPassword));
        } else if (user.getRole().isStudent()) {
            Student student= studentRepository.findById(username).orElseThrow(()->new StudentNotFoundException("Student not found"));
            return new FacultyUserDetails(user, student, passwordEncoder.encode(systemAuthenticationPassword));
        } else {
            return new FacultyUserDetails(user, passwordEncoder.encode(systemAuthenticationPassword));
        }
    }
}
