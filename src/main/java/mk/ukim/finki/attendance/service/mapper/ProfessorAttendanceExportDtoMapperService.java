package mk.ukim.finki.attendance.service.mapper;

import mk.ukim.finki.attendance.model.ProfessorClassSession;
import mk.ukim.finki.attendance.model.dto.ProfessorAttendanceExportDto;

import java.util.List;

public interface ProfessorAttendanceExportDtoMapperService {
    ProfessorAttendanceExportDto mapToProfessorAttendanceExportDto(ProfessorClassSession professorClassSessions);
}
