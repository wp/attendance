package mk.ukim.finki.attendance.service.specification;

import jakarta.persistence.criteria.Predicate;
import mk.ukim.finki.attendance.model.Room;
import mk.ukim.finki.attendance.model.ScheduledClassSession;
import mk.ukim.finki.attendance.model.enumeration.ClassType;
import org.springframework.data.jpa.domain.Specification;

import java.time.DayOfWeek;
import java.time.LocalTime;

public class ScheduledClassSessionSpecification {
    public static Specification<ScheduledClassSession> buildSpecification(DayOfWeek dayOfWeek,
                                                                          String roomName,
                                                                          ClassType classType,
                                                                          Long courseId) {
        return Specification.where(withDayOfWeek(dayOfWeek))
                .and(withRoomName(roomName))
                .and(withClassType(classType))
                .and(withCourse(courseId));
    }

    private static Specification<ScheduledClassSession> withDayOfWeek(DayOfWeek dayOfWeek) {
        if (dayOfWeek == null) {
            return null;
        }
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("dayOfWeek"), dayOfWeek);
    }

    private static Specification<ScheduledClassSession> withRoomName(String roomName) {
        if (roomName == null || roomName.isEmpty()) {
            return null;
        }
        return ((root, query, cb) -> cb.like(cb.lower(root.get("room").get("name")), "%" + roomName.toLowerCase() + "%"));
    }

    private static Specification<ScheduledClassSession> withClassType(ClassType classType) {
        if (classType == null) {
            return null;
        }
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("classType"), classType);
    }

    private static Specification<ScheduledClassSession> withCourse(Long courseId) {
        if (courseId == null) {
            return null;
        }
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("course").get("id"), courseId);
    }

    public static Specification<ScheduledClassSession> isConflict(LocalTime startTime, LocalTime endTime, DayOfWeek dayOfWeek, Room room, Long excludeId) {
        return (root, query, cb) -> {

            Predicate isSameDay = cb.equal(root.get("dayOfWeek"), dayOfWeek);
            Predicate isSameRoom = cb.equal(root.get("room"), room);
            Predicate isSameTime = cb.and(
                    cb.lessThan(root.get("startTime"), endTime),
                    cb.greaterThan(root.get("endTime"), startTime)
            );

            Predicate combinedPredicate = cb.and(isSameDay, isSameRoom, isSameTime);

            return excludeId != null ? cb.and(combinedPredicate, cb.notEqual(root.get("id"), excludeId)) : combinedPredicate;
        };
    }

}
