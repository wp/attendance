package mk.ukim.finki.attendance.service.impl;

import jakarta.transaction.Transactional;
import mk.ukim.finki.attendance.model.Subject;
import mk.ukim.finki.attendance.model.enumeration.SemesterType;
import mk.ukim.finki.attendance.model.exception.subject.SubjectAlreadyExistsException;
import mk.ukim.finki.attendance.model.exception.subject.SubjectNotFoundException;
import mk.ukim.finki.attendance.repository.SubjectRepository;
import mk.ukim.finki.attendance.service.SubjectService;
import mk.ukim.finki.attendance.service.specification.SubjectSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class SubjectServiceImpl implements SubjectService {

    private final SubjectRepository subjectRepository;

    public SubjectServiceImpl(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    @Override
    public List<Subject> listAll() {
        return this.subjectRepository.findAll();
    }

    @Override
    public Page<Subject> findAll(Pageable pageable) {
        return subjectRepository.findAll(pageable);
    }

    @Override
    public Subject findById(String id) {
        return subjectRepository.findById(id).orElseThrow(() -> new SubjectNotFoundException(id));
    }

    @Override
    public Subject create(String id, String name, SemesterType semester, Integer weeklyAuditoriumClasses, Integer weeklyLabClasses, Integer weeklyLecturesClasses, String abbreviation) {
        if (subjectRepository.existsById(id)) {
            throw new SubjectAlreadyExistsException(id);
        }

        Subject subject = setSubjectAttributes(new Subject(), id, name, semester, weeklyAuditoriumClasses, weeklyLabClasses, weeklyLecturesClasses, abbreviation);
        return subjectRepository.save(subject);
    }

    @Override
    public Subject update(String id, String name, SemesterType semester, Integer weeklyAuditoriumClasses, Integer weeklyLabClasses, Integer weeklyLecturesClasses, String abbreviation) {

        Subject existingSubject = subjectRepository.findById(id).orElseThrow(() -> new SubjectNotFoundException(id));

        Subject updatedSubject = setSubjectAttributes(existingSubject, id, name, semester, weeklyAuditoriumClasses, weeklyLabClasses, weeklyLecturesClasses, abbreviation);
        return subjectRepository.save(updatedSubject);
    }

    @Override
    public void delete(String id) {
        Subject subject = this.findById(id);
        subjectRepository.delete(subject);
    }

    private Subject setSubjectAttributes(Subject subject, String id, String name, SemesterType semester, Integer weeklyAuditoriumClasses, Integer weeklyLabClasses, Integer weeklyLecturesClasses, String abbreviation) {
        subject.setId(id);
        subject.setName(name);
        subject.setSemester(semester);
        subject.setWeeklyAuditoriumClasses(weeklyAuditoriumClasses);
        subject.setWeeklyLabClasses(weeklyLabClasses);
        subject.setWeeklyLecturesClasses(weeklyLecturesClasses);
        subject.setAbbreviation(abbreviation);
        return subject;
    }

    @Override
    public Page<Subject> filterByOptions(String name, SemesterType semester, String abbreviation, Pageable pageable) {
        Specification<Subject> specifiaction = SubjectSpecification.buildSpecification(name, semester, abbreviation);
        return subjectRepository.findAll(specifiaction, pageable);
    }

    @Override
    @Transactional
    public List<Subject> importData(List<Subject> subjects) {
        return subjects.stream()
                .map(it -> {
                    try {
                        if (isValidSubject(it)) {

                            it.setSemester(SemesterType.valueOf(it.getSemesterType().toUpperCase()));
                            subjectRepository.save(it);
                            return null;
                        } else {
                            System.out.println("Invalid subject: " + it);
                        }
                        return it;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return it;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }


    private boolean isValidSubject(Subject subject) {
        boolean isValid = subject.getId() != null && !subject.getId().isEmpty() &&
                subject.getName() != null && !subject.getName().isEmpty() &&
                subject.getAbbreviation() != null && !subject.getAbbreviation().isEmpty() &&
                subject.getWeeklyAuditoriumClasses() != null &&
                subject.getWeeklyLabClasses() != null &&
                subject.getWeeklyLecturesClasses() != null
                && !subjectRepository.existsById(subject.getId());

        if (!isValid) {
            System.out.println("Invalid subject fields: " + subject);
        }

        return isValid;
    }


}
