package mk.ukim.finki.attendance.service.impl;

import mk.ukim.finki.attendance.model.DTO.HolidayExportDto;
import mk.ukim.finki.attendance.model.Holiday;
import mk.ukim.finki.attendance.model.exception.holiday.InvalidHolidayIdException;
import mk.ukim.finki.attendance.repository.HolidayRepository;
import mk.ukim.finki.attendance.service.HolidayService;
import mk.ukim.finki.attendance.service.mapper.HolidayExportDtoMapper;
import mk.ukim.finki.attendance.service.mapper.impl.HolidayExportDtoMapperImpl;
import mk.ukim.finki.attendance.service.specification.HolidaySpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class HolidayServiceImpl implements HolidayService {

    private final HolidayRepository holidayRepository;
    private final HolidayExportDtoMapperImpl holidayExportDtoMapperImpl;

    public HolidayServiceImpl(HolidayRepository holidayRepository, HolidayExportDtoMapperImpl holidayExportDtoMapperImpl) {
        this.holidayRepository = holidayRepository;
        this.holidayExportDtoMapperImpl = holidayExportDtoMapperImpl;
    }

    @Override
    public List<Holiday> importData(List<Holiday> holidays) {

        Map<Boolean, List<Holiday>> partitionedHolidaysList = holidays.stream()
                .collect(Collectors.partitioningBy(this::isValidHoliday));

        List<Holiday> validHolidaysList = partitionedHolidaysList.get(true);
        List<Holiday> invalidHolidaysList = partitionedHolidaysList.get(false);

        holidayRepository.saveAll(validHolidaysList);
        return invalidHolidaysList;
    }

    private boolean isValidHoliday(Holiday holiday) {
        return holiday.getDate() != null && holiday.getName() != null && !holiday.getName().isEmpty();
    }

    @Override
    public List<Holiday> listAllHolidays() {
        return this.holidayRepository.findAll();
    }

    @Override
    public Holiday findHolidayById(LocalDate date) {
        return this.holidayRepository.findById(date).orElseThrow(() -> new InvalidHolidayIdException(date));
    }

    @Override
    public Holiday createHoliday(LocalDate date, String name) {
        Holiday holiday = new Holiday(date, name);
        return this.holidayRepository.save(holiday);
    }

    @Override
    public Holiday updateHoliday(LocalDate date, String name) {
        Holiday holiday = this.findHolidayById(date);
        holiday.setName(name);
        return this.holidayRepository.save(holiday);
    }

    @Override
    public Holiday deleteHoliday(LocalDate date) {
        Holiday holiday = this.findHolidayById(date);
        this.holidayRepository.delete(holiday);
        return holiday;
    }

    @Override
    public List<Holiday> findAllByYearAndMonth(Integer year, Integer month) {
        return holidayRepository.findAllByYearAndMonth(year, month);
    }

    @Override
    public Page<Holiday> getAllHolidays(Integer year, Integer month, Pageable pageable) {
        Specification<Holiday> holidaySpecification = HolidaySpecification.buildSpecification(year, month);
        return this.holidayRepository.findAll(holidaySpecification, pageable);
    }

    @Override
    public List<HolidayExportDto> exportHolidays(Integer year, Integer month) {
        Specification<Holiday> specification = HolidaySpecification.buildSpecification(year, month);
        List<Holiday> holidays = holidayRepository.findAll(specification, Sort.by(Sort.Direction.ASC, "date"));
        return holidays.stream()
                .map(holidayExportDtoMapperImpl::mapHolidayToExportDto)
                .collect(Collectors.toList());
    }
}
