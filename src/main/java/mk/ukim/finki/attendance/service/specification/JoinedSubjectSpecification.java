package mk.ukim.finki.attendance.service.specification;

import mk.ukim.finki.attendance.model.JoinedSubject;
import mk.ukim.finki.attendance.model.enumeration.SemesterType;
import mk.ukim.finki.attendance.model.enumeration.StudyCycle;
import org.springframework.data.jpa.domain.Specification;

public class JoinedSubjectSpecification {

    public static Specification<JoinedSubject> buildSpecification(String name, SemesterType semesterType, StudyCycle cycle, String abbreviation) {
        return Specification.where(withName(name))
                .and(withSemesterType(semesterType))
                .and(withCycle(cycle))
                .and(withAbbreviation(abbreviation));
    }

    private static Specification<JoinedSubject> withName(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        return (root, query, cb) -> cb.like(cb.lower(root.get("name")), "%" + name.toLowerCase() + "%");
    }

    private static Specification<JoinedSubject> withSemesterType(SemesterType semesterType) {
        if (semesterType == null) {
            return null;
        }
        return (root, query, cb) -> cb.equal(root.get("semesterType"), semesterType);
    }

    private static Specification<JoinedSubject> withCycle(StudyCycle cycle) {
        if (cycle == null) {
            return null;
        }
        return (root, query, cb) -> cb.equal(root.get("cycle"), cycle);
    }

    private static Specification<JoinedSubject> withAbbreviation(String abbreviation) {
        if (abbreviation == null || abbreviation.isEmpty()) {
            return null;
        }
        return (root, query, cb) -> cb.like(cb.lower(root.get("abbreviation")), "%" + abbreviation.toLowerCase() + "%");
    }
}