package mk.ukim.finki.attendance.service;

import mk.ukim.finki.attendance.model.*;
import mk.ukim.finki.attendance.model.enumeration.ExamSession;
import org.springframework.data.domain.Page;
import java.time.LocalDate;
import java.util.List;

public interface YearExamSessionService {

    YearExamSession findById(String id);

    List<YearExamSession> listAll();

    void deleteYearExamSessionById(String id);

    YearExamSession createYearExamSession(ExamSession session, Semester semester, LocalDate startDate, LocalDate endDate, LocalDate enrollmentStartDate, LocalDate enrollmentEndDate, String year);

    YearExamSession updateYearExamSession(String name, ExamSession session, Semester semester, LocalDate startDate, LocalDate endDate, LocalDate enrollmentStartDate, LocalDate enrollmentEndDate, String year);

    Page<YearExamSession> getAllExamSessionServices(int page, int size);
}
