package mk.ukim.finki.attendance.service.impl;

import mk.ukim.finki.attendance.model.*;
import mk.ukim.finki.attendance.model.enumeration.SemesterType;
import mk.ukim.finki.attendance.model.enumeration.StudyCycle;
import mk.ukim.finki.attendance.model.exception.subject.SubjectNotFoundException;
import mk.ukim.finki.attendance.model.exception.joinedsubject.JoinedSubjectNotFoundException;
import mk.ukim.finki.attendance.repository.JoinedSubjectRepository;
import mk.ukim.finki.attendance.repository.SubjectRepository;
import mk.ukim.finki.attendance.service.JoinedSubjectService;
import mk.ukim.finki.attendance.service.specification.JoinedSubjectSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class JoinedSubjectServiceImpl implements JoinedSubjectService {

    private final JoinedSubjectRepository joinedSubjectRepository;
    private final SubjectRepository subjectRepository;

    public JoinedSubjectServiceImpl(JoinedSubjectRepository joinedSubjectRepository, SubjectRepository subjectRepository) {
        this.joinedSubjectRepository = joinedSubjectRepository;
        this.subjectRepository = subjectRepository;
    }

    @Override
    public JoinedSubject createJoinedSubject(String abbreviation, String name, String codes, SemesterType semesterType, String mainSubjectId, Integer weeklyAuditoriumClasses, Integer weeklyLabClasses, Integer weeklyLecturesClasses, StudyCycle cycle, AuthUser lastUpdateUser, String validationMessage) {
        Subject subject = subjectRepository.findById(mainSubjectId).orElseThrow(() -> new SubjectNotFoundException(mainSubjectId));

        LocalDateTime localDateTime = LocalDateTime.now();

        return joinedSubjectRepository.save(new JoinedSubject(abbreviation, name, codes, semesterType, subject, weeklyAuditoriumClasses, weeklyLabClasses, weeklyLecturesClasses, cycle, localDateTime, lastUpdateUser, validationMessage));
    }

    @Override
    public JoinedSubject readJoinedSubject(String abbreviation) {
        return joinedSubjectRepository.findById(abbreviation).orElseThrow(() -> new JoinedSubjectNotFoundException(abbreviation));
    }

    @Override
    public JoinedSubject updateJoinedSubject(String abbreviation, String name, String codes, SemesterType semesterType, String mainSubjectId, Integer weeklyAuditoriumClasses, Integer weeklyLabClasses, Integer weeklyLecturesClasses, StudyCycle cycle, AuthUser lastUpdateUser, String validationMessage) {
        JoinedSubject joinedSubject = joinedSubjectRepository.findById(abbreviation).orElseThrow(() -> new JoinedSubjectNotFoundException(abbreviation));

        Subject subject = subjectRepository.findById(mainSubjectId).orElseThrow(() -> new SubjectNotFoundException(mainSubjectId));

        LocalDateTime localDateTime = LocalDateTime.now();
        joinedSubject.setAbbreviation(abbreviation);
        joinedSubject.setName(name);
        joinedSubject.setCodes(codes);
        joinedSubject.setSemesterType(semesterType);
        joinedSubject.setMainSubject(subject);
        joinedSubject.setWeeklyAuditoriumClasses(weeklyAuditoriumClasses);
        joinedSubject.setWeeklyLabClasses(weeklyLabClasses);
        joinedSubject.setWeeklyLecturesClasses(weeklyLecturesClasses);
        joinedSubject.setCycle(cycle);
        joinedSubject.setLastUpdateTime(localDateTime);
        joinedSubject.setLastUpdateUser(lastUpdateUser);
        joinedSubject.setValidationMessage(validationMessage);

        return joinedSubjectRepository.save(joinedSubject);
    }

    @Override
    public JoinedSubject deleteJoinedSubject(String abbreviation) {
        JoinedSubject joinedSubject = joinedSubjectRepository.findById(abbreviation).orElseThrow(() -> new JoinedSubjectNotFoundException(abbreviation));
        joinedSubjectRepository.delete(joinedSubject);
        return joinedSubject;
    }

    @Override
    public List<JoinedSubject> findAllJoinedSubjects() {
        List<JoinedSubject> joinedSubjects = joinedSubjectRepository.findAll();
        return joinedSubjects;
    }

    public Page getAllJoinedSubjects(String name, SemesterType semesterType, StudyCycle cycle, String abbreviation, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Specification<JoinedSubject> spec = JoinedSubjectSpecification.buildSpecification(name, semesterType, cycle, abbreviation);
        return joinedSubjectRepository.findAll(spec, pageable);
    }

    @Override
    @Transactional
    public List<JoinedSubject> importFromList(List<JoinedSubject> tsa) {
        return tsa.stream()
                .map(this::validateAndSave)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private JoinedSubject validateAndSave(JoinedSubject joinedSubject) {
        try {

            if (isInvalid(joinedSubject)) {
                return joinedSubject;
            }

            this.subjectRepository.findById(joinedSubject.getMainSubId())
                    .ifPresent(joinedSubject::setMainSubject);
            joinedSubject.setCycle(StudyCycle.valueOf(joinedSubject.getStudyCycle()));
            joinedSubject.setSemesterType(SemesterType.valueOf(joinedSubject.getSemType()));

            this.joinedSubjectRepository.save(joinedSubject);
            return null;

        } catch (Exception exception) {
            exception.printStackTrace();
            return joinedSubject;
        }
    }

    private boolean mainSubject(JoinedSubject joinedSubject) {
        return joinedSubject.getMainSubId() != null && !joinedSubject.getMainSubId().isEmpty()
                && subjectRepository.findById(joinedSubject.getMainSubId()).isPresent();
    }


    private boolean isInvalid(JoinedSubject joinedSubject) {
        return joinedSubject.getAbbreviation() == null || joinedSubject.getAbbreviation().isEmpty() ||
                joinedSubject.getName() == null || joinedSubject.getName().isEmpty() ||
                joinedSubject.getCodes() == null || joinedSubject.getCodes().isEmpty() ||
                joinedSubject.getSemType() == null || joinedSubject.getSemType().isEmpty() ||
                joinedSubject.getStudyCycle() == null || joinedSubject.getStudyCycle().isEmpty() ||
                joinedSubject.getValidationMessage() == null || joinedSubject.getValidationMessage().isEmpty() ||
                joinedSubject.getMainSubId() == null || joinedSubject.getMainSubId().isEmpty() ||
                subjectRepository.findById(joinedSubject.getMainSubId()).isEmpty();
    }

}
