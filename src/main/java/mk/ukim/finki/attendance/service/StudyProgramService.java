package mk.ukim.finki.attendance.service;

import mk.ukim.finki.attendance.model.exception.studyprogram.StudyProgramCrudException;
import mk.ukim.finki.attendance.model.StudyProgram;
import org.springframework.data.domain.Page;
import java.util.List;

public interface StudyProgramService {

    void create(String code, String name) throws StudyProgramCrudException;

    void update(String code, String newName) throws StudyProgramCrudException;

    StudyProgram findByCode(String code) throws StudyProgramCrudException;

    List<StudyProgram> findAll();

    Page<StudyProgram> getAllStudyPrograms(String code, String name, int page, int size);

    void delete(String code) ;

}
