package mk.ukim.finki.attendance.service.specification;

import mk.ukim.finki.attendance.model.Room;
import mk.ukim.finki.attendance.model.enumeration.RoomType;
import org.springframework.data.jpa.domain.Specification;

public class RoomSpecification {

    public static Specification<Room> buildSpecification(String name, RoomType roomType, Long capacity) {
        return Specification.where(withName(name)).and(withRoomType(roomType)).and(withCapacity(capacity));
    }

    private static Specification<Room> withCapacity(Long capacity) {
        if (capacity == null) {
            return null;
        }
        return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get("capacity"), capacity);
    }

    private static Specification<Room> withRoomType(RoomType roomType) {
        if (roomType == null) {
            return null;
        }
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("roomType"), roomType);
    }

    private static Specification<Room> withName(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        return ((root, query, cb) -> cb.like(cb.lower(root.get("name")), "%" + name.toLowerCase() + "%"));
    }
}
