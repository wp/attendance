package mk.ukim.finki.attendance.service.impl;

import mk.ukim.finki.attendance.model.Semester;
import mk.ukim.finki.attendance.model.YearExamSession;
import mk.ukim.finki.attendance.model.enumeration.ExamSession;
import mk.ukim.finki.attendance.model.exception.yearExamSession.YearExamSessionNotFoundException;
import mk.ukim.finki.attendance.repository.YearExamSessionRepository;
import mk.ukim.finki.attendance.service.YearExamSessionService;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import java.time.LocalDate;
import java.util.List;

@Service
public class YearExamSessionServiceImpl implements YearExamSessionService {
    private final YearExamSessionRepository yearExamSessionRepository;

    public YearExamSessionServiceImpl(YearExamSessionRepository yearExamSessionRepository) {
        this.yearExamSessionRepository = yearExamSessionRepository;
    }

    @Override
    public YearExamSession findById(String id) {
        return yearExamSessionRepository.findById(id).orElseThrow(() -> new YearExamSessionNotFoundException(id));
    }

    @Override
    public List<YearExamSession> listAll() {
        return yearExamSessionRepository.findAll();
    }

    @Override
    public void deleteYearExamSessionById(String id) {
        yearExamSessionRepository.deleteById(id);
    }

    public boolean dateValidation(LocalDate startDate, LocalDate endDate, LocalDate enrollmentStartDate, LocalDate enrollmentEndDate, String year, Semester semester) {
        if (startDate == null || endDate == null || enrollmentStartDate == null || enrollmentEndDate == null ||
                year == null || semester == null) {
            return false;
        }
        return startDate.isAfter(endDate) || startDate.isBefore(enrollmentStartDate)
                || endDate.isBefore(enrollmentStartDate)
                || enrollmentStartDate.isAfter(enrollmentEndDate)
                || startDate.isBefore(enrollmentEndDate)
                || !year.equals(semester.getYear());
    }

    @Override
    public YearExamSession createYearExamSession(ExamSession session,
                                                 Semester semester,
                                                 LocalDate startDate,
                                                 LocalDate endDate,
                                                 LocalDate enrollmentStartDate,
                                                 LocalDate enrollmentEndDate,
                                                 String year) {
        YearExamSession yearExamSession = new YearExamSession(session, semester, startDate, endDate, enrollmentStartDate, enrollmentEndDate, year);

        if (dateValidation(startDate, endDate, enrollmentStartDate, enrollmentEndDate, year, semester)) {
            throw new IllegalArgumentException("Датите не се совпаѓаат");
        }

        return yearExamSessionRepository.save(yearExamSession);
    }

    @Override
    public YearExamSession updateYearExamSession(String name,
                                                 ExamSession session,
                                                 Semester semester,
                                                 LocalDate startDate,
                                                 LocalDate endDate,
                                                 LocalDate enrollmentStartDate,
                                                 LocalDate enrollmentEndDate,
                                                 String year) {
        YearExamSession yearExamSession = yearExamSessionRepository.findById(name).orElseThrow(() -> new YearExamSessionNotFoundException(name));

        if (dateValidation(startDate, endDate, enrollmentStartDate, enrollmentEndDate, year, semester)) {
            throw new IllegalArgumentException("Датите не се совпаѓаат");
        }
        if (session.equals(yearExamSession.getSession()) || !year.equals(yearExamSession.getYear())) {
            yearExamSessionRepository.deleteById(name);
            return createYearExamSession(session, semester, startDate, endDate, enrollmentStartDate, enrollmentEndDate, year);
        } else {
            yearExamSession.setSession(session);
            yearExamSession.setSemester(semester);
            yearExamSession.setStartDate(startDate);
            yearExamSession.setEndDate(endDate);
            yearExamSession.setEnrollmentStartDate(enrollmentStartDate);
            yearExamSession.setEnrollmentEndDate(enrollmentEndDate);
            yearExamSession.setYear(year);
            return yearExamSessionRepository.save(yearExamSession);
        }
    }

    @Override
    public Page<YearExamSession> getAllExamSessionServices(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);

        return yearExamSessionRepository.findAll(pageable);
    }

}
