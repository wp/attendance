package mk.ukim.finki.attendance.service;

import mk.ukim.finki.attendance.model.Room;
import mk.ukim.finki.attendance.model.enumeration.RoomType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface RoomService {

    Room findById(String name);

    List<Room> findAll();

    Optional<Room> deleteOptionalById(String name);

    void deleteRoomById(String name);

    Room createRoom(String name, Long capacity, String equipmentDescription, String locationDescription, RoomType roomType);

    Room updateRoom(String name, Long capacity, String equipmentDescription, String locationDescription, RoomType roomType);

    Page<Room> getFilteredRooms(String name, RoomType roomType, Long capacity, Pageable pageable);

    Page<Room> getAllRooms(int page, int size);

    Page<Room> findAllRoomsSpec(String name, RoomType roomType, Long capacity, int page, int size);

    List<Room> importFromList(List<Room> rooms);
}
