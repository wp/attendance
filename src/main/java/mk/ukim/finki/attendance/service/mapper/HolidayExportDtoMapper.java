package mk.ukim.finki.attendance.service.mapper;

import mk.ukim.finki.attendance.model.DTO.HolidayExportDto;
import mk.ukim.finki.attendance.model.Holiday;

public interface HolidayExportDtoMapper {
    HolidayExportDto mapHolidayToExportDto(Holiday holiday);
}
