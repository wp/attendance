package mk.ukim.finki.attendance.service;

import mk.ukim.finki.attendance.model.Holiday;
import mk.ukim.finki.attendance.model.Student;
import mk.ukim.finki.attendance.model.StudyProgram;
import mk.ukim.finki.attendance.model.exception.student.StudentCrudException;
import org.springframework.data.domain.Page;

import java.util.List;

public interface StudentService {

    List<Student> importFromList(List<Student> students);

    Student findByIndex(String index) throws StudentCrudException;

    List<Student> findAll();

    void create(String index, String email, String lastName, String name, String parentName, String studyProgramCode) throws StudentCrudException;

    void update(String index, String newEmail, String newLastName, String newName, String parentName, String studyProgramCode);

    Page<Student> getAllStudents(int page, int size);

    void delete(String index);

    Page<Student> findAllStudents(String studentIndex, String name, String lastName,String studyProgram, int page, int size);

}
