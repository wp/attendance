package mk.ukim.finki.attendance.service;

import mk.ukim.finki.attendance.model.Subject;
import mk.ukim.finki.attendance.model.enumeration.SemesterType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface SubjectService {

    List<Subject> listAll();

    Page<Subject> findAll(Pageable pageable);

    Subject findById(String id);

    Subject create(String id, String name, SemesterType semester, Integer weeklyAuditoriumClasses, Integer weeklyLabClasses, Integer weeklyLecturesClasses, String abbreviation);

    Subject update(String id, String name, SemesterType semester, Integer weeklyAuditoriumClasses, Integer weeklyLabClasses, Integer weeklyLecturesClasses, String abbreviation);

    void delete(String id);

    Page<Subject> filterByOptions(String name, SemesterType semester, String abbreviation, Pageable pageable);

    List<Subject> importData(List<Subject> subjects);

}
