package mk.ukim.finki.attendance.service;

import mk.ukim.finki.attendance.model.DTO.HolidayExportDto;
import mk.ukim.finki.attendance.model.Holiday;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;

public interface HolidayService {
    List<Holiday> importData(List<Holiday> holidays);
    List<Holiday> listAllHolidays();
    Holiday findHolidayById(LocalDate date);
    Holiday createHoliday(LocalDate date, String name);
    Holiday updateHoliday(LocalDate date, String name);
    Holiday deleteHoliday(LocalDate date);
    List<Holiday> findAllByYearAndMonth(Integer year, Integer month);
    Page<Holiday> getAllHolidays(Integer year, Integer month, Pageable pageable);
    List<HolidayExportDto> exportHolidays(Integer year, Integer month);
}
