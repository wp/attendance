package mk.ukim.finki.attendance.service;

import mk.ukim.finki.attendance.model.Holiday;
import mk.ukim.finki.attendance.model.Semester;
import mk.ukim.finki.attendance.model.enumeration.SemesterState;
import mk.ukim.finki.attendance.model.enumeration.SemesterType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.util.List;

public interface SemesterService {

    Page<Semester> getAllSem(int page, int size);

    List<Semester> listAllSemesters();

    Semester getSemesterById(String code);

    void deleteSemester(String code);

    Semester updateSemester(String code, String year, SemesterType semesterType, LocalDate startDate, LocalDate end,
                        LocalDate enrollmentStartDate, LocalDate enrollmentEndDate, SemesterState semesterState);

    Semester addSemester(String code, String year, SemesterType semesterType, LocalDate startDate, LocalDate end,
                         LocalDate enrollmentStartDate, LocalDate enrollmentEndDate, SemesterState semesterState);

    Page<Semester> getAllSemesters(Specification<Semester> specification, Pageable pageable);

    List<Semester> importData(List<Semester> semesters);


}
