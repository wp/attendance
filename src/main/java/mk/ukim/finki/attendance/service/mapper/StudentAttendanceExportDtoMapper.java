package mk.ukim.finki.attendance.service.mapper;

import mk.ukim.finki.attendance.model.dto.StudentAttendanceExportDTO;
import mk.ukim.finki.attendance.model.StudentAttendance;

public interface StudentAttendanceExportDtoMapper {
    StudentAttendanceExportDTO mapStudentAttendanceExportDTOList(StudentAttendance attendance);
}
