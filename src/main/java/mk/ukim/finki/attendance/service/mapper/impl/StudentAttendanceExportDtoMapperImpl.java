package mk.ukim.finki.attendance.service.mapper.impl;

import mk.ukim.finki.attendance.model.dto.StudentAttendanceExportDTO;
import mk.ukim.finki.attendance.model.StudentAttendance;
import mk.ukim.finki.attendance.service.mapper.StudentAttendanceExportDtoMapper;
import org.springframework.stereotype.Service;

@Service
public class StudentAttendanceExportDtoMapperImpl implements StudentAttendanceExportDtoMapper {

    public StudentAttendanceExportDTO mapStudentAttendanceExportDTOList(StudentAttendance attendance) {

            StudentAttendanceExportDTO dto = new StudentAttendanceExportDTO();
            dto.setIndex(attendance.getStudent().getStudentIndex());
            dto.setStudentName(attendance.getStudent().getName());
            dto.setJoinedSubjectAbbreviation(attendance.getProfessorClassSession().getScheduledClassSession().getCourse().getJoinedSubject().getAbbreviation());
            dto.setJoinedSubjectName(attendance.getProfessorClassSession().getScheduledClassSession().getCourse().getJoinedSubject().getName());
            dto.setCourseEnglish(attendance.getProfessorClassSession().getScheduledClassSession().getCourse().getEnglish().toString());
            dto.setSemesterCode(attendance.getProfessorClassSession().getScheduledClassSession().getCourse().getSemester().getCode());
            dto.setScheduledClassSessionRoom(attendance.getProfessorClassSession().getScheduledClassSession().getRoom().getName());
            dto.setScheduledClassSessionType(attendance.getProfessorClassSession().getScheduledClassSession().getClassType().toString());
            dto.setDate(attendance.getProfessorClassSession().getDate());
            dto.setStartTime(attendance.getProfessorClassSession().getScheduledClassSession().getStartTime());
            dto.setEndTime(attendance.getProfessorClassSession().getScheduledClassSession().getEndTime());
            dto.setStudentArrivalTime(attendance.getArrivalTime());

            return dto;
    }
}
