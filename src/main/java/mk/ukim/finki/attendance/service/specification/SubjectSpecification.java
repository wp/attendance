package mk.ukim.finki.attendance.service.specification;

import mk.ukim.finki.attendance.model.Subject;
import mk.ukim.finki.attendance.model.enumeration.SemesterType;
import org.springframework.data.jpa.domain.Specification;

public class SubjectSpecification {

    public static Specification<Subject> buildSpecification(String name, SemesterType semester, String abbreviation) {
        return Specification.where(withName(name))
                .and(withAbbreviation(abbreviation))
                .and(withSemester(semester));
    }

    private static Specification<Subject> withName(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        return (root, query, cb) -> cb.like(cb.lower(root.get("name")), "%" + name.toLowerCase() + "%");
    }

    private static Specification<Subject> withAbbreviation(String abbreviation) {
        if (abbreviation == null || abbreviation.isEmpty()) {
            return null;
        }
        return (root, query, cb) -> cb.like(cb.lower(root.get("abbreviation")), "%" + abbreviation.toLowerCase() + "%");
    }

    private static Specification<Subject> withSemester(SemesterType semester) {
        if (semester == null) {
            return null;
        }

        return (root, query, cb) -> cb.equal(root.get("semester"), semester);
    }


}
