package mk.ukim.finki.attendance.service.mapper;

import mk.ukim.finki.attendance.model.ScheduledClassSession;
import mk.ukim.finki.attendance.model.dto.ScheduledClassSessionExportDTO;

public interface ScheduledClassSessionExportDtoMapper {
    ScheduledClassSessionExportDTO mapScheduledClassSessionToExportDto(ScheduledClassSession scs);
}
