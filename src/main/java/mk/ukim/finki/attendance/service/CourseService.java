package mk.ukim.finki.attendance.service;

import mk.ukim.finki.attendance.model.Course;
import mk.ukim.finki.attendance.model.JoinedSubject;
import mk.ukim.finki.attendance.model.Professor;
import mk.ukim.finki.attendance.model.Semester;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CourseService {

    List<Course> listAllCourses();

    Course findCourseById(Long id);

    Course saveCourse(Short studyYear, String lastNameRegex, JoinedSubject joinedSubject, Semester semester, Professor professor, Professor assistant, Integer numberOfFirstEnrollments, Integer numberOfReEnrollments, Double groupPortion, List<String> professors, List<String> groups, List<String> assistants, Boolean english);

    Course editCourse(Long id, Short studyYear, String lastNameRegex, JoinedSubject joinedSubject, Semester semester, Professor professor, Professor assistant, Integer numberOfFirstEnrollments, Integer numberOfReEnrollments, Double groupPortion, List<String> professors, List<String> groups, List<String> assistants, Boolean english);

    Course deleteCourse(Long id);

    Course findCourseByCode(String code);

    Page<Course> listAllSemesters(int page, int size, Short studyYear, String semester, Boolean english, String joinedSubject,
                                  String professor, String assistant);

    Page<Course> getAllCourses(Integer page, Integer size, Short studyYear, String semesterId, Boolean english, String joinedSubjectId, String professorId, String assistantId);
}
