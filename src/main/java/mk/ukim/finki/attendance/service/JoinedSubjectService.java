package mk.ukim.finki.attendance.service;

import mk.ukim.finki.attendance.model.*;
import mk.ukim.finki.attendance.model.enumeration.SemesterType;
import mk.ukim.finki.attendance.model.enumeration.StudyCycle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface JoinedSubjectService {

    JoinedSubject createJoinedSubject(String abbreviation, String name, String codes, SemesterType semesterType, String mainSubjectId, Integer weeklyAuditoriumClasses, Integer weeklyLabClasses, Integer weeklyLecturesClasses, StudyCycle cycle, AuthUser lastUpdateUser, String validationMessage);

    JoinedSubject readJoinedSubject(String abbreviation);

    JoinedSubject updateJoinedSubject(String abbreviation, String name, String codes, SemesterType semesterType, String mainSubjectId, Integer weeklyAuditoriumClasses, Integer weeklyLabClasses, Integer weeklyLecturesClasses, StudyCycle cycle, AuthUser lastUpdateUser, String validationMessage);

    JoinedSubject deleteJoinedSubject(String abbreviation);

    List<JoinedSubject> findAllJoinedSubjects();

    Page<JoinedSubject> getAllJoinedSubjects(String name, SemesterType semesterType, StudyCycle cycle, String abbreviation, int page, int size);

    List<JoinedSubject> importFromList(List<JoinedSubject> tsa);

}
