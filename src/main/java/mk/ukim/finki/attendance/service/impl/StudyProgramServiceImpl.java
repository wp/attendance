package mk.ukim.finki.attendance.service.impl;

import mk.ukim.finki.attendance.model.exception.studyprogram.StudyProgramAlreadyExistsException;
import mk.ukim.finki.attendance.model.exception.studyprogram.StudyProgramInvalidArgumentException;
import mk.ukim.finki.attendance.model.exception.studyprogram.StudyProgramNotFoundException;
import mk.ukim.finki.attendance.model.StudyProgram;
import mk.ukim.finki.attendance.repository.StudyProgramRepository;
import mk.ukim.finki.attendance.service.StudyProgramService;
import mk.ukim.finki.attendance.service.specification.StudyProgramSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudyProgramServiceImpl implements StudyProgramService {

    private final StudyProgramRepository repository;

    public StudyProgramServiceImpl(StudyProgramRepository repository) {
        this.repository = repository;
    }


    @Override
    public void create(String code, String name) throws
            StudyProgramAlreadyExistsException,
            StudyProgramInvalidArgumentException {
        if (code.isBlank() || name.isBlank()) {
            throw new StudyProgramInvalidArgumentException("StudyProgram code can't be empty");
        }
        if (repository.existsByCode(code)) {
            throw new StudyProgramAlreadyExistsException(
                    String.format("Can't create StudyProgram with code %s because a StudyProgram with that " +
                            "code already exists", code));
        }
        StudyProgram toCreate = new StudyProgram(code, name);
        repository.save(toCreate);
    }

    @Override
    public void update(String code, String newName) throws StudyProgramInvalidArgumentException{
        if ( newName.isBlank()) {
            throw new StudyProgramInvalidArgumentException("StudyProgram code can't be empty");
        }
        StudyProgram toUpdate = findByCode(code);
        toUpdate.setName(newName);
        repository.save(toUpdate);
    }

    @Override
    public StudyProgram findByCode(String code) throws StudyProgramNotFoundException {
        return repository.findByCode(code).orElseThrow(() ->
                new StudyProgramNotFoundException(String.format("StudyProgram with code %s not found", code)));
    }

    @Override
    public List<StudyProgram> findAll() {
        return repository.findAll();
    }

    @Override
    public Page<StudyProgram> getAllStudyPrograms(String code, String name, int page, int size) {
        Specification<StudyProgram> specification = StudyProgramSpecification.buildSpecification(code, name);
        Pageable pageable = PageRequest.of(page, size);

        return this.repository.findAll(specification, pageable);
    }

    @Override
    public void delete(String code) {
        StudyProgram toDelete = findByCode(code);
        repository.delete(toDelete);
    }
}
