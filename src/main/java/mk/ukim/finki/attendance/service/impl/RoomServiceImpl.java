package mk.ukim.finki.attendance.service.impl;

import mk.ukim.finki.attendance.model.Room;
import mk.ukim.finki.attendance.model.enumeration.RoomType;
import mk.ukim.finki.attendance.model.exception.room.NoRoomFoundException;
import mk.ukim.finki.attendance.model.exception.room.RoomNotFoundException;
import mk.ukim.finki.attendance.model.exception.room.RoomSaveNotValidException;
import mk.ukim.finki.attendance.model.exception.room.RoomUpdateNotValidException;
import mk.ukim.finki.attendance.repository.RoomRepository;
import mk.ukim.finki.attendance.service.RoomService;
import mk.ukim.finki.attendance.service.specification.RoomSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RoomServiceImpl implements RoomService {

    private final RoomRepository roomRepository;

    public RoomServiceImpl(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Override
    public Room findById(String name) {
        return this.roomRepository.findById(name).orElseThrow(RoomNotFoundException::new);
    }

    @Override
    public List<Room> findAll() {
        return this.roomRepository.findAll();
    }

    @Override
    public Optional<Room> deleteOptionalById(String name) {
        Optional<Room> room = this.roomRepository.findById(name);
        if (room.isEmpty()) {
            throw new NoRoomFoundException(name);
        }
        this.roomRepository.deleteById(name);
        return room;
    }

    @Override
    public void deleteRoomById(String name) {
        this.roomRepository.deleteById(name);
    }

    @Override
    @Transactional
    public Room createRoom(String name, Long capacity, String equipmentDescription, String locationDescription, RoomType roomType) {
        Room room = new Room();
        room.setName(name);
        room.setCapacity(capacity);
        room.setEquipmentDescription(equipmentDescription);
        room.setLocationDescription(locationDescription);
        room.setRoomType(roomType);
        try {
            return this.roomRepository.save(room);
        } catch (Exception exception) {
            throw new RoomSaveNotValidException();
        }
    }

    @Override
    @Transactional
    public Room updateRoom(String name, Long capacity, String equipmentDescription, String locationDescription, RoomType roomType) {
        Room room = this.roomRepository.findById(name).orElseThrow(() -> new NoRoomFoundException(name));
//        room.setName(name);
        room.setCapacity(capacity);
        room.setRoomType(roomType);
        room.setEquipmentDescription(equipmentDescription);
        room.setLocationDescription(locationDescription);
        try {
            return this.roomRepository.save(room);
        } catch (Exception exception) {
            throw new RoomUpdateNotValidException();
        }
    }

    @Override
    public Page<Room> getAllRooms(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return this.roomRepository.findAll(pageable);
    }

    @Override
    public Page<Room> findAllRoomsSpec(String name, RoomType roomType, Long capacity, int page, int size) {
        Specification<Room> specification = RoomSpecification.buildSpecification(name, roomType, capacity);
        Pageable pageable = PageRequest.of(page, size);
        return this.roomRepository.findAll(specification, pageable);
    }

    public Page<Room> getFilteredRooms(String name, RoomType roomType, Long capacity, Pageable pageable) {
        return roomRepository.findRooms(name, roomType, capacity, pageable);
    }

    @Override
    public List<Room> importFromList(List<Room> rooms) {
        return rooms.stream()
                .map(room -> {
                    try {
                        if (isValidRoom(room)) {
                            room.setRoomType(RoomType.valueOf(room.getRoomTypeName()));
                            roomRepository.save(room);
                            return null;
                        } else {
                            return room;
                        }
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                        return room;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private boolean isValidRoom(Room room) {
        return room.getName() != null && !room.getName().isEmpty() &&
                room.getCapacity() != null &&
                room.getRoomTypeName() != null && !room.getRoomTypeName().isEmpty() && !roomRepository.existsById(room.getName());
    }
}
