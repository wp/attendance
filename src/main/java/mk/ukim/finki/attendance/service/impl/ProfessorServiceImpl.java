package mk.ukim.finki.attendance.service.impl;

import mk.ukim.finki.attendance.model.Professor;
import mk.ukim.finki.attendance.model.Room;
import mk.ukim.finki.attendance.model.enumeration.ProfessorTitle;
import mk.ukim.finki.attendance.model.exception.professor.ProfessorNotFoundException;
import mk.ukim.finki.attendance.repository.ProfessorRepository;
import mk.ukim.finki.attendance.repository.RoomRepository;
import mk.ukim.finki.attendance.service.ProfessorService;
import mk.ukim.finki.attendance.service.specification.ProfessorSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ProfessorServiceImpl implements ProfessorService {

    private final ProfessorRepository professorRepository;
    private final RoomRepository roomRepository;

    public ProfessorServiceImpl(ProfessorRepository professorRepository, RoomRepository roomRepository) {
        this.professorRepository = professorRepository;
        this.roomRepository = roomRepository;
    }


    @Override
    public List<Professor> importProfessor(List<Professor> professors) {
        return professors.stream()
                .map(it -> {
                    try {
                        checkIfValidProfessor(it);
                        it.setId(it.getId());
                        it.setEmail(it.getEmail());
                        it.setName(it.getName());
                        it.setTitle(ProfessorTitle.valueOf(it.getTitleName()));
                        it.setOrderingRank(it.getOrderingRank());
                        it.setRoom(roomRepository.findByName(it.getRoomName())
                                .orElseThrow(() -> new RuntimeException("Room not found: " + it.getRoom().getName())));
                        professorRepository.save(it);
                        return null;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return it;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public void checkIfValidProfessor(Professor it) {
        if (it.getId() == null ||
                it.getId().isBlank() ||
                it.getEmail() == null ||
                it.getEmail().isBlank() ||
                it.getName() == null ||
                it.getName().isBlank() ||
                it.getOrderingRank() == null ||
                it.getTitleName() == null ||
                it.getTitleName().isBlank() ||
                it.getRoomName() == null ||
                it.getRoomName().isBlank()
        ) {
            throw new RuntimeException();
        }

    }

    @Override
    public Page<Professor> getAllProfessors(Specification<Professor> specification, Pageable pageable) {
        return professorRepository.findAll(specification, pageable);
    }

    @Override
    public List<Professor> listAllProfessors() {
        return this.professorRepository.findAll();
    }

    @Override
    public Professor findProfessorById(String id) {
        return this.professorRepository.findById(id).orElseThrow(() -> new ProfessorNotFoundException(id));
    }

    @Override
    public Professor createProfessor(String email, String name, ProfessorTitle title, Short orderingRank, Room room) {
        Professor professor = new Professor();
        String id = email.split("@")[0];
        professor.setId(id);
        professor.setEmail(email);
        professor.setName(name);
        professor.setTitle(title);
        professor.setOrderingRank(orderingRank);
        professor.setRoom(room);

        return this.professorRepository.save(professor);
    }

    @Override
    public Professor updateProfessor(String id, String email, String name, ProfessorTitle title, Short orderingRank, Room room) {
        Professor professor = this.findProfessorById(id);
        professor.setTitle(title);
        professor.setName(name);
        professor.setTitle(title);
        professor.setOrderingRank(orderingRank);
        professor.setRoom(room);

        return this.professorRepository.save(professor);
    }

    @Override
    public Professor deleteProfessor(String id) {
        Professor professor = this.findProfessorById(id);
        this.professorRepository.delete(professor);

        return professor;
    }

    @Override
    public Page<Professor> findAllByEmailandNameandTitle(String email, String name, ProfessorTitle title, Pageable pageable) {
        Specification<Professor> specification = ProfessorSpecification.buildSpecification(email, name, title);
        return this.professorRepository.findAll(specification, pageable);
    }

}
