package mk.ukim.finki.attendance.service;

import mk.ukim.finki.attendance.model.dto.ProfessorAttendanceSingleExportDTO;
import mk.ukim.finki.attendance.model.ProfessorClassSession;
import mk.ukim.finki.attendance.model.dto.ProfessorAttendanceExportDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ProfessorClassSessionService {

    Optional<ProfessorClassSession> findById(Long id);

    Optional<ProfessorClassSession> deleteById(Long id);

    Page<ProfessorClassSession> getAllProfessorClassSessions(int page, int size);

    List<ProfessorClassSession> findAll();

    ProfessorClassSession createProfessorClassSession(String professor, Long scheduledClassSession,
                                                                LocalDate date, LocalDateTime professorArrivalTime,
                                                                String attendanceToken);

    ProfessorClassSession updateProfessorClassSession(Long id, String professor, Long scheduledClassSession,
                                                                LocalDate date, LocalDateTime professorArrivalTime,
                                                                String attendanceToken);

    Page<ProfessorClassSession> filterByOptions(String professor, LocalDate from, LocalDate to, Pageable pageable);

    List<ProfessorClassSession> importData(List<ProfessorClassSession> professorClassSessions);

    void generateProfessorClassSessions();

    List<ProfessorAttendanceExportDto> getProfessorAttendance(String professorId, Long courseId);

    List<ProfessorAttendanceSingleExportDTO> getProfessorAttendance(Long id);
}
