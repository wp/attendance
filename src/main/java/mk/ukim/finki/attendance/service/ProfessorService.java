package mk.ukim.finki.attendance.service;

import mk.ukim.finki.attendance.model.Professor;
import mk.ukim.finki.attendance.model.Room;
import mk.ukim.finki.attendance.model.enumeration.ProfessorTitle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface ProfessorService {

    Page<Professor> getAllProfessors(Specification<Professor> specification, Pageable pageable);

    List<Professor> listAllProfessors();

    Professor findProfessorById(String id);

    Professor updateProfessor(String id, String email, String name, ProfessorTitle title, Short orderingRank, Room room);

    Professor createProfessor(String email, String name, ProfessorTitle title, Short orderingRank, Room room);

    Professor deleteProfessor(String id);

    List<Professor> importProfessor(List<Professor> professors);

    Page<Professor> findAllByEmailandNameandTitle(String email, String name, ProfessorTitle title, Pageable pageable);
}
