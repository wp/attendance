package mk.ukim.finki.attendance.service;

import mk.ukim.finki.attendance.model.Course;
import mk.ukim.finki.attendance.model.Room;
import mk.ukim.finki.attendance.model.ScheduledClassSession;
import mk.ukim.finki.attendance.model.dto.ScheduledClassSessionExportDTO;
import mk.ukim.finki.attendance.model.enumeration.ClassType;
import mk.ukim.finki.attendance.model.enumeration.RoomType;
import org.springframework.data.domain.Page;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

public interface ScheduledClassSessionService {

    Page<ScheduledClassSession> findAllFilteredAndPaged(String dayOfWeekName, String roomName, String classTypeName, Long courseId, int page, int size);

    Optional<ScheduledClassSession> findById(Long id);

    List<ScheduledClassSession> listAll();

    Optional<ScheduledClassSession> deleteOptionalById(Long id);

    void deleteScheduledClassSessionById(Long id);

    ScheduledClassSession createScheduledClassSession(LocalTime startTime, DayOfWeek dayOfWeek, LocalTime endTime, Room room, ClassType classType, Course course);

    ScheduledClassSession updateScheduledClassSession(Long id, LocalTime startTime, DayOfWeek dayOfWeek, LocalTime endTime, Room room, ClassType classType, Course course);

    Page<ScheduledClassSession> getAllScheduledClassSessions(int page, int size);

    Page<ScheduledClassSession> findAllScheduledClassSessionSpec(DayOfWeek dayOfWeekName, String roomName, ClassType classType, Long courseId, int page, int size);

    List<ScheduledClassSession> importFromList(List<ScheduledClassSession> scheduledClassSessions);

    List<ScheduledClassSessionExportDTO> exportScheduledClassSessions(DayOfWeek dayOfWeek, String roomId, Long courseId, ClassType classTypeName);
}
