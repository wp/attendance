package mk.ukim.finki.attendance.service.specification;

import jakarta.persistence.criteria.Expression;
import mk.ukim.finki.attendance.model.Holiday;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.time.YearMonth;

public class HolidaySpecification {

    public static Specification<Holiday> buildSpecification(Integer year, Integer month) {
        return Specification.where(withYear(year))
                .and(withMonth(year, month));
    }

    private static Specification<Holiday> withYear(Integer year) {
        if (year == null) {
            return null;
        }
        LocalDate startDate = LocalDate.of(year, 1, 1);
        LocalDate endDate = LocalDate.of(year, 12, 31);
        return (root, query, cb) -> cb.between(root.get("date"), startDate, endDate);
    }

    private static Specification<Holiday> withMonth(Integer year, Integer month) {
        if (month == null || year == null) {
            return null;
        }
        LocalDate startOfMonth = LocalDate.of(year, month, 1);
        LocalDate endOfMonth = startOfMonth.withDayOfMonth(startOfMonth.lengthOfMonth());

        return (root, query, cb) -> cb.between(root.get("date"), startOfMonth, endOfMonth);
    }
}
