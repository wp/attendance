package mk.ukim.finki.attendance.service.impl;

import mk.ukim.finki.attendance.model.Student;
import mk.ukim.finki.attendance.model.StudyProgram;
import mk.ukim.finki.attendance.model.exception.student.InvalidStudentException;
import mk.ukim.finki.attendance.model.exception.student.StudentAlreadyExistsException;
import mk.ukim.finki.attendance.model.exception.student.StudentNotFoundException;
import mk.ukim.finki.attendance.model.exception.studyprogram.StudyProgramInvalidArgumentException;
import mk.ukim.finki.attendance.model.exception.studyprogram.StudyProgramNotFoundException;
import mk.ukim.finki.attendance.repository.StudentRepository;
import mk.ukim.finki.attendance.repository.StudyProgramRepository;
import mk.ukim.finki.attendance.service.StudentService;
import mk.ukim.finki.attendance.service.StudyProgramService;
import mk.ukim.finki.attendance.service.specification.StudentSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {
    private final StudentRepository repository;
    private final StudyProgramService studyProgramService;
    private final StudyProgramRepository studyProgramRepository;

    public StudentServiceImpl(StudentRepository studentRepository, StudyProgramService studyProgramService, StudyProgramRepository studyProgramRepository) {
        this.repository = studentRepository;
        this.studyProgramService = studyProgramService;
        this.studyProgramRepository = studyProgramRepository;
    }

    @Override
    public List<Student> importFromList(List<Student> students) {
        return students.stream()
                .map(it -> {
                    try {
                        if (isValidStudent(it) && isAlreadyTaken(it.getEmail(), it.getStudentIndex())) {
                            it.setStudyProgram(studyProgramRepository.getReferenceByCode(it.getStudyProgramCode()));
                            repository.save(it);
                            return null;
                        }
                        else {
                            return it;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        return it;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private boolean isValidStudent(Student student){
        return student.getStudentIndex() != null && !student.getStudentIndex().isEmpty() &&
                student.getName() != null && !student.getName().isEmpty() &&
                student.getLastName() != null && !student.getLastName().isEmpty() &&
                student.getEmail() != null && !student.getEmail().isEmpty() &&
                student.getParentName() != null && !student.getParentName().isEmpty();
    }

    private boolean isAlreadyTaken(String email, String id){
        return !repository.existsByStudentIndex(id) && !repository.existsByEmail(email);
    }

    @Override
    public Student findByIndex(String index) throws StudentNotFoundException {
        return repository.findByStudentIndex(index).orElseThrow(() ->
                new StudentNotFoundException(String.format("Student with index %s not found", index)));
    }

    @Override
    public List<Student> findAll() {
        return repository.findAll();
    }

    @Override
    public void create(String index, String email, String lastName, String name, String parentName, String studyProgramCode) throws
            InvalidStudentException,
            StudyProgramNotFoundException {
        if (index.isBlank() || email.isBlank() || lastName.isBlank() || name.isBlank() || parentName.isBlank()) {
            throw new InvalidStudentException("All fields must be filled!");
        }
        if (studyProgramCode.isBlank()) {
            throw new StudyProgramInvalidArgumentException(String.format("Study Program code can't be blank for student with index: %s!", index));
        }

        if (repository.existsByStudentIndex(index)) {
            throw new StudentAlreadyExistsException(
                    String.format("Can't create Student with index %s because a Student with that " +
                            "index already exists", index));
        }

        StudyProgram studyProgram = studyProgramService.findByCode(studyProgramCode);

        Student toCreate = new Student(index, email, lastName, name, parentName, studyProgram);
        repository.save(toCreate);
    }

    @Override
    public void update(String index, String newEmail, String newLastName, String newName, String newParentName, String studyProgramCode) throws
            InvalidStudentException,
            StudyProgramNotFoundException {
        if (index.isBlank() || newEmail.isBlank() || newLastName.isBlank() || newName.isBlank() || newParentName.isBlank()) {
            throw new InvalidStudentException("All fields must be filled!");
        }
        if (studyProgramCode.isBlank()) {
            throw new StudyProgramInvalidArgumentException(String.format("Study Program code can't be blank for student with index: %s!", index));
        }

        StudyProgram studyProgram = studyProgramService.findByCode(studyProgramCode);

        Student toUpdate = findByIndex(index);
        toUpdate.setEmail(newEmail);
        toUpdate.setLastName(newLastName);
        toUpdate.setName(newName);
        toUpdate.setParentName(newParentName);
        toUpdate.setStudyProgram(studyProgram);
        repository.save(toUpdate);

    }

    @Override
    public Page<Student> getAllStudents(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return this.repository.findAll(pageable);
    }

    @Override
    public void delete(String index) {
        Student toDelete = findByIndex(index);
        repository.delete(toDelete);
    }

    @Override
    public Page<Student> findAllStudents(String studentIndex, String name, String lastName, String studyProgram, int page, int size) {
        Specification<Student> specification = StudentSpecification.buildSpecification(studentIndex, name,lastName,studyProgram);
        Pageable pageable = PageRequest.of(page, size);
        return this.repository.findAll(specification, pageable);
    }
}
