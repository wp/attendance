package mk.ukim.finki.attendance.service;

import mk.ukim.finki.attendance.model.ProfessorClassSession;
import mk.ukim.finki.attendance.model.Semester;
import mk.ukim.finki.attendance.model.dto.StudentAttendanceExportDTO;
import mk.ukim.finki.attendance.model.StudentAttendance;
import mk.ukim.finki.attendance.model.dto.StudentAttendanceDTO;
import mk.ukim.finki.attendance.model.exception.studentattendance.StudentAttendanceNotFoundException;
import org.springframework.data.domain.Page;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface StudentAttendanceService {

    void create(String studentIndex, Long professorClassSessionId, LocalDateTime arrivalTime) throws StudentAttendanceNotFoundException;

    void update(Long id, String studentIndex, Long professorClassSessionId, LocalDateTime arrivalTime) throws StudentAttendanceNotFoundException;

    void delete(Long id);

    StudentAttendance findById(Long id);

    List<StudentAttendance> findAll();

    Page<StudentAttendance> getAllAttendingStudents(int page, int size);

    Page<StudentAttendance> getAllStudentsSpecification(String student, int page, int size);

    List<StudentAttendance> importStudentAttendance(List<StudentAttendance> attendances);

    List<StudentAttendanceExportDTO> exportStudentAttendance(String studentIndex,
                                                             Long courseId,
                                                             String semesterCode,
                                                             LocalDate dateFrom,
                                                             LocalDate dateTo,
                                                             Semester activeSemester);


    List<StudentAttendanceDTO> studentAttendancesExport(String studentIndex, String semesterCode);

    List<StudentAttendance> findAllByProfessorClassSession(ProfessorClassSession session);
}
