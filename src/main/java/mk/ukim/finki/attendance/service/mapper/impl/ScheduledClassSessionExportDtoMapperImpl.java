package mk.ukim.finki.attendance.service.mapper.impl;

import mk.ukim.finki.attendance.model.ScheduledClassSession;
import mk.ukim.finki.attendance.model.dto.ScheduledClassSessionExportDTO;
import mk.ukim.finki.attendance.model.enumeration.ClassType;
import mk.ukim.finki.attendance.service.mapper.ScheduledClassSessionExportDtoMapper;
import org.springframework.stereotype.Service;

@Service
public class ScheduledClassSessionExportDtoMapperImpl implements ScheduledClassSessionExportDtoMapper {
    @Override
    public ScheduledClassSessionExportDTO mapScheduledClassSessionToExportDto(ScheduledClassSession scs) {
        ScheduledClassSessionExportDTO dto = new ScheduledClassSessionExportDTO();

        dto.setStartTime(scs.getStartTime());
        dto.setEndTime(scs.getEndTime());
        dto.setDayOfWeek(scs.getDayOfWeek());
        dto.setRoomId(scs.getRoom().getName());
        dto.setCourseId(scs.getCourse().getJoinedSubject().getAbbreviation());
        dto.setClassTypeName(scs.getClassType().name());
        return dto;
    }
}
