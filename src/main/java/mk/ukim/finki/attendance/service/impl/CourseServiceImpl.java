package mk.ukim.finki.attendance.service.impl;

import mk.ukim.finki.attendance.model.Course;
import mk.ukim.finki.attendance.model.JoinedSubject;
import mk.ukim.finki.attendance.model.Professor;
import mk.ukim.finki.attendance.model.Semester;
import mk.ukim.finki.attendance.model.exception.course.CourseNotFoundException;
import mk.ukim.finki.attendance.repository.CourseRepository;
import mk.ukim.finki.attendance.repository.JoinedSubjectRepository;
import mk.ukim.finki.attendance.service.CourseService;
import mk.ukim.finki.attendance.service.ProfessorService;
import mk.ukim.finki.attendance.service.SemesterService;
import mk.ukim.finki.attendance.service.specification.CourseSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;
    private final ProfessorService professorService;
    private final SemesterService semesterService;
    private final JoinedSubjectRepository joinedSubjectRepository;

    public CourseServiceImpl(CourseRepository courseRepository, ProfessorService professorService, SemesterService semesterService, JoinedSubjectRepository joinedSubjectRepository) {
        this.courseRepository = courseRepository;
        this.professorService = professorService;
        this.semesterService = semesterService;
        this.joinedSubjectRepository = joinedSubjectRepository;
    }

    @Override
    public List<Course> listAllCourses() {
        return this.courseRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    @Override
    public Course findCourseById(Long id) {
        return this.courseRepository.findById(id).orElseThrow(CourseNotFoundException::new);
    }

    @Override
    public Course findCourseByCode(String code) {
        return this.courseRepository.findByLastNameRegex(code).orElseThrow(CourseNotFoundException::new);
    }

    @Override
    public Course saveCourse(Short studyYear, String lastNameRegex, JoinedSubject joinedSubject, Semester semester, Professor professor, Professor assistant, Integer numberOfFirstEnrollments, Integer numberOfReEnrollments, Double groupPortion, List<String> professors, List<String> groups, List<String> assistants, Boolean english) {
        professors.add(professor.getId());
        assistants.add(assistant.getId());
        String professorsString = String.join(";", professors);
        String assistantsString = String.join(";", assistants);
        String groupsString = String.join(";", groups);
        if (english != null) {
            english = true;
        } else {
            english = false;
        }
        Course course = new Course(studyYear, lastNameRegex, joinedSubject, semester, professor, assistant, numberOfFirstEnrollments, numberOfReEnrollments, groupPortion, professorsString, groupsString, assistantsString, english);
        return this.courseRepository.save(course);
    }

    @Override
    public Course editCourse(Long id, Short studyYear, String lastNameRegex, JoinedSubject joinedSubject, Semester semester, Professor professor, Professor assistant, Integer numberOfFirstEnrollments, Integer numberOfReEnrollments, Double groupPortion, List<String> professors, List<String> groups, List<String> assistants, Boolean english) {
        Course course = this.courseRepository.findById(id).orElseThrow(CourseNotFoundException::new);
        String professorsString = String.join(";", professors);
        String assistantsString = String.join(";", assistants);
        course.setStudyYear(studyYear);
        course.setLastNameRegex(lastNameRegex);
        course.setJoinedSubject(joinedSubject);
        course.setSemester(semester);
        course.setProfessor(professor);
        course.setAssistant(assistant);
        course.setNumberOfFirstEnrollments(numberOfFirstEnrollments);
        course.setNumberOfReEnrollments(numberOfReEnrollments);
        course.setGroupPortion(groupPortion);
        course.setProfessors(professorsString);
        course.setAssistants(assistantsString);
        if (english != null) {
            course.setEnglish(true);
        } else {
            course.setEnglish(false);
        }

        return this.courseRepository.save(course);
    }


    @Override
    public Course deleteCourse(Long id) {
        Course course = this.courseRepository.findById(id).orElseThrow(CourseNotFoundException::new);
        this.courseRepository.delete(course);
        return course;
    }


    @Override
    public Page<Course> listAllSemesters(int page, int size, Short studyYear, String semesterId, Boolean english, String joinedSubjectId, String professorId, String assistantId) {
        Pageable pageable = PageRequest.of(page, size);

        return this.courseRepository.findAllFiltered(
                studyYear, semesterId, english, joinedSubjectId, professorId, assistantId, pageable);
    }

    @Override
    public Page<Course> getAllCourses(Integer page, Integer size, Short studyYear, String semesterId, Boolean english, String joinedSubjectId, String professorId, String assistantId) {
        Pageable pageable = PageRequest.of(page, size);
        Specification<Course> courseSpecification = CourseSpecification.buildSpecification(studyYear, semesterId, english, joinedSubjectId, professorId, assistantId);
        return this.courseRepository.findAll(courseSpecification, pageable);
    }




}
