package mk.ukim.finki.attendance.service.mapper.impl;

import mk.ukim.finki.attendance.model.ProfessorClassSession;
import mk.ukim.finki.attendance.model.ScheduledClassSession;
import mk.ukim.finki.attendance.model.StudentAttendance;
import mk.ukim.finki.attendance.model.dto.ProfessorAttendanceExportDto;
import mk.ukim.finki.attendance.model.enumeration.ClassType;
import mk.ukim.finki.attendance.service.mapper.ProfessorAttendanceExportDtoMapperService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;


@Service
public class ProfessorAttendanceExportDtoMapperServiceImpl implements ProfessorAttendanceExportDtoMapperService {

    public ProfessorAttendanceExportDto mapToProfessorAttendanceExportDto(ProfessorClassSession session) {
        String professorName = session.getProfessor().getName();
        ScheduledClassSession scheduledClassSession = session.getScheduledClassSession();

        String joinedSubjectAbbreviation = scheduledClassSession.getCourse().getJoinedSubject().getAbbreviation();
        Short studyYear = scheduledClassSession.getCourse().getStudyYear();
        String joinedSubjectName = scheduledClassSession.getCourse().getJoinedSubject().getName();
        Boolean courseEnglish = scheduledClassSession.getCourse().getEnglish();
        String semesterCode = scheduledClassSession.getCourse().getSemester().getCode();
        String scheduledClassSessionRoom = scheduledClassSession.getRoom().getName();
        ClassType scheduledClassSessionType = scheduledClassSession.getClassType();
        LocalDate date = session.getDate();
        LocalTime startTime = scheduledClassSession.getStartTime();
        LocalTime endTime = scheduledClassSession.getEndTime();
        Boolean professorAttended;
        LocalDateTime profArrivalTime = session.getProfessorArrivalTime();

        if (session.getAttendanceToken() == null || session.getAttendanceToken().isEmpty()) {
            professorAttended = false;
        } else if (profArrivalTime != null && startTime != null && endTime != null) {
            LocalTime arrivalTime = profArrivalTime.toLocalTime();
            if (!arrivalTime.isBefore(startTime) && !arrivalTime.isAfter(endTime)) {
                professorAttended = true;
            } else {
                professorAttended = false;
            }
        } else {
            professorAttended = false;
        }

        long numberOfStudents = 0;
        for (StudentAttendance studentAttendance : session.getStudentAttendances()){
            LocalTime arrivalTime = studentAttendance.getArrivalTime().toLocalTime();
            if (!arrivalTime.isBefore(startTime) && !arrivalTime.isAfter(endTime)) {
                numberOfStudents ++;
            }
        }

        return new ProfessorAttendanceExportDto(
                professorName,
                joinedSubjectAbbreviation,
                studyYear,
                joinedSubjectName,
                courseEnglish,
                semesterCode,
                scheduledClassSessionRoom,
                scheduledClassSessionType,
                date,
                startTime,
                endTime,
                professorAttended,
                numberOfStudents

        );
    }
}
