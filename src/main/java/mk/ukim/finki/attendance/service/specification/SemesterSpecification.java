package mk.ukim.finki.attendance.service.specification;

import mk.ukim.finki.attendance.model.Semester;
import mk.ukim.finki.attendance.model.enumeration.SemesterType;
import org.springframework.data.jpa.domain.Specification;

public class SemesterSpecification {
    public static Specification<Semester> buildSpecification(String year, SemesterType semesterType) {
        return Specification.where(withYear(year))
                .and(withSemesterType(semesterType));
    }

    private static Specification<Semester> withYear(String year) {
        if (year == null || year.isEmpty()) {
            return null;
        }
        return (root, query, cb) -> cb.like(root.get("year"), "%" + year + "%");
    }

    private static Specification<Semester> withSemesterType(SemesterType semesterType) {
        if (semesterType == null) {
            return null;
        }

        return (root, query, cb) -> cb.equal(root.get("semesterType"), semesterType);
    }

}
