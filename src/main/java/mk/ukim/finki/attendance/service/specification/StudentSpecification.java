package mk.ukim.finki.attendance.service.specification;

import jakarta.persistence.criteria.JoinType;
import mk.ukim.finki.attendance.model.Student;
import org.springframework.data.jpa.domain.Specification;

public class StudentSpecification {
    public static Specification<Student> buildSpecification(String studentIndex, String name, String lastName, String studyProgram) {
        return Specification.where(withIndex(studentIndex))
                .and(withName(name))
                .and(withSurname(lastName))
                .and(withStudyProgram(studyProgram));

    }

    private static Specification<Student> withIndex(String studentIndex){
        if (studentIndex == null || studentIndex.isEmpty()) {
            return null;
        }
        return ((root, query, cb) -> cb.like(root.get("studentIndex"), "%" + studentIndex + "%"));
    }


    private static Specification<Student> withName(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        return ((root, query, cb) -> cb.like(cb.lower(root.get("name")), "%" + name.toLowerCase() + "%"));
    }


    private static Specification<Student> withSurname(String lastName){
        if (lastName == null || lastName.isEmpty()) {
            return null;
        }
        return ((root, query, cb) -> cb.like(cb.lower(root.get("lastName")), "%" + lastName.toLowerCase() + "%"));
    }


    private static Specification<Student> withStudyProgram(String studyProgram) {
        if (studyProgram == null || studyProgram.isEmpty()) {
            return null;
        }
        return (root, query, cb) -> cb.equal(root.join("studyProgram", JoinType.INNER).get("code"), studyProgram);
    }

}
