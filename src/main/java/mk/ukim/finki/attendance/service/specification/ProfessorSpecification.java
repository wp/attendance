package mk.ukim.finki.attendance.service.specification;

import mk.ukim.finki.attendance.model.Professor;
import mk.ukim.finki.attendance.model.enumeration.ProfessorTitle;
import org.springframework.data.jpa.domain.Specification;

public class ProfessorSpecification {

    public static Specification<Professor> buildSpecification(String email, String name, ProfessorTitle professorTitle) {
        return Specification.where(withEmail(email)).and(withName(name)).and(withProfessorTitle(professorTitle));
    }

    private static Specification<Professor> withEmail(String email) {
        if (email == null) {
            return null;
        }
        return ((root, query, cb) -> cb.like(cb.lower(root.get("email")), "%" + email.toLowerCase() + "%"));
    }

    private static Specification<Professor> withName(String name) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        return ((root, query, cb) -> cb.like(cb.lower(root.get("name")), "%" + name.toLowerCase() + "%"));
    }

    private static Specification<Professor> withProfessorTitle(ProfessorTitle title) {
        if (title == null) {
            return null;
        }
        return ((root, query, cb) -> cb.equal(root.get("title"), title));
    }
}
