package mk.ukim.finki.attendance.service.mapper.impl;

import mk.ukim.finki.attendance.model.DTO.HolidayExportDto;
import mk.ukim.finki.attendance.model.Holiday;
import mk.ukim.finki.attendance.service.mapper.HolidayExportDtoMapper;
import org.springframework.stereotype.Service;

@Service
public class HolidayExportDtoMapperImpl implements HolidayExportDtoMapper {

    @Override
    public HolidayExportDto mapHolidayToExportDto(Holiday holiday) {
        HolidayExportDto dto = new HolidayExportDto();
        dto.setDate(holiday.getDate());
        dto.setName(holiday.getName());
        return dto;
    }
}
