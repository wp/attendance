package mk.ukim.finki.attendance.service.specification;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import mk.ukim.finki.attendance.model.Course;
import org.springframework.data.jpa.domain.Specification;


public class CourseSpecification {

    public static Specification<Course> buildSpecification(Short studyYear, String semesterId, Boolean english, String joinedSubjectId, String professorId, String assistantId) {
        return Specification.where(withStudyYear(studyYear))
                .and(withSemesterId(semesterId))
                .and(withEnglish(english))
                .and(withJoinedSubjectId(joinedSubjectId))
                .and(withProfessorId(professorId))
                .and(withAssistantId(assistantId));
    }

    private static Specification<Course> withStudyYear(Short studyYear) {
        if (studyYear == null) {
            return null;
        }
        return (root, query, cb) -> cb.equal(root.get("studyYear"), studyYear);
    }

    private static Specification<Course> withSemesterId(String semesterId) {
        if (semesterId == null || semesterId.isEmpty()) {
            return null;
        }
        return ((root, query, cb) -> cb.like(cb.lower(root.get("semester").get("code")), "%" + semesterId.toLowerCase() + "%"));
    }

    private static Specification<Course> withEnglish(Boolean english) {
        if (english == null) {
            return null;
        }
        return ((root, query, cb) -> cb.equal(root.get("english"), english));
    }

    private static Specification<Course> withJoinedSubjectId(String joinedSubjectId) {
        if (joinedSubjectId == null || joinedSubjectId.isEmpty()) {
            return null;
        }
        return ((root, query, cb) -> cb.like(cb.lower(root.get("joinedSubject").get("abbreviation")), "%" + joinedSubjectId.toLowerCase() + "%"));
    }


    private static Specification<Course> withProfessorId(String professorId) {
        return (Root<Course> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
            if (professorId == null || professorId.isEmpty()) {
                return null;
            }

            Predicate mainProfessorMatch = cb.like(cb.lower(root.get("professor").get("id")), "%" + professorId.toLowerCase() + "%");
            Predicate professorsMatch = cb.like(cb.lower(root.get("professors")), "%" + professorId.toLowerCase() + "%");

            return cb.or(mainProfessorMatch, professorsMatch);
        };
    }


    private static Specification<Course> withAssistantId(String assistantId) {
        return (Root<Course> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
            if (assistantId == null || assistantId.isEmpty()) {
                return null;
            }
            Predicate mainAssistantMatch = cb.like(cb.lower(root.get("assistant").get("id")), "%" + assistantId.toLowerCase() + "%");
            Predicate assistantsMatch = cb.like(cb.lower(root.get("assistants")), "%" + assistantId.toLowerCase() + "%");
            return cb.or(mainAssistantMatch, assistantsMatch);
        };
    }
}

