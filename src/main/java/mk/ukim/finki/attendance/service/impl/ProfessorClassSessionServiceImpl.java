package mk.ukim.finki.attendance.service.impl;

import mk.ukim.finki.attendance.model.*;
import mk.ukim.finki.attendance.model.dto.ProfessorAttendanceExportDto;
import mk.ukim.finki.attendance.model.dto.ProfessorAttendanceSingleExportDTO;
import mk.ukim.finki.attendance.model.exception.ProfessorClassSessionNotFoundException;
import mk.ukim.finki.attendance.model.exception.professor.ProfessorNotFoundException;
import mk.ukim.finki.attendance.model.exception.scheduledClassSession.ScheduledClassSessionNotFoundException;
import mk.ukim.finki.attendance.repository.*;
import mk.ukim.finki.attendance.service.ProfessorClassSessionService;
import mk.ukim.finki.attendance.service.mapper.ProfessorAttendanceExportDtoMapperService;
import mk.ukim.finki.attendance.service.specification.ProfessorClassSessionSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProfessorClassSessionServiceImpl implements ProfessorClassSessionService {

    private final ProfessorClassSessionRepository professorClassSessionRepository;
    private final ProfessorRepository professorRepository;
    private final ScheduledClassSessionRepository scheduledClassSessionRepository;
    private final ProfessorAttendanceExportDtoMapperService professorAttendanceExportDtoMapper;
    private final SemesterRepository semesterRepository;
    private final HolidayRepository holidayRepository;
    private final YearExamSessionRepository yearExamSessionRepository;

    public ProfessorClassSessionServiceImpl(ProfessorClassSessionRepository professorClassSessionRepository, ProfessorRepository professorRepository, ScheduledClassSessionRepository scheduledClassSessionRepository, ProfessorAttendanceExportDtoMapperService professorAttendanceExportDtoMapper, SemesterRepository semesterRepository, HolidayRepository holidayRepository, YearExamSessionRepository yearExamSessionRepository) {
        this.professorClassSessionRepository = professorClassSessionRepository;
        this.professorRepository = professorRepository;
        this.scheduledClassSessionRepository = scheduledClassSessionRepository;
        this.professorAttendanceExportDtoMapper = professorAttendanceExportDtoMapper;
        this.semesterRepository = semesterRepository;
        this.holidayRepository = holidayRepository;
        this.yearExamSessionRepository = yearExamSessionRepository;
    }


    @Override
    public Optional<ProfessorClassSession> findById(Long id) {
        Optional<ProfessorClassSession> professorClassSession = this.professorClassSessionRepository.findById(id);
        if (professorClassSession.isEmpty()) {
            throw new ProfessorClassSessionNotFoundException(id);
        }
        return professorClassSession;
    }

    @Override
    @Transactional
    public Optional<ProfessorClassSession> deleteById(Long id) {
        Optional<ProfessorClassSession> professorClassSession = this.professorClassSessionRepository.findById(id);
        if (professorClassSession.isEmpty()) {
            throw new ProfessorClassSessionNotFoundException(id);
        }
        this.professorClassSessionRepository.deleteById(id);
        return professorClassSession;
    }

    @Override
    public Page<ProfessorClassSession> getAllProfessorClassSessions(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return this.professorClassSessionRepository.findAll(pageable);
    }

    @Override
    public List<ProfessorClassSession> findAll() {
        return this.professorClassSessionRepository.findAll();
    }


    public boolean isArrivalTimeOnSameDay(LocalDateTime professorArrivalTime, LocalDate date) {
        if (professorArrivalTime == null || date == null) {
            return true;
        }
        return professorArrivalTime.toLocalDate().equals(date);
    }

    @Override
    @Transactional
    public ProfessorClassSession createProfessorClassSession(String professor, Long
            scheduledClassSession, LocalDate date, LocalDateTime professorArrivalTime, String attendanceToken) {
        Professor prof = this.professorRepository.findById(professor).orElseThrow(() -> new ProfessorNotFoundException(professor));
        ScheduledClassSession scs = this.scheduledClassSessionRepository.findById(scheduledClassSession).orElseThrow(() -> new ScheduledClassSessionNotFoundException(scheduledClassSession));
        ProfessorClassSession professorClassSession = new ProfessorClassSession(prof, scs, date, professorArrivalTime, attendanceToken);
        if (!isArrivalTimeOnSameDay(professorArrivalTime, date)) {
            throw new IllegalArgumentException("Времето на доаѓање на професорот мора да биде на истиот ден со закажаната сесија.");
        }
        return this.professorClassSessionRepository.save(professorClassSession);
    }

    @Override
    @Transactional
    public ProfessorClassSession updateProfessorClassSession(Long id, String professor, Long
            scheduledClassSession, LocalDate date, LocalDateTime professorArrivalTime, String attendanceToken) {
        ProfessorClassSession professorClassSession = this.professorClassSessionRepository.findById(id)
                .orElseThrow(() -> new ProfessorClassSessionNotFoundException(id));

        Professor prof = this.professorRepository.findById(professor).orElseThrow(() -> new ProfessorNotFoundException(professor));
        ScheduledClassSession scs = this.scheduledClassSessionRepository.findById(scheduledClassSession).orElseThrow(() -> new ScheduledClassSessionNotFoundException(scheduledClassSession));

        professorClassSession.setProfessor(prof);

        professorClassSession.setScheduledClassSession(scs);

        professorClassSession.setDate(date);

        professorClassSession.setAttendanceToken(attendanceToken);

        professorClassSession.setProfessorArrivalTime(professorArrivalTime);

        if (!isArrivalTimeOnSameDay(professorArrivalTime, date)) {
            throw new IllegalArgumentException("Времето на доаѓање на професорот мора да биде на истиот ден со закажаната сесија.");
        }

        return this.professorClassSessionRepository.save(professorClassSession);
    }

    @Override
    public Page<ProfessorClassSession> filterByOptions(String professor, LocalDate from, LocalDate to, Pageable
            pageable) {
        Specification<ProfessorClassSession> specification = ProfessorClassSessionSpecification.buildSpecification(null, professor, from, to);

        return professorClassSessionRepository.findAll(specification, pageable);
    }


    @Override
    public List<ProfessorClassSession> importData(List<ProfessorClassSession> professorClassSessions) {
        return professorClassSessions.stream()
                .map(it -> {
                    try {
                        it.setProfessor(professorRepository.findById(it.getProfessor_id()).orElseThrow(() -> new ProfessorNotFoundException(it.getProfessor_id())));
                        it.setScheduledClassSession(scheduledClassSessionRepository.findById(Long.valueOf(it.getScheduled_class_session_id_name())).orElseThrow());
                        if (isValidPCS(it)) {
                            it.setProfessor(professorRepository.findById(it.getProfessor_id()).orElseThrow(() -> new ProfessorNotFoundException(it.getProfessor_id())));
                            professorClassSessionRepository.save(it);
                            return null;
                        } else {
                            return it;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        return it;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProfessorAttendanceExportDto> getProfessorAttendance(String professorId, Long courseId) {
        Specification<ProfessorClassSession> spec =
                ProfessorClassSessionSpecification.buildSpecification(courseId, professorId, null, null);

        List<ProfessorClassSession> professorClassSessions = professorClassSessionRepository.findAll(spec);
        return professorClassSessions.stream()
                .map(professorAttendanceExportDtoMapper::mapToProfessorAttendanceExportDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProfessorAttendanceSingleExportDTO> getProfessorAttendance(Long id) {
        return professorClassSessionRepository.findForSingleProfessorClassSession(id);
    }


    public void generateProfessorClassSessions() {
        Semester activeSemester = semesterRepository.findActiveSemester();

        List<Holiday> holidays = holidayRepository.findAll();
        LocalDate startDate = activeSemester.getStartDate();
        LocalDate endDate = activeSemester.getEndDate();

        List<ScheduledClassSession> scheduledSessions = scheduledClassSessionRepository.findBySemester(activeSemester);


        for (ScheduledClassSession scheduledSession : scheduledSessions) {
            LocalDate sessionDate = startDate;

            while (sessionDate.getDayOfWeek() != scheduledSession.getDayOfWeek()) {
                sessionDate = sessionDate.plusDays(1);
            }

            while (!sessionDate.isAfter(endDate)) {
                if (isWorkDay(sessionDate, holidays) && !yearExamSessionRepository.existsByDateBetweenStartDateAndEndDate(sessionDate)) {
                    String professorId = scheduledSession.getCourse().getProfessor().getId();
                    Professor courseProfessor = professorRepository.findById(professorId).orElse(null);

                    if (courseProfessor != null && !professorClassSessionRepository.existsByDateAndProfessorAndScheduledClassSession(sessionDate, courseProfessor, scheduledSession)) {
                        ProfessorClassSession professorClassSession = new ProfessorClassSession();
                        professorClassSession.setDate(sessionDate);
                        professorClassSession.setProfessor(courseProfessor);
                        professorClassSession.setScheduledClassSession(scheduledSession);
                        professorClassSessionRepository.save(professorClassSession);
                    }
                }

                sessionDate = sessionDate.plusWeeks(1);
            }
        }
    }

    private boolean isWorkDay(LocalDate date, List<Holiday> holidays) {
        return date.getDayOfWeek() != DayOfWeek.SATURDAY && date.getDayOfWeek() != DayOfWeek.SUNDAY &&
                holidays.stream().noneMatch(holiday -> holiday.getDate().isEqual(date));
    }

    private boolean isValidPCS(ProfessorClassSession it) {
        return it.getProfessor_id() != null && !it.getProfessor_id().isEmpty() &&
                it.getScheduledClassSession() != null &&
                it.getDate() != null &&
                it.getProfessorArrivalTime() != null &&
                it.getAttendanceToken() != null && !it.getAttendanceToken().isEmpty();
    }
}

