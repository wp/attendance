package mk.ukim.finki.attendance.service.impl;

import jakarta.transaction.Transactional;
import mk.ukim.finki.attendance.model.Course;
import mk.ukim.finki.attendance.model.Room;
import mk.ukim.finki.attendance.model.ScheduledClassSession;
import mk.ukim.finki.attendance.model.dto.ScheduledClassSessionExportDTO;
import mk.ukim.finki.attendance.model.enumeration.ClassType;
import mk.ukim.finki.attendance.model.exception.scheduledClassSession.ConflictingScheduledClassSessionException;
import mk.ukim.finki.attendance.model.exception.scheduledClassSession.InvalidScheduledClassSessionUpdate;
import mk.ukim.finki.attendance.model.exception.scheduledClassSession.ScheduledClassSessionNotFoundException;
import mk.ukim.finki.attendance.repository.ScheduledClassSessionRepository;
import mk.ukim.finki.attendance.service.CourseService;
import mk.ukim.finki.attendance.service.RoomService;
import mk.ukim.finki.attendance.service.ScheduledClassSessionService;
import mk.ukim.finki.attendance.service.mapper.impl.ScheduledClassSessionExportDtoMapperImpl;
import mk.ukim.finki.attendance.service.specification.ScheduledClassSessionSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ScheduledClassSessionServiceImpl implements ScheduledClassSessionService {

    private final ScheduledClassSessionRepository scheduledClassSessionRepository;
    private final CourseService courseService;
    private final RoomService roomService;
    private final ScheduledClassSessionExportDtoMapperImpl scheduledClassSessionExportDtoMapperImpl;


    public ScheduledClassSessionServiceImpl(ScheduledClassSessionRepository scheduledClassSessionRepository, CourseService courseService, RoomService roomService, ScheduledClassSessionExportDtoMapperImpl scheduledClassSessionExportDtoMapperImpl) {
        this.scheduledClassSessionRepository = scheduledClassSessionRepository;
        this.courseService = courseService;
        this.roomService = roomService;
        this.scheduledClassSessionExportDtoMapperImpl = scheduledClassSessionExportDtoMapperImpl;
    }

    @Override
    public Page<ScheduledClassSession> findAllFilteredAndPaged(String dayOfWeekName, String roomName, String classTypeName, Long courseId, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);

        String roomNameWithWildcards = (roomName == null) ? null : "%" + roomName + "%";

        return scheduledClassSessionRepository.findFiltered(dayOfWeekName == null ? null : DayOfWeek.valueOf(dayOfWeekName), roomNameWithWildcards, classTypeName == null ? null : ClassType.valueOf(classTypeName), courseId, pageable);
    }


    @Override
    public Optional<ScheduledClassSession> findById(Long id) {
        Optional<ScheduledClassSession> scheduledClassSession = scheduledClassSessionRepository.findById(id);
        if (scheduledClassSession.isEmpty()) {
            throw new ScheduledClassSessionNotFoundException(id);
        }
        return scheduledClassSession;
    }

    @Override
    public List<ScheduledClassSession> listAll() {
        return scheduledClassSessionRepository.findAll();
    }

    @Override
    public Optional<ScheduledClassSession> deleteOptionalById(Long id) {
        Optional<ScheduledClassSession> scheduledClassSession = scheduledClassSessionRepository.findById(id);
        if (scheduledClassSession.isEmpty()) {
            throw new ScheduledClassSessionNotFoundException(id);
        }
        scheduledClassSessionRepository.deleteById(id);
        return scheduledClassSession;
    }

    @Override
    public void deleteScheduledClassSessionById(Long id) {
        scheduledClassSessionRepository.deleteById(id);
    }

    @Override
    @Transactional
    public ScheduledClassSession createScheduledClassSession(LocalTime startTime, DayOfWeek dayOfWeek, LocalTime endTime, Room room, ClassType classType, Course course) {
        ScheduledClassSession scheduledClassSession = new ScheduledClassSession();
        scheduledClassSession.setStartTime(startTime);
        scheduledClassSession.setDayOfWeek(dayOfWeek);
        scheduledClassSession.setEndTime(endTime);
        scheduledClassSession.setRoom(room);
        scheduledClassSession.setClassType(classType);
        scheduledClassSession.setCourse(course);

        Specification<ScheduledClassSession> conflictSpec = ScheduledClassSessionSpecification.isConflict(startTime, endTime, dayOfWeek, room, null);

        if (scheduledClassSessionRepository.findAll(conflictSpec).isEmpty()) {
            return scheduledClassSessionRepository.save(scheduledClassSession);
        } else {
            throw new ConflictingScheduledClassSessionException(dayOfWeek, startTime, endTime, room);
        }
    }

    @Override
    @Transactional
    public ScheduledClassSession updateScheduledClassSession(Long id, LocalTime startTime, DayOfWeek dayOfWeek, LocalTime endTime, Room room, ClassType classType, Course course) {
        ScheduledClassSession scheduledClassSession = scheduledClassSessionRepository.findById(id).orElseThrow(() -> new ScheduledClassSessionNotFoundException(id));
        scheduledClassSession.setStartTime(startTime);
        scheduledClassSession.setDayOfWeek(dayOfWeek);
        scheduledClassSession.setEndTime(endTime);
        scheduledClassSession.setRoom(room);
        scheduledClassSession.setClassType(classType);
        scheduledClassSession.setCourse(course);

        Specification<ScheduledClassSession> conflictSpec = ScheduledClassSessionSpecification.isConflict(startTime, endTime, dayOfWeek, room, id);
        if (scheduledClassSessionRepository.findAll(conflictSpec).isEmpty()) {
            try {
                return scheduledClassSessionRepository.save(scheduledClassSession);
            } catch (Exception exception) {
                throw new InvalidScheduledClassSessionUpdate();
            }
        } else {
            throw new ConflictingScheduledClassSessionException(dayOfWeek, startTime, endTime, room);
        }


    }

    @Override
    public Page<ScheduledClassSession> getAllScheduledClassSessions(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return this.scheduledClassSessionRepository.findAll(pageable);
    }

    @Override
    public Page<ScheduledClassSession> findAllScheduledClassSessionSpec(DayOfWeek dayOfWeekName, String roomName, ClassType classType, Long courseId, int page, int size) {
        Specification<ScheduledClassSession> specification = ScheduledClassSessionSpecification.buildSpecification(dayOfWeekName, roomName, classType, courseId);
        Pageable pageable = PageRequest.of(page, size);
        return this.scheduledClassSessionRepository.findAll(specification, pageable);
    }

    @Override
    public List<ScheduledClassSession> importFromList(List<ScheduledClassSession> scheduledClassSessions) {
        return scheduledClassSessions.stream().map(scs -> {
            try {
                if (!isValidScheduledClassSession(scs)) {
                    return scs;
                }
                scs.setRoom(this.roomService.findById(scs.getRoomId()));
                scs.setCourse(this.courseService.findCourseById(scs.getCourseId()));
                scs.setClassType(ClassType.valueOf(scs.getClassTypeName()));
                scs.setDayOfWeek(DayOfWeek.valueOf(scs.getDayOfTheWeekName().toUpperCase()));

                Specification<ScheduledClassSession> conflictSpec = ScheduledClassSessionSpecification.isConflict(scs.getStartTime(), scs.getEndTime(), scs.getDayOfWeek(), scs.getRoom(), scs.getId());

                if (scheduledClassSessionRepository.findAll(conflictSpec).isEmpty()) {
                    scheduledClassSessionRepository.save(scs);
                    return null;
                }
                return scs;
            } catch (Exception e) {
                return scs;
            }
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }


    private boolean isValidScheduledClassSession(ScheduledClassSession scs) {
        if (scs.getStartTime() == null || scs.getEndTime() == null || scs.getDayOfTheWeekName() == null || scs.getDayOfTheWeekName().isEmpty() || !isDayOfWeekValid(scs.getDayOfTheWeekName()) || scs.getRoomId() == null || scs.getRoomId().isEmpty() || scs.getClassTypeName() == null || scs.getClassTypeName().isEmpty() || scs.getCourseId() == null || !isCourseIdValid(scs.getCourseId())) {
            return false;
        }

        return !scs.getStartTime().isAfter(scs.getEndTime()) && !scs.getStartTime().equals(scs.getEndTime());
    }

    private boolean isDayOfWeekValid(String dayOfTheWeekName) {
        try {
            DayOfWeek.valueOf(dayOfTheWeekName.toUpperCase());
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    private boolean isCourseIdValid(Long courseId) {
        try {
            courseService.findCourseById(courseId);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<ScheduledClassSessionExportDTO> exportScheduledClassSessions(DayOfWeek dayOfWeek, String roomName, Long courseId, ClassType classTypeName) {
        Specification<ScheduledClassSession> specification = ScheduledClassSessionSpecification.buildSpecification(dayOfWeek, roomName, classTypeName, courseId);
        List<ScheduledClassSession> holidays = scheduledClassSessionRepository.findAll(specification);
        return holidays.stream().map(scheduledClassSessionExportDtoMapperImpl::mapScheduledClassSessionToExportDto).collect(Collectors.toList());
    }
}