package mk.ukim.finki.attendance.service.impl;

import jakarta.transaction.Transactional;
import mk.ukim.finki.attendance.model.Semester;
import mk.ukim.finki.attendance.model.dto.StudentAttendanceExportDTO;
import mk.ukim.finki.attendance.model.ProfessorClassSession;
import mk.ukim.finki.attendance.model.Student;
import mk.ukim.finki.attendance.model.StudentAttendance;
import mk.ukim.finki.attendance.model.exception.ProfessorClassSessionNotFoundException;
import mk.ukim.finki.attendance.model.exception.studentattendance.StudentAttendanceNotFoundException;
import mk.ukim.finki.attendance.repository.StudentAttendanceRepository;
import mk.ukim.finki.attendance.repository.StudentRepository;
import mk.ukim.finki.attendance.service.ProfessorClassSessionService;
import mk.ukim.finki.attendance.service.StudentAttendanceService;
import mk.ukim.finki.attendance.service.StudentService;
import mk.ukim.finki.attendance.service.mapper.impl.StudentAttendanceExportDtoMapperImpl;
import mk.ukim.finki.attendance.service.specification.StudentAttendanceSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class StudentAttendanceServiceImpl implements StudentAttendanceService {

    private final StudentAttendanceRepository repository;
    private final StudentService studentService;
    private final ProfessorClassSessionService professorClassSessionService;
    private final StudentRepository studentRepository;
    private final StudentAttendanceExportDtoMapperImpl studentAttendanceExportDtoMapperImpl;

    public StudentAttendanceServiceImpl(
            StudentAttendanceRepository repository,
            StudentService studentService,
            ProfessorClassSessionService professorClassSessionService,
            StudentRepository studentRepository,
            StudentAttendanceExportDtoMapperImpl studentAttendanceExportDtoMapperImpl) {
        this.repository = repository;
        this.studentService = studentService;
        this.professorClassSessionService = professorClassSessionService;
        this.studentRepository = studentRepository;
        this.studentAttendanceExportDtoMapperImpl = studentAttendanceExportDtoMapperImpl;
    }

    @Override
    public void create(String studentIndex, Long professorClassSessionId, LocalDateTime arrivalTime) {
        if (arrivalTime == null) {
            arrivalTime = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
            String formattedArrivalTime = arrivalTime.format(formatter);
            arrivalTime = LocalDateTime.parse(formattedArrivalTime, formatter);
        }
        Student student = studentService.findByIndex(studentIndex);
        ProfessorClassSession professorClassSession = professorClassSessionService.findById(professorClassSessionId)
                .orElseThrow(() -> new ProfessorClassSessionNotFoundException(professorClassSessionId));

        StudentAttendance toCreate = new StudentAttendance(student, professorClassSession, arrivalTime);
        repository.save(toCreate);
    }

    @Override
    public void update(Long id, String studentIndex, Long professorClassSessionId, LocalDateTime arrivalTime) {
        Student student = studentService.findByIndex(studentIndex);
        ProfessorClassSession professorClassSession = professorClassSessionService.findById(professorClassSessionId)
                .orElseThrow(() -> new ProfessorClassSessionNotFoundException(professorClassSessionId));

        StudentAttendance toUpdate = findById(id);
        toUpdate.setStudent(student);
        toUpdate.setProfessorClassSession(professorClassSession);
        toUpdate.setArrivalTime(arrivalTime);

        repository.save(toUpdate);
    }

    @Override
    public void delete(Long id) {
        StudentAttendance toDelete = findById(id);
        repository.delete(toDelete);
    }

    @Override
    public StudentAttendance findById(Long id) {
        return repository.findById(id).orElseThrow(() ->
                new StudentAttendanceNotFoundException(id));
    }

    @Override
    public List<StudentAttendance> findAll() {
        return repository.findAll();
    }

    @Override
    public Page<StudentAttendance> getAllAttendingStudents(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return this.repository.findAll(pageable);
    }

    @Override
    public Page<StudentAttendance> getAllStudentsSpecification(String student, int page, int size) {
        Specification<StudentAttendance> specification = StudentAttendanceSpecification.buildSpecification(
                student, null, null, false, null, null, null
        );

        Pageable pageable = PageRequest.of(page, size);
        return this.repository.findAll(specification, pageable);
    }

    @Override
    public List<StudentAttendance> importStudentAttendance(List<StudentAttendance> attendances) {
        return attendances.stream()
                .map(it -> {
                    try {
                        if (!studentAttendanceIsValid(it)) {
                            return it;
                        }
                        it.setProfessorClassSession(professorClassSessionService.findById(it.getClassSession())
                                .orElseThrow(ProfessorClassSessionNotFoundException::new));
                        it.setStudent(studentService.findByIndex(it.getStudentIndex()));
                        it.setArrivalTime(it.getArrivalTime());
                        repository.save(it);
                        return null;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return it;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private boolean studentAttendanceIsValid(StudentAttendance studentAttendance) {
        return studentAttendance.getArrivalTime() != null &&
                studentAttendance.getClassSession() != null &&
                studentAttendance.getStudentIndex() != null;
    }

    @Override
    public List<StudentAttendanceExportDTO> exportStudentAttendance(
            String studentIndex,
            Long courseId,
            String semesterCode,
            LocalDate dateFrom,
            LocalDate dateTo,
            Semester activeSemester
    ) {
        Specification<StudentAttendance> specification = StudentAttendanceSpecification.buildSpecification(
                studentIndex,
                courseId,
                semesterCode,
                true,
                dateFrom,
                dateTo,
                activeSemester
        );

        List<StudentAttendance> attendances = repository.findAll(specification);
        return attendances.stream()
                .map(studentAttendanceExportDtoMapperImpl::mapStudentAttendanceExportDTOList)
                .collect(Collectors.toList());
    }


    @Override
    @Transactional
    public List<mk.ukim.finki.attendance.model.dto.StudentAttendanceDTO> studentAttendancesExport(String studentIndex, String semesterCode) {
        return this.repository.findAttendanceForStudentInSemester(studentIndex, semesterCode);
    }

    @Override
    public List<StudentAttendance> findAllByProfessorClassSession(ProfessorClassSession session) {
        return repository.findAllByProfessorClassSession(session);
    }
}
