package mk.ukim.finki.attendance.service.specification;

import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import mk.ukim.finki.attendance.model.ProfessorClassSession;
import mk.ukim.finki.attendance.model.ScheduledClassSession;
import mk.ukim.finki.attendance.model.Semester;
import mk.ukim.finki.attendance.model.StudentAttendance;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.time.LocalDateTime;


public class StudentAttendanceSpecification {
    private static Specification<StudentAttendance> withStudent(String student) {
        if(student==null || student.isEmpty()){
            return null;
        }
        return ((root, query, cb) -> cb.like(cb.lower(root.get("student").get("studentIndex")),"%" +  student.toLowerCase() + "%"));
    }

    private static Specification<StudentAttendance> withStudentIndex(String studentIndex) {
        return (root, query, cb) -> {
            if (studentIndex == null || studentIndex.isEmpty()) {
                return cb.conjunction();
            }
            return cb.equal(root.join("student").get("studentIndex"), studentIndex);
        };
    }

    private static Specification<StudentAttendance> withCourseId(Long courseId) {
        return (root, query, cb) -> {
            if (courseId == null) {
                return cb.conjunction();
            }
            return cb.equal(root.join("professorClassSession").join("scheduledClassSession").join("course").get("id"), courseId);
        };
    }

    private static Specification<StudentAttendance> withSemesterCode(String semesterCode) {
        return (root, query, cb) -> {
            if (semesterCode == null || semesterCode.isEmpty()) {
                return cb.conjunction();
            }
            return cb.equal(root.join("professorClassSession").join("scheduledClassSession").join("course").join("semester").get("code"), semesterCode);
        };
    }

    private static Specification<StudentAttendance> withArrivalTimeBetween() {
        return (root, query, cb) -> {
            Join<StudentAttendance, ProfessorClassSession> pcsJoin = root.join("professorClassSession", JoinType.INNER);
            Join<ProfessorClassSession, ScheduledClassSession> scsJoin = pcsJoin.join("scheduledClassSession", JoinType.INNER);

            Expression<String> arrivalTimeAsTime = cb.function(
                    "to_char",
                    String.class,
                    root.get("arrivalTime"),
                    cb.literal("HH24:MI:SS")
            );

            Expression<String> startTime = cb.function(
                    "to_char",
                    String.class,
                    scsJoin.get("startTime"),
                    cb.literal("HH24:MI:SS")
            );
            Expression<String> endTime = cb.function(
                    "to_char",
                    String.class,
                    scsJoin.get("endTime"),
                    cb.literal("HH24:MI:SS")
            );


            Predicate betweenStartEndTime = cb.between(arrivalTimeAsTime, startTime, endTime);

            Predicate condition = cb.and(betweenStartEndTime);

            return condition;
        };

    }

    private static Specification<StudentAttendance> withDateOrActiveSemester(LocalDate dateFrom, LocalDate dateTo, Semester activeSemester) {
        return (root, query, cb) -> {
            if (dateFrom == null && dateTo == null) {
                return cb.equal(root.join("professorClassSession").join("scheduledClassSession").join("course").join("semester").get("code"), activeSemester);
            } else {
                LocalDateTime startOfDay = (dateFrom != null) ? dateFrom.atStartOfDay() : LocalDateTime.MIN;
                LocalDateTime endOfDay = (dateTo != null) ? dateTo.atTime(23, 59, 59) : LocalDateTime.MAX;

                return cb.between(root.get("arrivalTime"), startOfDay, endOfDay);
            }
        };
    }


    public static Specification<StudentAttendance> buildSpecification(
            String studentIndex,
            Long courseId,
            String semesterCode,
            boolean forExport,
            LocalDate dateFrom,
            LocalDate dateTo,
            Semester activeSemester

    ) {

        Specification<StudentAttendance> spec = Specification.where(withStudentIndex(studentIndex));

        if (courseId != null) {
            spec = spec.and(withCourseId(courseId));
        }

        if (semesterCode != null && !semesterCode.isEmpty()) {
            spec = spec.and(withSemesterCode(semesterCode));
        }

        if(dateFrom != null || dateTo!= null ){
            spec = spec.and(withDateOrActiveSemester(dateFrom,dateTo,activeSemester));
        }

        if (forExport) {
            spec = spec.and(withArrivalTimeBetween());
        }

        return spec;
    }


}
