package mk.ukim.finki.attendance.service.specification;

import mk.ukim.finki.attendance.model.StudyProgram;
import org.springframework.data.jpa.domain.Specification;

public class StudyProgramSpecification {
    public static Specification<StudyProgram> buildSpecification(String code, String name){
        return Specification.where(withCode(code))
                .and(withName(name));
    }

    private static Specification<StudyProgram> withCode(String code){
        if(code == null || code.isEmpty()){
            return null;
        }
        return ((root, query, cb) -> cb.like(cb.lower(root.get("code")), "%" + code.toLowerCase() + "%"));
    }

    private static Specification<StudyProgram> withName(String name){
        if(name == null || name.isEmpty()){
            return null;
        }
        return ((root, query, cb) -> cb.like(cb.lower(root.get("name")), "%" + name.toLowerCase() + "%"));
    }
}
