package mk.ukim.finki.attendance.service;
import java.util.List;


public interface ProfessorAttendanceService {

    List<Object[]> findAllProfessorAttendance(String professorId);
}
