package mk.ukim.finki.attendance.service.specification;

import jakarta.persistence.criteria.*;
import mk.ukim.finki.attendance.model.*;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;

public class ProfessorClassSessionSpecification {

    public static Specification<ProfessorClassSession> buildSpecification(Long courseId, String professor, LocalDate from, LocalDate to) {
        return Specification.where(withCourse(courseId))
                .and(withProfessor(professor))
                .and(withDate(from, to));
    }

    private static Specification<ProfessorClassSession> withCourse(Long courseId) {
        return (root, query, cb) -> {
            if (courseId == null) {
                return null;
            }

            Join<ProfessorClassSession, ScheduledClassSession> scsJoin = root.join("scheduledClassSession", JoinType.INNER);
            Join<ScheduledClassSession, Course> courseJoin = scsJoin.join("course", JoinType.INNER);

            return cb.equal(courseJoin.get("id"), courseId);
        };
    }


    private static Specification<ProfessorClassSession> withProfessor(String professor) {
        return (root, query, cb) -> {
            if (professor == null || professor.isEmpty()) {
                return null;
            }

            Join<ProfessorClassSession, Professor> professorJoin = root.join("professor", JoinType.INNER);

            Predicate profIdInMiddle = cb.like(professorJoin.get("id"), "%;" + professor + ";%");
            Predicate profIdAtStart = cb.like(professorJoin.get("id"), professor + ";%");
            Predicate profIdAtEnd = cb.like(professorJoin.get("id"), "%;" + professor);
            Predicate profIdExact = cb.equal(professorJoin.get("id"), professor);
            Predicate containsProfId = cb.or(profIdInMiddle, profIdAtStart, profIdAtEnd, profIdExact);

            return containsProfId;
        };
    }

    private static Specification<ProfessorClassSession> withDate(LocalDate from, LocalDate to) {
        if (from == null && to == null) {
            return (root, query, cb) -> cb.conjunction();
        } else if (from == null) {
            return (root, query, cb) -> cb.lessThanOrEqualTo(root.get("date"), to);
        } else if (to == null) {
            return (root, query, cb) -> cb.greaterThanOrEqualTo(root.get("date"), from);
        } else {
            return (root, query, cb) -> cb.between(root.get("date"), from, to);
        }
    }
}
