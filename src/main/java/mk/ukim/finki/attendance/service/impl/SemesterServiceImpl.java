package mk.ukim.finki.attendance.service.impl;

import mk.ukim.finki.attendance.model.Semester;
import mk.ukim.finki.attendance.model.enumeration.SemesterState;
import mk.ukim.finki.attendance.model.enumeration.SemesterType;
import mk.ukim.finki.attendance.model.exception.semester.SemesterAlreadyExistsWithCodeException;
import mk.ukim.finki.attendance.model.exception.semester.SemesterNotFoundException;
import mk.ukim.finki.attendance.repository.SemesterRepository;
import mk.ukim.finki.attendance.service.SemesterService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Service
public class SemesterServiceImpl implements SemesterService {

    private final SemesterRepository semesterRepository;

    public SemesterServiceImpl(SemesterRepository semesterRepository) {
        this.semesterRepository = semesterRepository;
    }

    @Override
    public Page<Semester> getAllSem(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return semesterRepository.findAll(pageable);
    }

    @Override
    public List<Semester> listAllSemesters() {
        return this.semesterRepository.findAll();
    }

    @Override
    public Semester getSemesterById(String code) {
        return semesterRepository.findById(code).orElseThrow(SemesterNotFoundException::new);
    }

    @Override
    public void deleteSemester(String code) {
        Semester semester = getSemesterById(code);
        semesterRepository.delete(semester);
    }

    @Override
    public Semester updateSemester(String code, String year, SemesterType semesterType, LocalDate startDate, LocalDate end, LocalDate enrollmentStartDate, LocalDate enrollmentEndDate, SemesterState semesterState) {
            validateSemesterDates(startDate, end, enrollmentStartDate, enrollmentEndDate);

        if (semesterState == SemesterState.STARTED) {
            Semester activeSemester = semesterRepository.findActiveSemester();
            if (activeSemester != null) {
                throw new IllegalStateException("Another semester is already in the STARTED state.");
            }
        }

        Semester semester = getSemesterById(code);

        semester.setYear(year);
        semester.setSemesterType(semesterType);
        semester.setStartDate(startDate);
        semester.setEndDate(end);
        semester.setEnrollmentStartDate(enrollmentStartDate);
        semester.setEnrollmentEndDate(enrollmentEndDate);
        semester.setState(semesterState);

        return semesterRepository.save(semester);

    }

    @Override
    public Semester addSemester(String code, String year, SemesterType semesterType, LocalDate startDate, LocalDate end, LocalDate enrollmentStartDate, LocalDate enrollmentEndDate, SemesterState semesterState) {
        validateSemesterDates(startDate, end, enrollmentStartDate, enrollmentEndDate);

        if (semesterRepository.existsById(code)) {
            throw new SemesterAlreadyExistsWithCodeException("Semester with code " + code + " already exists.");
        }

        if (semesterState == SemesterState.STARTED) {
            Semester activeSemester = semesterRepository.findActiveSemester();
            if (activeSemester != null) {
                throw new IllegalStateException("Another semester is already in the STARTED state.");
            }
        }

        Semester semester = new Semester(code, year, semesterType, startDate, end, enrollmentStartDate, enrollmentEndDate, semesterState);
        return semesterRepository.save(semester);
    }

    @Override
    public Page<Semester> getAllSemesters(Specification<Semester> specification, Pageable pageable) {
        return semesterRepository.findAll(specification, pageable);
    }

    @Override
    public List<Semester> importData(List<Semester> semesters) {
        return semesters.stream()
                .map(it -> {
                    try {
                        it.setCode(it.getCode());
                        it.setYear(it.getYear());
                        it.setSemesterType(SemesterType.valueOf(it.getSemesterTypeName()));
                        it.setStartDate(it.getStartDate());
                        it.setEndDate(it.getEndDate());
                        it.setEnrollmentStartDate(it.getEnrollmentStartDate());
                        it.setEnrollmentEndDate(it.getEnrollmentEndDate());
                        it.setState(SemesterState.valueOf(it.getStateName()));
                        semesterRepository.save(it);

                        return null;

                    } catch (Exception e) {
                        e.printStackTrace();
                        return it;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private void validateSemesterDates(LocalDate startDate, LocalDate endDate, LocalDate enrollmentStartDate, LocalDate enrollmentEndDate) {
        if (enrollmentStartDate != null && enrollmentStartDate.isAfter(startDate)) {
            throw new IllegalArgumentException("Enrollment start date cannot be after the semester start date.");
        }

        if (endDate != null && endDate.isBefore(startDate)) {
            throw new IllegalArgumentException("End date cannot be after the semester start date.");
        }

    }

}
