package mk.ukim.finki.attendance.web;

import jakarta.servlet.http.HttpServletResponse;
import mk.ukim.finki.attendance.model.Course;
import mk.ukim.finki.attendance.model.Room;
import mk.ukim.finki.attendance.model.ScheduledClassSession;
import mk.ukim.finki.attendance.model.enumeration.ClassType;
import mk.ukim.finki.attendance.model.exception.scheduledClassSession.InvalidScheduledTimeException;
import mk.ukim.finki.attendance.repository.ImportRepository;
import mk.ukim.finki.attendance.service.CourseService;
import mk.ukim.finki.attendance.service.RoomService;
import mk.ukim.finki.attendance.service.ScheduledClassSessionService;
import mk.ukim.finki.attendance.service.utils.PaginationService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.List;

@Controller
@RequestMapping("/scheduledClassSession")
public class ScheduledClassSessionController {

    private final ScheduledClassSessionService scheduledClassSessionService;
    private final RoomService roomService;
    private final CourseService courseService;
    private final ImportRepository importRepository;
    private final PaginationService paginationService;

    public ScheduledClassSessionController(ScheduledClassSessionService scheduledClassSessionService, RoomService roomService, CourseService courseService, ImportRepository importRepository, PaginationService paginationService) {
        this.scheduledClassSessionService = scheduledClassSessionService;
        this.roomService = roomService;
        this.courseService = courseService;
        this.importRepository = importRepository;
        this.paginationService = paginationService;
    }

    @GetMapping
    public String showAll(Model model,
                          @RequestParam(defaultValue = "1") int page,
                          @RequestParam(defaultValue = "5") int size,
                          @RequestParam(required = false) DayOfWeek dayOfWeekName,
                          @RequestParam(required = false) String roomName,
                          @RequestParam(required = false) ClassType classTypeName,
                          @RequestParam(required = false) Long courseId
    ) {
        Page<ScheduledClassSession> scheduledClassSessionPage = this.scheduledClassSessionService
                .findAllScheduledClassSessionSpec(dayOfWeekName, roomName, classTypeName, courseId, page - 1, size);

        PaginationService.PaginationData paginationData = paginationService.calculatePagination(page, size, scheduledClassSessionPage.getTotalPages());

        model.addAttribute("selectedDayOfWeek", dayOfWeekName);
        model.addAttribute("selectedRoomName", roomName);
        model.addAttribute("selectedClassTypeName", classTypeName);
        model.addAttribute("selectedCourseId", courseId);
        model.addAttribute("allCourses", courseService.listAllCourses());
        model.addAttribute("classTypes", ClassType.values());
        model.addAttribute("rooms", roomService.findAll());
        model.addAttribute("daysOfWeek", DayOfWeek.values());
        model.addAttribute("scheduledClassSessions", scheduledClassSessionPage.getContent());
        model.addAttribute("currentPage", paginationData.getCurrentPage());
        model.addAttribute("totalPages", paginationData.getTotalPages());
        model.addAttribute("startPage", paginationData.getStartPage());
        model.addAttribute("endPage", paginationData.getEndPage());
        model.addAttribute("bodyContent", "scheduled-class-session/scheduledClassSessionsList");
        return "master-template";
    }

    @GetMapping("/detail/{id}")
    public String getScheduledClassSessionDetail(@PathVariable Long id, Model model) {
        model.addAttribute("scs", scheduledClassSessionService.findById(id));
        model.addAttribute("bodyContent", "scheduled-class-session/scheduledClassSessionsDetails");
        return "master-template";
    }

    @GetMapping("/addForm")
    public String showAdd(Model model) {
        model.addAttribute("rooms", roomService.findAll());
        model.addAttribute("courses", courseService.listAllCourses());
        model.addAttribute("classTypes", ClassType.values());
        model.addAttribute("daysOfWeek", DayOfWeek.values());
        model.addAttribute("bodyContent", "scheduled-class-session/scheduledClassSessionsForm");
        return "master-template";
    }

    @GetMapping("/editForm/{id}")
    public String showEdit(@PathVariable Long id, Model model) {
        ScheduledClassSession scheduledClassSession = this.scheduledClassSessionService.findById(id).get();
        model.addAttribute("scheduledClassSession", scheduledClassSession);
        model.addAttribute("rooms", roomService.findAll());
        model.addAttribute("courses", courseService.listAllCourses());
        model.addAttribute("classTypes", ClassType.values());
        model.addAttribute("daysOfWeek", DayOfWeek.values());
        model.addAttribute("bodyContent", "scheduled-class-session/scheduledClassSessionsForm");
        return "master-template";
    }

    @PostMapping("/delete/{id}")
    public String deleteScheduledClassSession(@PathVariable Long id) {
        this.scheduledClassSessionService.deleteScheduledClassSessionById(id);
        return "redirect:/scheduledClassSession";
    }

    @PostMapping("/add")
    public String createScheduledClassSession(
            @RequestParam LocalTime startTime,
            @RequestParam LocalTime endTime,
            @RequestParam DayOfWeek dayOfWeek,
            @RequestParam String roomId,
            @RequestParam ClassType classType,
            @RequestParam Long courseId) {

        if (!startTime.isBefore(endTime)) {
            throw new InvalidScheduledTimeException("Start time must be before end time.");
        }
        Room room = roomService.findById(roomId);
        Course course = courseService.findCourseById(courseId);
        this.scheduledClassSessionService.createScheduledClassSession(startTime, dayOfWeek, endTime, room, classType, course);
        return "redirect:/scheduledClassSession";
    }

    @PostMapping("/edit/{id}")
    public String updateScheduledClassSession(@PathVariable Long id,
                                              @RequestParam LocalTime startTime,
                                              @RequestParam LocalTime endTime,
                                              @RequestParam DayOfWeek dayOfWeek,
                                              @RequestParam String roomId,
                                              @RequestParam ClassType classType,
                                              @RequestParam Long courseId) {
        if (!startTime.isBefore(endTime)) {
            throw new InvalidScheduledTimeException("Start time must be before end time.");
        }
        Room room = roomService.findById(roomId);
        Course course = courseService.findCourseById(courseId);
        this.scheduledClassSessionService.updateScheduledClassSession(id, startTime, dayOfWeek, endTime, room, classType, course);
        return "redirect:/scheduledClassSession";
    }

    @PostMapping("/import")
    public void importRooms(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        if (!file.getOriginalFilename().endsWith(".tsv")) {
            throw new IllegalArgumentException("Invalid file type. Please upload a TSV file.");
        }

        List<ScheduledClassSession> data = importRepository.readTypeList(file, ScheduledClassSession.class);
        List<ScheduledClassSession> invalid = scheduledClassSessionService.importFromList(data);
        String fileName = "invalid_scheduled_class_session.tsv";
        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(ScheduledClassSession.class, invalid, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/export")
    public void exportHolidays(HttpServletResponse response,
                               @RequestParam(required = false) DayOfWeek dayOfWeekExport,
                               @RequestParam(required = false) String roomNameExport,
                               @RequestParam(required = false) ClassType classTypeExport,
                               @RequestParam(required = false) Long courseIdExport) {

        List<mk.ukim.finki.attendance.model.dto.ScheduledClassSessionExportDTO> scsDtoList = scheduledClassSessionService.exportScheduledClassSessions(dayOfWeekExport, roomNameExport, courseIdExport, classTypeExport);
        String fileName = "scheduled_class_session_export.tsv";
        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(mk.ukim.finki.attendance.model.dto.ScheduledClassSessionExportDTO.class, scsDtoList, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}