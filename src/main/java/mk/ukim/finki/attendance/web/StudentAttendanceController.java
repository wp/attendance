package mk.ukim.finki.attendance.web;

import jakarta.servlet.http.HttpServletResponse;
import mk.ukim.finki.attendance.model.ProfessorClassSession;
import mk.ukim.finki.attendance.model.Student;
import mk.ukim.finki.attendance.model.StudentAttendance;
import mk.ukim.finki.attendance.model.*;
import mk.ukim.finki.attendance.model.dto.StudentAttendanceExportDTO;
import mk.ukim.finki.attendance.repository.ImportRepository;
import mk.ukim.finki.attendance.service.*;
import mk.ukim.finki.attendance.service.ProfessorClassSessionService;
import mk.ukim.finki.attendance.service.SemesterService;
import mk.ukim.finki.attendance.service.StudentAttendanceService;
import mk.ukim.finki.attendance.service.StudentService;
import mk.ukim.finki.attendance.service.utils.PaginationService;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;


@Controller
@RequestMapping("/studentAttendance")
public class StudentAttendanceController {

    private final StudentAttendanceService studentAttendanceService;
    private final StudentService studentService;

    private final ProfessorService professorService;
    private final CourseService courseService;
    private final ProfessorClassSessionService professorClassSessionService;
    private final ImportRepository importRepository;
    private final SemesterService semesterService;
    private final PaginationService paginationService;

    public StudentAttendanceController(StudentAttendanceService studentAttendanceService, StudentService studentService, ProfessorClassSessionService professorClassSessionService, ImportRepository importRepository, SemesterService semesterService, ProfessorService professorService, CourseService courseService, PaginationService paginationService) {
        this.studentAttendanceService = studentAttendanceService;
        this.studentService = studentService;
        this.professorService = professorService;
        this.courseService = courseService;
        this.professorClassSessionService = professorClassSessionService;
        this.importRepository = importRepository;
        this.semesterService = semesterService;
        this.paginationService = paginationService;
    }

    @GetMapping
    public String showAll(Model model,
                          @RequestParam(defaultValue = "1") int page,
                          @RequestParam(defaultValue = "10") int size,
                          @RequestParam(required = false) String student,
                          @RequestParam(required = false) LocalDate dateFrom,
                          @RequestParam(required = false) LocalDate dateTo

    ) {
        Page<StudentAttendance> studentAttendancePage = this.studentAttendanceService.getAllStudentsSpecification(student, page-1, size);

        PaginationService.PaginationData paginationData = paginationService.calculatePagination(page, size, studentAttendancePage.getTotalPages());

        model.addAttribute("courses", courseService.listAllCourses());
        model.addAttribute("professors", professorService.listAllProfessors());
        model.addAttribute("semesters",this.semesterService.listAllSemesters());
        model.addAttribute("studentAttendances", studentAttendancePage.getContent());
        model.addAttribute("currentPage", paginationData.getCurrentPage());
        model.addAttribute("totalPages", paginationData.getTotalPages());
        model.addAttribute("startPage", paginationData.getStartPage());
        model.addAttribute("endPage", paginationData.getEndPage());
        model.addAttribute("bodyContent", "student-attendance/student-attendance-list");
        model.addAttribute("dateFrom", dateFrom);
        model.addAttribute("dateTo", dateTo);

        return "master-template";
    }

    @GetMapping("/addForm")
    public String showAddForm(Model model) {
        List<Student> students = studentService.findAll();
        List<ProfessorClassSession> professorClassSessions = professorClassSessionService.findAll();

        model.addAttribute("students", students);
        model.addAttribute("professorClassSessions", professorClassSessions);
        model.addAttribute("bodyContent", "student-attendance/student-attendance-form");

        return "master-template";
    }

    @PostMapping("/add")
    public String add(
            @RequestParam String index,
            @RequestParam Long classSessionId,
            @RequestParam(required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDateTime dateTime

    ) {
        studentAttendanceService.create(index, classSessionId, dateTime);

        return "redirect:/studentAttendance";
    }

    @GetMapping("/editForm/{attendanceId}")
    public String showEditForm(@PathVariable Long attendanceId, Model model) {

        StudentAttendance studentAttendance = studentAttendanceService.findById(attendanceId);
        model.addAttribute("studentAttendance", studentAttendance);

        List<Student> students = studentService.findAll();
        List<ProfessorClassSession> professorClassSessions = professorClassSessionService.findAll();

        model.addAttribute("students", students);
        model.addAttribute("professorClassSessions", professorClassSessions);
        model.addAttribute("bodyContent", "student-attendance/student-attendance-form");
        return "master-template";
    }

    @PostMapping("/edit/{id}")
    public String update(@PathVariable Long id,
                         @RequestParam String index,
                         @RequestParam Long classSessionId,
                         @RequestParam(required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDateTime dateTime) {
        studentAttendanceService.update(id, index, classSessionId, dateTime);
        return "redirect:/studentAttendance";
    }

    @PostMapping("/delete/{attendanceId}")
    public String delete(@PathVariable Long attendanceId) {
        studentAttendanceService.delete(attendanceId);
        return "redirect:/studentAttendance";
    }

    @PostMapping("/import")
    public void importStudentAttendance(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        if (!file.getOriginalFilename().endsWith(".tsv")) {
            throw new IllegalArgumentException("Invalid file type. Please upload a TSV file.");
        }

        List<StudentAttendance> data = importRepository.readTypeList(file, StudentAttendance.class);
        List<StudentAttendance> invalid = studentAttendanceService.importStudentAttendance(data);
        String fileName = "invalid_student_attendance.tsv";
        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(StudentAttendance.class, invalid, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/export")
    public void export(HttpServletResponse response,
                       @RequestParam String studentIndex,
                       @RequestParam(required = false) Long courseId,
                       @RequestParam(required = false) String semesterCode,
                       @RequestParam(required = false) LocalDate dateFrom,
                       @RequestParam(required = false) LocalDate dateTo,
                       @RequestParam(required = false) Semester activeSemester

    ) {

        List<StudentAttendanceExportDTO> studentAttendanceExportDTOList = studentAttendanceService.exportStudentAttendance(studentIndex, courseId, semesterCode,dateFrom,dateTo,activeSemester);

        String fileName = "student_attendance_export.tsv";
        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(StudentAttendanceExportDTO.class, studentAttendanceExportDTOList, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
