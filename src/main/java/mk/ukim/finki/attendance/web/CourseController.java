package mk.ukim.finki.attendance.web;

import mk.ukim.finki.attendance.model.Course;
import mk.ukim.finki.attendance.model.JoinedSubject;
import mk.ukim.finki.attendance.model.Professor;
import mk.ukim.finki.attendance.model.Semester;
import mk.ukim.finki.attendance.model.enumeration.ProfessorTitle;
import mk.ukim.finki.attendance.service.CourseService;
import mk.ukim.finki.attendance.service.JoinedSubjectService;
import mk.ukim.finki.attendance.service.ProfessorService;
import mk.ukim.finki.attendance.service.SemesterService;
import mk.ukim.finki.attendance.service.utils.PaginationService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/courses")
public class CourseController {

    private final CourseService courseService;
    private final ProfessorService professorService;
    private final SemesterService semesterService;
    private final JoinedSubjectService joinedSubjectService;
    private final PaginationService paginationService;

    public CourseController(CourseService courseService, ProfessorService professorService, SemesterService semesterService, JoinedSubjectService joinedSubjectService, PaginationService paginationService) {
        this.courseService = courseService;
        this.professorService = professorService;
        this.semesterService = semesterService;
        this.joinedSubjectService = joinedSubjectService;
        this.paginationService = paginationService;
    }

    @GetMapping()
    public String showCourses(@RequestParam(defaultValue = "1") int page,
                              @RequestParam(defaultValue = "5") int size,
                              @RequestParam(required = false) Short studyYear,
                              @RequestParam(required = false) String semesterId,
                              @RequestParam(required = false) Boolean english,
                              @RequestParam(required = false) String joinedSubjectId,
                              @RequestParam(required = false) String professorId,
                              @RequestParam(required = false) String assistantId,
                              Model model) {


        Page<Course> coursePage = this.courseService.getAllCourses(page-1, size, studyYear, semesterId, english, joinedSubjectId, professorId, assistantId);

        PaginationService.PaginationData paginationData = paginationService.calculatePagination(page, size, coursePage.getTotalPages());


        model.addAttribute("courses", coursePage.getContent());
        model.addAttribute("professors", professorService.listAllProfessors().stream().filter(p -> p.getTitle().equals(ProfessorTitle.PROFESSOR)).toList());
        model.addAttribute("assistants", professorService.listAllProfessors().stream().filter(p -> p.getTitle().equals(ProfessorTitle.TEACHING_ASSISTANT)).toList());
        model.addAttribute("subjects", joinedSubjectService.findAllJoinedSubjects());
        model.addAttribute("semesters", semesterService.listAllSemesters());
        model.addAttribute("currentPage", paginationData.getCurrentPage());
        model.addAttribute("totalPages", paginationData.getTotalPages());
        model.addAttribute("startPage", paginationData.getStartPage());
        model.addAttribute("endPage", paginationData.getEndPage());
        model.addAttribute("bodyContent", "courses/courses");
        model.addAttribute("studyYear",studyYear);
        model.addAttribute("semesterId",semesterId);
        model.addAttribute("english",english);
        model.addAttribute("joinedSubjectId",joinedSubjectId);
        model.addAttribute("professorId",professorId);
        model.addAttribute("assistantId",assistantId);

        return "master-template";
    }


    @GetMapping("/details/{id}")
    public String showDetails(@PathVariable Long id, Model model) {

        Course course = this.courseService.findCourseById(id);

        List<String> professors = Arrays.stream(course.getProfessors().split(";")).collect(Collectors.toList());
        List<String> assistants = Arrays.stream(course.getAssistants().split(";")).collect(Collectors.toList());
        List<Professor> profList = professors.stream().map(professorService::findProfessorById).collect(Collectors.toList());
        List<Professor> assistantList = assistants.stream().map(professorService::findProfessorById).collect(Collectors.toList());

        model.addAttribute("course", course);
        model.addAttribute("professors", profList);
        model.addAttribute("assistants", assistantList);
        model.addAttribute("bodyContent", "courses/courseDetails");

        return "master-template";
    }

    @GetMapping("/add")
    public String addCourse(Model model) {

        List<Professor> professors = professorService.listAllProfessors().stream().filter(professor -> professor.getTitle().equals(ProfessorTitle.PROFESSOR)).collect(Collectors.toList());
        List<Professor> assistants = professorService.listAllProfessors().stream().filter(assistant -> assistant.getTitle().equals(ProfessorTitle.TEACHING_ASSISTANT)).collect(Collectors.toList());
        List<Semester> semesters = semesterService.listAllSemesters();
        List<JoinedSubject> joinedSubjects = joinedSubjectService.findAllJoinedSubjects();

        model.addAttribute("professors", professors);
        model.addAttribute("assistants", assistants);
        model.addAttribute("semesters", semesters);
        model.addAttribute("joinedSubjects", joinedSubjects);
        model.addAttribute("bodyContent", "courses/courseForm");

        return "master-template";
    }

    @PostMapping("/add")
    public String addCourse(@RequestParam Short studyYear,
                            @RequestParam String regex,
                            @RequestParam String joinedSubject,
                            @RequestParam String semester,
                            @RequestParam String professor,
                            @RequestParam String assistant,
                            @RequestParam Integer numberOfFirstEnrollments,
                            @RequestParam Integer numberOfReEnrollments,
                            @RequestParam Double groupPortion,
                            @RequestParam List<String> professors,
                            @RequestParam List<String> groups,
                            @RequestParam List<String> assistants,
                            @RequestParam(required = false) Boolean english) {

        JoinedSubject subject = this.joinedSubjectService.readJoinedSubject(joinedSubject);
        Semester sem = this.semesterService.getSemesterById(semester);
        Professor prof = this.professorService.findProfessorById(professor);
        Professor ass = this.professorService.findProfessorById(assistant);

        this.courseService.saveCourse(studyYear, regex, subject, sem, prof, ass, numberOfFirstEnrollments, numberOfReEnrollments, groupPortion, professors, groups, assistants, english);
        return "redirect:/courses";
    }


    @GetMapping("/edit/{id}")
    public String editCourse(@PathVariable Long id, Model model) {

        Course course = this.courseService.findCourseById(id);

        List<Professor> professors = professorService.listAllProfessors().stream().filter(professor -> professor.getTitle().equals(ProfessorTitle.PROFESSOR)).collect(Collectors.toList());
        List<Professor> assistants = professorService.listAllProfessors().stream().filter(assistant -> assistant.getTitle().equals(ProfessorTitle.TEACHING_ASSISTANT)).collect(Collectors.toList());
        List<Semester> semesters = this.semesterService.listAllSemesters();
        List<JoinedSubject> joinedSubjects = this.joinedSubjectService.findAllJoinedSubjects();

        model.addAttribute("professors", professors);
        model.addAttribute("assistants", assistants);
        model.addAttribute("semesters", semesters);
        model.addAttribute("joinedSubjects", joinedSubjects);
        model.addAttribute("course", course);
        model.addAttribute("bodyContent", "courses/courseForm");

        return "master-template";
    }

    @PostMapping("/edit/{id}")
    public String editCourse(@PathVariable Long id,
                             @RequestParam Short studyYear,
                             @RequestParam String regex,
                             @RequestParam String joinedSubject,
                             @RequestParam String semester,
                             @RequestParam String professor,
                             @RequestParam String assistant,
                             @RequestParam Integer numberOfFirstEnrollments,
                             @RequestParam Integer numberOfReEnrollments,
                             @RequestParam Double groupPortion,
                             @RequestParam List<String> professors,
                             @RequestParam List<String> groups,
                             @RequestParam List<String> assistants,
                             @RequestParam(required = false) Boolean english) {

        JoinedSubject subject = this.joinedSubjectService.readJoinedSubject(joinedSubject);
        Semester sem = this.semesterService.getSemesterById(semester);
        Professor prof = this.professorService.findProfessorById(professor);
        Professor ass = this.professorService.findProfessorById(assistant);

        this.courseService.editCourse(id, studyYear, regex, subject, sem, prof, ass, numberOfFirstEnrollments, numberOfReEnrollments, groupPortion, professors, groups, assistants, english);
        return "redirect:/courses";
    }


    @PostMapping("/delete/{id}")
    public String deleteCourse(@PathVariable Long id) {
        this.courseService.deleteCourse(id);
        return "redirect:/courses";
    }
}
