package mk.ukim.finki.attendance.web;

import jakarta.servlet.http.HttpServletResponse;
import mk.ukim.finki.attendance.model.Semester;
import mk.ukim.finki.attendance.model.enumeration.SemesterType;
import mk.ukim.finki.attendance.model.enumeration.SemesterState;
import mk.ukim.finki.attendance.model.exception.semester.SemesterAlreadyExistsWithCodeException;
import mk.ukim.finki.attendance.repository.ImportRepository;
import mk.ukim.finki.attendance.service.SemesterService;
import mk.ukim.finki.attendance.service.specification.SemesterSpecification;
import mk.ukim.finki.attendance.service.utils.PaginationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/semesters")
public class SemesterController {

    private final ImportRepository importRepository;
    private final SemesterService semesterService;
    private final PaginationService paginationService;

    public SemesterController(SemesterService semesterService, ImportRepository importRepository, PaginationService paginationService) {
        this.semesterService = semesterService;
        this.importRepository = importRepository;
        this.paginationService = paginationService;
    }

    @GetMapping
    public String getAllSemesters(@RequestParam(defaultValue = "1") int page,
                                  @RequestParam(defaultValue = "5") int size,
                                  @RequestParam(required = false) String year,
                                  @RequestParam(required = false) SemesterType semesterType,
                                  Model model) {
        Pageable pageable = PageRequest.of(page - 1, size);
        Specification<Semester> specification = SemesterSpecification.buildSpecification(year, semesterType);
        Page<Semester> semesterPage = semesterService.getAllSemesters(specification, pageable);

        PaginationService.PaginationData paginationData = paginationService.calculatePagination(page, size, semesterPage.getTotalPages());

        model.addAttribute("semesters", semesterPage);
        model.addAttribute("selectedSemester", semesterType);
        model.addAttribute("currentPage", paginationData.getCurrentPage());
        model.addAttribute("totalPages", paginationData.getTotalPages());
        model.addAttribute("startPage", paginationData.getStartPage());
        model.addAttribute("endPage", paginationData.getEndPage());        model.addAttribute("bodyContent", "semesters/semesters");

        return "master-template";
    }


    @GetMapping("/{code}")
    public String getSemesterById(@PathVariable String code, Model model) {
        Semester semester = semesterService.getSemesterById(code);
        model.addAttribute("semester", semester);
        model.addAttribute("bodyContent", "semesters/semester-details");
        return "master-template";

    }

    @GetMapping("/add")
    public String showAddForm(@RequestParam(required = false) String error, Model model) {
        model.addAttribute("semesterTypes", SemesterType.values());
        model.addAttribute("semesterStates", SemesterState.values());
        model.addAttribute("bodyContent", "semesters/semester-form");
        model.addAttribute("errorMessage", error);
        model.addAttribute("existingSemester", false);
        return "master-template";

    }

    @PostMapping("/add")
    public String addSemester(@ModelAttribute Semester semester, Model model) {
        try {
            semesterService.addSemester(semester.getCode(), semester.getYear(), semester.getSemesterType(),
                    semester.getStartDate(), semester.getEndDate(), semester.getEnrollmentStartDate(),
                    semester.getEnrollmentEndDate(), semester.getState());
            return "redirect:/semesters";
        } catch (SemesterAlreadyExistsWithCodeException | IllegalArgumentException | IllegalStateException e) {
            model.addAttribute("semester", semester);
            model.addAttribute("existingSemester", "existingSemesterAdd");
            model.addAttribute("semesterTypes", SemesterType.values());
            model.addAttribute("semesterStates", SemesterState.values());
            model.addAttribute("errorMessage", e.getMessage());
            model.addAttribute("existingSemester", false);

            model.addAttribute("bodyContent", "semesters/semester-form");
            return "master-template";
        }
    }


    @GetMapping("/edit/{code}")
    public String showEditForm(@PathVariable String code, Model model) {
        Semester semester = semesterService.getSemesterById(code);
        model.addAttribute("semester", semester);
        model.addAttribute("semesterTypes", SemesterType.values());
        model.addAttribute("semesterStates", SemesterState.values());
        model.addAttribute("existingSemester", true);
        model.addAttribute("bodyContent", "semesters/semester-form");
        return "master-template";
    }

    @PostMapping("/edit/{code}")
    public String editSemester(
            @PathVariable String code,
            @RequestParam String year,
            @RequestParam SemesterType semesterType,
            @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate startDate,
            @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate endDate,
            @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate enrollmentStartDate,
            @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate enrollmentEndDate,
            @RequestParam SemesterState state,
            Model model) {
        try {
            semesterService.updateSemester(code, year, semesterType, startDate, endDate, enrollmentStartDate, enrollmentEndDate, state);
            return "redirect:/semesters";
        } catch (IllegalArgumentException | IllegalStateException e) {
            Semester semester = new Semester(code, year, semesterType, startDate, endDate, enrollmentStartDate, enrollmentEndDate, state);
            model.addAttribute("semester", semester);
            model.addAttribute("errorMessage", e.getMessage());
            model.addAttribute("semesterTypes", SemesterType.values());
            model.addAttribute("semesterStates", SemesterState.values());
            model.addAttribute("existingSemester", true);
            return "semesters/semester-form";
        }
    }

    @PostMapping("/delete/{code}")
    public String deleteSemester(@PathVariable String code) {
        semesterService.deleteSemester(code);
        return "redirect:/semesters";
    }

    @PostMapping("/import")
    public void importGroups(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        if (!file.getOriginalFilename().endsWith(".tsv")) {
            throw new IllegalArgumentException("Invalid file type. Please upload a TSV file.");
        }

        List<Semester> data = importRepository.readTypeList(file, Semester.class);
        List<Semester> invalid = semesterService.importData(data);

        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"invalid_semesters.tsv\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(Semester.class, invalid, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
