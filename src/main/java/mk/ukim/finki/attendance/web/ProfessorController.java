package mk.ukim.finki.attendance.web;

import jakarta.servlet.http.HttpServletResponse;
import mk.ukim.finki.attendance.model.Professor;
import mk.ukim.finki.attendance.model.Room;
import mk.ukim.finki.attendance.model.enumeration.ProfessorTitle;
import mk.ukim.finki.attendance.repository.ImportRepository;
import mk.ukim.finki.attendance.service.ProfessorService;
import mk.ukim.finki.attendance.service.RoomService;
import mk.ukim.finki.attendance.service.utils.PaginationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@Controller
@RequestMapping("/professors")
public class ProfessorController {


    private final ImportRepository importRepository;
    private final ProfessorService professorService;
    private final RoomService roomService;
    private final PaginationService paginationService;

    public ProfessorController(ImportRepository importRepository, ProfessorService professorService, RoomService roomService, PaginationService paginationService) {
        this.importRepository = importRepository;
        this.professorService = professorService;
        this.roomService = roomService;
        this.paginationService = paginationService;
    }


    @GetMapping()
    public String showProfessors(@RequestParam(required = false) String email,
                                 @RequestParam(required = false) String name,
                                 @RequestParam(required = false) ProfessorTitle title,
                                 @RequestParam(defaultValue = "1") int page,
                                 @RequestParam(defaultValue = "5") int size,
                                 Model model) {

        Pageable pageable = PageRequest.of(page-1, size);
        Page<Professor> professorPage = professorService.findAllByEmailandNameandTitle(email, name, title, pageable);

        PaginationService.PaginationData paginationData = paginationService.calculatePagination(page, size, professorPage.getTotalPages());

        model.addAttribute("titles", ProfessorTitle.values());
        model.addAttribute("professors", professorPage.getContent());
        model.addAttribute("selectedTitle", title);
        model.addAttribute("bodyContent", "professors/professors");
        model.addAttribute("currentPage", paginationData.getCurrentPage());
        model.addAttribute("totalPages", paginationData.getTotalPages());
        model.addAttribute("startPage", paginationData.getStartPage());
        model.addAttribute("endPage", paginationData.getEndPage());

        return "master-template";
    }

    @GetMapping("/add")
    public String showAdd(Model model) {

        List<Room> rooms = roomService.findAll();

        model.addAttribute("professors", this.professorService.listAllProfessors());
        model.addAttribute("rooms", rooms);

        model.addAttribute("bodyContent", "professors/formProfessors");

        return "master-template";
    }

    @PostMapping("/add")
    public String create(
            @RequestParam String email,
            @RequestParam String name,
            @RequestParam ProfessorTitle title,
            @RequestParam Short orderingRank,
            @RequestParam(required = false) String roomName) {

        Room room = roomService.findById(roomName);
        this.professorService.createProfessor(email, name, title, orderingRank, room);

        return "redirect:/professors";
    }

    @GetMapping("/edit/{id}")
    public String showEdit(@PathVariable("id") String id, Model model) {
        Professor professor = this.professorService.findProfessorById(id);
        List<Room> rooms = roomService.findAll();

        model.addAttribute("professor", professor);
        model.addAttribute("rooms", rooms);
        model.addAttribute("bodyContent", "professors/formProfessors");

        return "master-template";
    }

    @PostMapping("/edit/{id}")
    public String update(@PathVariable("id") String id,
                         @RequestParam String email,
                         @RequestParam String name,
                         @RequestParam ProfessorTitle title,
                         @RequestParam Short orderingRank,
                         @RequestParam(required = false) String roomName) {

        Room room = roomService.findById(roomName);
        this.professorService.updateProfessor(id, email, name, title, orderingRank, room);

        return "redirect:/professors";
    }

    @PostMapping("/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        this.professorService.deleteProfessor(id);

        return "redirect:/professors";
    }

    @GetMapping("/detail/{id}")
    public String showProfessorDetail(@PathVariable String id, Model model) {
        model.addAttribute("professor", professorService.findProfessorById(id));
        model.addAttribute("bodyContent", "professors/professorDetails");
        return "master-template";
    }

    @PostMapping("/import")
    public void importProfessor(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        if (!file.getOriginalFilename().endsWith(".tsv")) {
            throw new IllegalArgumentException("Invalid file type. Please upload a TSV file.");
        }

        List<Professor> data = importRepository.readTypeList(file, Professor.class);
        List<Professor> invalid = professorService.importProfessor(data);
        String fileName = "invalid_professor.tsv";
        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(Professor.class, invalid, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
