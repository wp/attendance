package mk.ukim.finki.attendance.web;

import mk.ukim.finki.attendance.model.*;
import mk.ukim.finki.attendance.model.enumeration.ExamSession;
import mk.ukim.finki.attendance.service.SemesterService;
import mk.ukim.finki.attendance.service.YearExamSessionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@Controller
@RequestMapping("/yearExamSessions")
public class YearExamSessionController {
    private final YearExamSessionService yearExamSessionService;
    private final SemesterService semesterService;

    public YearExamSessionController(YearExamSessionService yearExamSessionService, SemesterService semesterService) {
        this.yearExamSessionService = yearExamSessionService;
        this.semesterService = semesterService;
    }

    @GetMapping
    public String showAll(Model model,
                          @RequestParam(defaultValue = "0") int page,
                          @RequestParam(defaultValue = "10") int size) {
        Page<YearExamSession> professorPage = yearExamSessionService.getAllExamSessionServices(page, size);

        model.addAttribute("yearExamSessions", professorPage.getContent());
        model.addAttribute("currentPage", page);
        model.addAttribute("totalPages", professorPage.getTotalPages());
        model.addAttribute("bodyContent", "year-exam-sessions/yearExamSessions");

        return "master-template";
    }

    @GetMapping("/add")
    public String showAdd(Model model) {
        model.addAttribute("examSessions", ExamSession.values());
        model.addAttribute("semesters", semesterService.listAllSemesters());
        model.addAttribute("bodyContent", "year-exam-sessions/yearExamSessionsForm");

        return "master-template";
    }

    @GetMapping("/edit/{id}")
    public String showEdit(@PathVariable String id, Model model) {
        YearExamSession yearExamSession = this.yearExamSessionService.findById(id);
        model.addAttribute("yearExamSession", yearExamSession);
        model.addAttribute("examSessions", ExamSession.values());
        model.addAttribute("semesters", semesterService.listAllSemesters());
        model.addAttribute("bodyContent", "year-exam-sessions/yearExamSessionsForm");

        return "master-template";
    }

    @PostMapping("/delete/{id}")
    public String deleteYearExamSession(@PathVariable String id) {
        this.yearExamSessionService.deleteYearExamSessionById(id);

        return "redirect:/yearExamSessions";
    }

    @PostMapping("/add")
    public String createScheduledClassSession(
            @RequestParam ExamSession session,
            @RequestParam String semester_id,
            @RequestParam LocalDate startDate,
            @RequestParam LocalDate endDate,
            @RequestParam LocalDate enrollmentStartDate,
            @RequestParam LocalDate enrollmentEndDate,
            @RequestParam String year) {
        Semester semester = semesterService.getSemesterById(semester_id);
        this.yearExamSessionService.createYearExamSession(session, semester, startDate, endDate, enrollmentStartDate, enrollmentEndDate, year);

        return "redirect:/yearExamSessions";
    }

    @PostMapping("/edit/{id}")
    public String updateScheduledClassSession(@PathVariable String id,
                                              @RequestParam ExamSession session,
                                              @RequestParam String semester_id,
                                              @RequestParam LocalDate startDate,
                                              @RequestParam LocalDate endDate,
                                              @RequestParam LocalDate enrollmentStartDate,
                                              @RequestParam LocalDate enrollmentEndDate,
                                              @RequestParam String year) {

        Semester semester = semesterService.getSemesterById(semester_id);

        this.yearExamSessionService.updateYearExamSession(id, session, semester, startDate, endDate, enrollmentStartDate, enrollmentEndDate, year);
        return "redirect:/yearExamSessions";
    }
}
