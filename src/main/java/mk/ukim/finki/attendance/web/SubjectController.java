package mk.ukim.finki.attendance.web;

import jakarta.servlet.http.HttpServletResponse;
import mk.ukim.finki.attendance.model.Subject;
import mk.ukim.finki.attendance.model.enumeration.SemesterType;
import mk.ukim.finki.attendance.repository.ImportRepository;
import mk.ukim.finki.attendance.service.SubjectService;
import mk.ukim.finki.attendance.service.utils.PaginationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@Controller
@RequestMapping("/subject")
public class SubjectController {

    private final SubjectService subjectService;
    private final ImportRepository importRepository;
    private final PaginationService paginationService;

    public SubjectController(SubjectService subjectService, ImportRepository importRepository, PaginationService paginationService) {
        this.subjectService = subjectService;
        this.importRepository = importRepository;
        this.paginationService = paginationService;
    }

    @GetMapping({"", "/", "/list", "/search"})
    public String listOrSearchSubjects(@RequestParam(required = false) String name,
                                       @RequestParam(required = false) SemesterType semester,
                                       @RequestParam(required = false) String abbreviation,
                                       @RequestParam(defaultValue = "1") int page,
                                       @RequestParam(defaultValue = "5") int size,
                                       Model model) {

        Pageable pageable = PageRequest.of(page - 1, size);
        Page<Subject> subjectPage = subjectService.filterByOptions(name, semester, abbreviation, pageable);

        PaginationService.PaginationData paginationData = paginationService.calculatePagination(page, size, subjectPage.getTotalPages());

        model.addAttribute("previousSearchedName", name);
        model.addAttribute("previousSearchedSemester", semester);
        model.addAttribute("previousSearchedAbbreviation", abbreviation);
        model.addAttribute("subjects", subjectPage.getContent());
        model.addAttribute("semesters", SemesterType.values());
        model.addAttribute("currentPage", paginationData.getCurrentPage());
        model.addAttribute("totalPages", paginationData.getTotalPages());
        model.addAttribute("startPage", paginationData.getStartPage());
        model.addAttribute("endPage", paginationData.getEndPage());
        model.addAttribute("bodyContent", "subject/subject");
        return "master-template";
    }

    @GetMapping("/{id}")
    public String showSubjectById(@PathVariable String id, Model model) {
        Subject subject = subjectService.findById(id);
        model.addAttribute("subjects", subject);
        model.addAttribute("bodyContent", "subject/subject-details");
        return "master-template";
    }


    @GetMapping("/add")
    public String showAddSubjectForm(Model model) {
        model.addAttribute("semesterType", SemesterType.values());
        model.addAttribute("bodyContent", "subject/subjectForm");
        return "master-template";
    }

    @GetMapping("/{id}/edit")
    public String showEditSubjectForm(@PathVariable String id, Model model) {
        Subject subject = subjectService.findById(id);
        model.addAttribute("subject", subject);
        model.addAttribute("semesterType", SemesterType.values());
        model.addAttribute("bodyContent", "subject/subjectForm");
        return "master-template";
    }

    @PostMapping
    public String create(@RequestParam String id,
                         @RequestParam String name,
                         @RequestParam SemesterType semester,
                         @RequestParam Integer weeklyAuditoriumClasses,
                         @RequestParam Integer weeklyLabClasses,
                         @RequestParam Integer weeklyLecturesClasses,
                         @RequestParam String abbreviation) {
        this.subjectService.create(id, name, semester, weeklyAuditoriumClasses, weeklyLabClasses, weeklyLecturesClasses, abbreviation);
        return "redirect:/subject/";
    }

    @PostMapping("/{id}")
    public String update(@PathVariable String id,
                         @RequestParam String name,
                         @RequestParam SemesterType semester,
                         @RequestParam Integer weeklyAuditoriumClasses,
                         @RequestParam Integer weeklyLabClasses,
                         @RequestParam Integer weeklyLecturesClasses,
                         @RequestParam String abbreviation) {
        this.subjectService.update(id, name, semester, weeklyAuditoriumClasses, weeklyLabClasses, weeklyLecturesClasses, abbreviation);
        return "redirect:/subject/";
    }

    @PostMapping("/{id}/delete")
    public String deleteSubject(@PathVariable String id) {
        subjectService.delete(id);
        return "redirect:/subject/";
    }

    @PostMapping("/import")
    public void importSubject(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        if (!file.getOriginalFilename().endsWith(".tsv")) {
            throw new IllegalArgumentException("Invalid file type. Please upload a TSV file.");
        }

        List<Subject> data = importRepository.readTypeList(file, Subject.class);
        List<Subject> invalid = subjectService.importData(data);
        String fileName = "invalid_subjects.tsv";
        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(Subject.class, invalid, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

