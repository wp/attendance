package mk.ukim.finki.attendance.web;


import jakarta.servlet.http.HttpServletResponse;
import mk.ukim.finki.attendance.model.Student;
import mk.ukim.finki.attendance.model.StudyProgram;
import mk.ukim.finki.attendance.repository.ImportRepository;
import mk.ukim.finki.attendance.service.StudentService;
import mk.ukim.finki.attendance.service.StudyProgramService;
import mk.ukim.finki.attendance.service.utils.PaginationService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@Controller
@RequestMapping("/student")
public class StudentContoller {

    private final StudentService studentService;
    private final StudyProgramService studyProgramService;
    private final ImportRepository importRepository;
    private final PaginationService paginationService;

    public StudentContoller(StudentService studentService, StudyProgramService studyProgramService, ImportRepository importRepository, PaginationService paginationService) {
        this.studentService = studentService;
        this.studyProgramService = studyProgramService;
        this.importRepository = importRepository;
        this.paginationService = paginationService;
    }

    @GetMapping
    public String showAll(
                          @RequestParam(required = false) String studentIndex,
                          @RequestParam(required = false) String name,
                          @RequestParam(required = false) String lastName,
                          @RequestParam(required = false) String studyProgram,
                          @RequestParam(defaultValue = "1") int page,
                          @RequestParam(defaultValue = "5") int size,
                          Model model) {

        Page<Student> studentPage = studentService.findAllStudents(studentIndex,name,lastName,studyProgram,page-1,size);

        PaginationService.PaginationData paginationData = paginationService.calculatePagination(page, size, studentPage.getTotalPages());

        model.addAttribute("studentIndex",studentIndex);
        model.addAttribute("name",name);
        model.addAttribute("lastName",lastName);
        model.addAttribute("studyProgram",studyProgram);
        model.addAttribute("studyPrograms",studyProgramService.findAll());
        model.addAttribute("students", studentPage.getContent());
        model.addAttribute("currentPage", paginationData.getCurrentPage());
        model.addAttribute("totalPages", paginationData.getTotalPages());
        model.addAttribute("startPage", paginationData.getStartPage());
        model.addAttribute("endPage", paginationData.getEndPage());
        model.addAttribute("bodyContent", "student/index");
        return "master-template";
    }

    @GetMapping("/addForm")
    public String showAddForm(Model model) {
        List<StudyProgram> studyPrograms = studyProgramService.findAll();
        model.addAttribute("studyPrograms", studyPrograms);
        model.addAttribute("bodyContent", "student/add-form");
        return "master-template";
    }

    @GetMapping("/editForm/{index}")
    public String showEditForm(@PathVariable String index, Model model) {
        Student student = studentService.findByIndex(index);
        model.addAttribute("student", student);
        List<StudyProgram> studyPrograms = studyProgramService.findAll();
        model.addAttribute("studyPrograms", studyPrograms);
        model.addAttribute("bodyContent", "student/edit-form");
        return "master-template";
    }

    @PostMapping("/add")
    public String add(
            @RequestParam String index,
            @RequestParam String email,
            @RequestParam String lastName,
            @RequestParam String name,
            @RequestParam String parentName,
            @RequestParam String studyProgramCode) {
        studentService.create(index, email, lastName, name, parentName, studyProgramCode);
        return "redirect:/student";
    }

    @PostMapping("/update/{index}")
    public String update(@PathVariable String index,
                         @RequestParam String newEmail,
                         @RequestParam String newLastName,
                         @RequestParam String newName,
                         @RequestParam String newParentName,
                         @RequestParam String newStudyProgramCode) {
        studentService.update(index, newEmail, newLastName, newName, newParentName, newStudyProgramCode);
        return "redirect:/student";
    }

    @PostMapping("/delete/{index}")
    public String delete(@PathVariable String index) {
        studentService.delete(index);
        return "redirect:/student";
    }

    @PostMapping("/import")
    public void importGroups(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        if (!file.getOriginalFilename().endsWith(".tsv")) {
            throw new IllegalArgumentException("Invalid file type. Please upload a TSV file.");
        }

        List<Student> students = importRepository.readTypeList(file, Student.class);

        List<Student> invalidEnrollments = studentService.importFromList(students);
        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"invalid_students.tsv\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(Student.class, invalidEnrollments, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
