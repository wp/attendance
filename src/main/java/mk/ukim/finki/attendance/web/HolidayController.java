package mk.ukim.finki.attendance.web;

import jakarta.servlet.http.HttpServletResponse;
import mk.ukim.finki.attendance.model.DTO.HolidayExportDto;
import mk.ukim.finki.attendance.model.Holiday;
import mk.ukim.finki.attendance.repository.ImportRepository;
import mk.ukim.finki.attendance.service.HolidayService;
import mk.ukim.finki.attendance.service.utils.PaginationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
@RequestMapping("/holidays")
public class HolidayController {

    private final ImportRepository importRepository;
    private final HolidayService holidayService;
    private final PaginationService paginationService;

    public HolidayController(ImportRepository importRepository, HolidayService holidayService, PaginationService paginationService) {
        this.importRepository = importRepository;
        this.holidayService = holidayService;
        this.paginationService = paginationService;
    }

    @GetMapping()
    public String showHolidays(
            @RequestParam(defaultValue = "1") int page,
            @RequestParam(defaultValue = "5") int size,
            @RequestParam(required = false) Integer year,
            @RequestParam(required = false) Integer month,
            Model model) {

        Pageable pageable = PageRequest.of(page - 1, size);
        Page<Holiday> holidays = holidayService.getAllHolidays(year, month, pageable);

        int totalPages = holidays.getTotalPages();
        int maxPages = 10;

        int currentPage = Math.max(1, Math.min(page, totalPages));
        int startPage = Math.max(1, currentPage - maxPages / 2);
        int endPage = Math.min(totalPages, startPage + maxPages - 1);

        if (endPage - startPage + 1 < maxPages) {
            startPage = Math.max(1, endPage - maxPages + 1);
        }

        model.addAttribute("holidays", holidays.getContent());
        model.addAttribute("year", year);
        model.addAttribute("month", month);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("totalPages", totalPages);
        model.addAttribute("startPage", startPage);
        model.addAttribute("endPage", endPage);
        model.addAttribute("bodyContent", "holidays/holidays");
        return "master-template";
    }

    @GetMapping("/add")
    public String showAdd(Model model) {

        model.addAttribute("holidays", this.holidayService.listAllHolidays());
        model.addAttribute("bodyContent", "holidays/formHoliday");
        return "master-template";
    }

    @PostMapping()
    public String create(
            @RequestParam LocalDate date,
            @RequestParam String name) {

        this.holidayService.createHoliday(date, name);

        return "redirect:/holidays";
    }


    @GetMapping("/edit/{id}")
    public String showEdit(@PathVariable("id") String dateString, Model model) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(dateString, formatter);

        Holiday holiday = this.holidayService.findHolidayById(date);
        model.addAttribute("holiday", holiday);
        model.addAttribute("bodyContent", "holidays/formHoliday");
        return "master-template";
    }

    @PostMapping("/{id}")
    public String update(@PathVariable("id") String dateString,
                         @RequestParam String name) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(dateString, formatter);

        this.holidayService.updateHoliday(date, name);

        return "redirect:/holidays";
    }

    @PostMapping("/delete/{id}")
    public String delete(@PathVariable("id") String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(dateString, formatter);

        this.holidayService.deleteHoliday(date);

        return "redirect:/holidays";
    }

    @PostMapping("/import")
    public void importHolidays(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        if (!file.getOriginalFilename().endsWith(".tsv")) {
            throw new IllegalArgumentException("Invalid file type. Please upload a TSV file.");
        }

        List<Holiday> data = importRepository.readTypeList(file, Holiday.class);
        List<Holiday> invalid = holidayService.importData(data);
        String fileName = "invalid_holidays.tsv";
        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(Holiday.class, invalid, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/export")
    public void exportHolidays(HttpServletResponse response,
                               @RequestParam Integer year,
                               @RequestParam(required = false) Integer month) {

        List<HolidayExportDto> holidayExportDtoList = holidayService.exportHolidays(year, month);

        String fileName = "holiday_export.tsv";
        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(HolidayExportDto.class, holidayExportDtoList, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
