package mk.ukim.finki.attendance.web;

import jakarta.servlet.http.HttpServletResponse;
import mk.ukim.finki.attendance.model.Professor;
import mk.ukim.finki.attendance.model.ProfessorClassSession;
import mk.ukim.finki.attendance.model.ScheduledClassSession;
import mk.ukim.finki.attendance.model.dto.ProfessorAttendanceExportDto;
import mk.ukim.finki.attendance.model.dto.ProfessorAttendanceSingleExportDTO;
import mk.ukim.finki.attendance.repository.ImportRepository;
import mk.ukim.finki.attendance.service.CourseService;
import mk.ukim.finki.attendance.service.ProfessorClassSessionService;
import mk.ukim.finki.attendance.service.ProfessorService;
import mk.ukim.finki.attendance.service.ScheduledClassSessionService;
import mk.ukim.finki.attendance.service.utils.PaginationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/professorClassSessions")
public class ProfessorClassSessionController {

    private final ProfessorClassSessionService professorClassSessionService;
    private final ProfessorService professorService;    
    private final ImportRepository importRepository;
    private final ScheduledClassSessionService scheduledClassSessionService;
    private final CourseService courseService;
    private final PaginationService paginationService;

    public ProfessorClassSessionController(ProfessorClassSessionService professorClassSessionService, ProfessorService professorService, ScheduledClassSessionService scheduledClassSessionService, ImportRepository importRepository, CourseService courseService, PaginationService paginationService) {
        this.professorClassSessionService = professorClassSessionService;
        this.professorService = professorService;
        this.importRepository = importRepository;
        this.scheduledClassSessionService = scheduledClassSessionService;
        this.courseService = courseService;
        this.paginationService = paginationService;
    }

    @GetMapping({"", "/", "/list", "/search"})
    public String getProfessorClassSessions(
            @RequestParam(defaultValue = "1") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(required = false) String professor,
            @RequestParam(required = false) LocalDate from,
            @RequestParam(required = false) LocalDate to,
            Model model) {

        Pageable pageable = PageRequest.of(page - 1, size);
        Page<ProfessorClassSession> professorClassSessionPage = professorClassSessionService.filterByOptions(professor, from, to, pageable);

        int totalPages = professorClassSessionPage.getTotalPages();
        int maxPages = 10;

        int currentPage = page;
        int startPage = Math.max(1, currentPage - maxPages / 2);
        int endPage = Math.min(totalPages, startPage + maxPages - 1);

        if (endPage - startPage + 1 < maxPages) {
            startPage = Math.max(1, endPage - maxPages + 1);
        }

        List<Professor> professors = professorService.listAllProfessors();

        model.addAttribute("courses", courseService.listAllCourses());
        model.addAttribute("previousSearchedProfessor", professor);
        model.addAttribute("previousSearchedDateFrom", from);
        model.addAttribute("previousSearchedDateTo", to);
        model.addAttribute("professors", professors);
        model.addAttribute("professorClassSessions", professorClassSessionPage.getContent());
        model.addAttribute("totalPages", totalPages);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("startPage", startPage);
        model.addAttribute("endPage", endPage);
        model.addAttribute("bodyContent", "professorClassSessions/professorClassSession");
        return "master-template";
    }

    @GetMapping("/details/{id}")
    public String getDetails(@PathVariable Long id, Model model) {
        ProfessorClassSession professorClassSession = this.professorClassSessionService.findById(id).get();
        model.addAttribute("professorClassSession", professorClassSession);
        model.addAttribute("bodyContent", "professorClassSessions/professorClassSessionDetails");
        return "master-template";
    }

    @GetMapping("/add")
    public String addProfessorClassSession(Model model) {
        List<Professor> professors = this.professorService.listAllProfessors();
        List<ScheduledClassSession> scheduledClassSessions = this.scheduledClassSessionService.listAll();
        model.addAttribute("professors", professors);
        model.addAttribute("scheduledClassSessions", scheduledClassSessions);
        model.addAttribute("bodyContent", "professorClassSessions/professorClassSessionForm");
        return "master-template";
    }

    @GetMapping("/edit/{id}")
    public String editProfessorClassSession(@PathVariable Long id, Model model) {
        ProfessorClassSession professorClassSession = this.professorClassSessionService.findById(id).get();
        List<Professor> professors = this.professorService.listAllProfessors();
        List<ScheduledClassSession> scheduledClassSessions = this.scheduledClassSessionService.listAll();
        model.addAttribute("professorClassSession", professorClassSession);
        model.addAttribute("professors", professors);
        model.addAttribute("scheduledClassSessions", scheduledClassSessions);
        model.addAttribute("bodyContent", "professorClassSessions/professorClassSessionForm");
        return "master-template";
    }

    @PostMapping("/delete/{id}")
    public String deleteById(@PathVariable Long id) {
        this.professorClassSessionService.deleteById(id);
        return "redirect:/professorClassSessions";
    }

    @PostMapping("/create")
    public String createProfessorClassSession(@RequestParam String professor,
                                              @RequestParam Long scheduledClassSession,
                                              @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate date,
                                              @RequestParam(required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDateTime professorArrivalTime,
                                              @RequestParam(required = false) String attendanceToken) {

        this.professorClassSessionService.createProfessorClassSession(professor, scheduledClassSession, date, professorArrivalTime, attendanceToken);
        return "redirect:/professorClassSessions";
    }

    @PostMapping("/create/{id}")
    public String updateProfessorClassSession(@PathVariable Long id,
                                              @RequestParam String professor,
                                              @RequestParam Long scheduledClassSession,
                                              @RequestParam @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDate date,
                                              @RequestParam(required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") LocalDateTime professorArrivalTime,
                                              @RequestParam(required = false) String attendanceToken) {



        this.professorClassSessionService.updateProfessorClassSession(id, professor, scheduledClassSession, date, professorArrivalTime, attendanceToken);
        return "redirect:/professorClassSessions";
    }

    @PostMapping("/import")
    public void importProfessorClassSessions(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        if (!file.getOriginalFilename().endsWith(".tsv")) {
            throw new IllegalArgumentException("Invalid file type. Please upload a TSV file.");
        }

        List<ProfessorClassSession> data = importRepository.readTypeList(file, ProfessorClassSession.class);
        List<ProfessorClassSession> invalid = professorClassSessionService.importData(data);

        String fileName = "invalid_professor_class_session.tsv";
        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(ProfessorClassSession.class, invalid, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/export")
    public void export(@RequestParam(required = false) Long courseId,
                       @RequestParam(required = false) String professorId,
                       HttpServletResponse response) {

        List<ProfessorAttendanceExportDto> statt = professorClassSessionService.getProfessorAttendance(professorId,courseId);

        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"professor_class_session_export.tsv\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(ProfessorAttendanceExportDto.class, statt, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/export/single/{id}")
    public void exportSingle(@PathVariable Long id, HttpServletResponse response) {
        List<ProfessorAttendanceSingleExportDTO> statt = professorClassSessionService.getProfessorAttendance(id);

        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"professor_attendance_single.tsv\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(ProfessorAttendanceSingleExportDTO.class, statt, outputStream);
        } catch (IOException e) {

        }
    }


    @PostMapping("/generate")
    public String generate() {
        professorClassSessionService.generateProfessorClassSessions();
        return "redirect:/professorClassSessions";
    }
}
