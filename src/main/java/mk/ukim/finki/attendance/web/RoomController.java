package mk.ukim.finki.attendance.web;

import jakarta.servlet.http.HttpServletResponse;
import mk.ukim.finki.attendance.model.Room;
import mk.ukim.finki.attendance.model.enumeration.RoomType;
import mk.ukim.finki.attendance.repository.ImportRepository;
import mk.ukim.finki.attendance.service.RoomService;
import mk.ukim.finki.attendance.service.utils.PaginationService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

@Controller
public class RoomController {

    private final RoomService roomService;
    private final ImportRepository importRepository;
    private final PaginationService paginationService;

    public RoomController(ImportRepository importRepository, RoomService roomService, PaginationService paginationService) {
        this.importRepository = importRepository;
        this.roomService = roomService;
        this.paginationService = paginationService;
    }

    @GetMapping("/rooms")
    public String getAllRooms(@RequestParam(defaultValue = "1") int page,
                              @RequestParam(defaultValue = "5") int size,
                              @RequestParam(defaultValue = "") String name,
                              @RequestParam(defaultValue = "") RoomType roomType,
                              @RequestParam(defaultValue = "0") Long capacity,
                              Model model) {


        Page<Room> roomPage = this.roomService.findAllRoomsSpec(name, roomType, capacity, page-1, size);

        PaginationService.PaginationData paginationData = paginationService.calculatePagination(page, size, roomPage.getTotalPages());

        model.addAttribute("rooms", roomPage.getContent());
        model.addAttribute("name", name);
        model.addAttribute("roomTypes", RoomType.values());
        model.addAttribute("roomType", roomType);
        model.addAttribute("capacity", capacity);
        model.addAttribute("bodyContent", "rooms/rooms");
        model.addAttribute("currentPage", paginationData.getCurrentPage());
        model.addAttribute("totalPages", paginationData.getTotalPages());
        model.addAttribute("startPage", paginationData.getStartPage());
        model.addAttribute("endPage", paginationData.getEndPage());
        return "master-template";
    }


    @GetMapping("/room/details/{name}")
    public String getRoomDetails(@PathVariable String name, Model model) {
        model.addAttribute("room", this.roomService.findById(name));
        model.addAttribute("bodyContent", "rooms/roomDetails");
        return "master-template";
    }

    @GetMapping("/room/add")
    public String addRoom(Model model) {
        List<RoomType> roomTypes = Arrays.asList(RoomType.values());
        model.addAttribute("types", roomTypes);
        model.addAttribute("bodyContent", "rooms/roomForm");
        return "master-template";
    }

    @GetMapping("/room/edit/{name}")
    public String editRoom(@PathVariable String name, Model model) {
        Room room = this.roomService.findById(name);
        List<RoomType> roomTypes = Arrays.asList(RoomType.values());
        model.addAttribute("types", roomTypes);
        model.addAttribute("room", room);
        model.addAttribute("bodyContent", "rooms/roomForm");
        return "master-template";
    }

    @PostMapping("/room/delete/{name}")
    public String deleteRoom(@PathVariable String name) {
        this.roomService.deleteOptionalById(name);
        return "redirect:/rooms";
    }

    @PostMapping("/room")
    public String createRoom(@RequestParam String name,
                             @RequestParam Long capacity,
                             @RequestParam String equipmentDescription,
                             @RequestParam String locationDescription,
                             @RequestParam RoomType roomType) {
        this.roomService.createRoom(name, capacity, equipmentDescription, locationDescription, roomType);
        return "redirect:/rooms";
    }

    @PostMapping("/room/{name}")
    public String updateRoom(@PathVariable String name,
                             @RequestParam Long capacity,
                             @RequestParam(required = false) String equipmentDescription,
                             @RequestParam(required = false) String locationDescription,
                             @RequestParam RoomType roomType) {
        this.roomService.updateRoom(name, capacity, equipmentDescription, locationDescription, roomType);
        return "redirect:/rooms";
    }

    @PostMapping("rooms/import")
    public void importRooms(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        if (!file.getOriginalFilename().endsWith(".tsv")) {
            throw new IllegalArgumentException("Invalid file type. Please upload a TSV file.");
        }

        List<Room> data = importRepository.readTypeList(file, Room.class);
        List<Room> invalid = roomService.importFromList(data);
        String fileName = "invalid_rooms.tsv";
        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(Room.class, invalid, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
