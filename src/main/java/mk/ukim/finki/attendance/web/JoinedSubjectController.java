package mk.ukim.finki.attendance.web;


import jakarta.servlet.http.HttpServletResponse;
import mk.ukim.finki.attendance.config.FacultyUserDetails;
import mk.ukim.finki.attendance.model.AuthUser;
import mk.ukim.finki.attendance.model.JoinedSubject;
import mk.ukim.finki.attendance.model.enumeration.SemesterType;
import mk.ukim.finki.attendance.model.enumeration.StudyCycle;
import mk.ukim.finki.attendance.repository.ImportRepository;
import mk.ukim.finki.attendance.service.JoinedSubjectService;
import mk.ukim.finki.attendance.service.SubjectService;
import mk.ukim.finki.attendance.service.utils.PaginationService;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
@RequestMapping("/joinedSubjects")
public class JoinedSubjectController {

    private final JoinedSubjectService joinedSubjectService;
    private final SubjectService subjectService;
    final static DateTimeFormatter CUSTOM_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private final ImportRepository importRepository;
    private final PaginationService paginationService;

    public JoinedSubjectController(JoinedSubjectService joinedSubjectService, SubjectService subjectService, ImportRepository importRepository, PaginationService paginationService) {
        this.joinedSubjectService = joinedSubjectService;
        this.subjectService = subjectService;
        this.importRepository = importRepository;
        this.paginationService = paginationService;
    }

    @GetMapping("/add")
    public String createJS(Model model) {
        model.addAttribute("mainSubject", subjectService.listAll());
        model.addAttribute("types", SemesterType.values());
        model.addAttribute("cycles", StudyCycle.values());
        model.addAttribute("mainSubjects", subjectService.listAll());
        model.addAttribute("bodyContent", "joinedsubject/formJoinedSubject");

        return "master-template";
    }

    @PostMapping("/add")
    public String createJoinedSubject(@RequestParam String abbreviation,
                                      @RequestParam String name,
                                      @RequestParam String codes,
                                      @RequestParam SemesterType semesterType,
                                      @RequestParam String mainSubjectId,
                                      @RequestParam Integer weeklyAuditoriumClasses,
                                      @RequestParam Integer weeklyLabClasses,
                                      @RequestParam Integer weeklyLecturesClasses,
                                      @RequestParam StudyCycle cycle,
                                      @RequestParam(required = false) String validationMessage,
                                      Authentication auth
    ) {
        var x = auth.getPrincipal().getClass();
        if (auth.getPrincipal() instanceof FacultyUserDetails) {
            FacultyUserDetails fuser = (FacultyUserDetails) auth.getPrincipal();
            AuthUser user = fuser.getUser();

            joinedSubjectService.createJoinedSubject(
                    abbreviation,
                    name,
                    codes,
                    semesterType,
                    mainSubjectId,
                    weeklyAuditoriumClasses,
                    weeklyLabClasses,
                    weeklyLecturesClasses,
                    cycle,
                    user,
                    validationMessage
            );
        }
        return "redirect:/joinedSubjects";
    }

    @GetMapping("/{abbreviation}")
    public String getJoinedSubject(@PathVariable String abbreviation, Model model) {
        JoinedSubject joinedSubject = joinedSubjectService.readJoinedSubject(abbreviation);
        model.addAttribute("joinedsubject", joinedSubject);
        model.addAttribute("formatedTime",
                joinedSubject.getLastUpdateTime() != null ?
                        joinedSubject.getLastUpdateTime().format(CUSTOM_FORMATTER) : null);
        return "joinedsubject/joinedsubject";
    }

    @GetMapping("/edit/{abbreviation}")
    public String showEditJoinedSubject(@PathVariable String abbreviation, Model model) {

        JoinedSubject joinedSubject = joinedSubjectService.readJoinedSubject(abbreviation);
        model.addAttribute("joinedSubject", joinedSubject);
        model.addAttribute("types", SemesterType.values());
        model.addAttribute("cycles", StudyCycle.values());
        model.addAttribute("mainSubjects", subjectService.listAll());
        model.addAttribute("bodyContent", "joinedsubject/formJoinedSubject");

        return "master-template";
    }

    @PostMapping("/edit/{abbreviation}")
    public String updateJoinedSubject(@PathVariable String abbreviation,
                                      @RequestParam String name,
                                      @RequestParam String codes,
                                      @RequestParam SemesterType semesterType,
                                      @RequestParam String mainSubjectId,
                                      @RequestParam Integer weeklyAuditoriumClasses,
                                      @RequestParam Integer weeklyLabClasses,
                                      @RequestParam Integer weeklyLecturesClasses,
                                      @RequestParam StudyCycle cycle,
                                      @RequestParam(required = false) String validationMessage,
                                      Authentication auth) {
        var x = auth.getPrincipal().getClass();
        if (auth.getPrincipal() instanceof FacultyUserDetails) {
            FacultyUserDetails fuser = (FacultyUserDetails) auth.getPrincipal();
            AuthUser user = fuser.getUser();

            joinedSubjectService.createJoinedSubject(
                    abbreviation,
                    name,
                    codes,
                    semesterType,
                    mainSubjectId,
                    weeklyAuditoriumClasses,
                    weeklyLabClasses,
                    weeklyLecturesClasses,
                    cycle,
                    user,
                    validationMessage
            );
        }
        return "redirect:/joinedSubjects";
    }


    @PostMapping("/delete/{abbreviation}")
    public String deleteJoinedSubject(@PathVariable String abbreviation) {
        joinedSubjectService.deleteJoinedSubject(abbreviation);
        return "redirect:/joinedSubjects";
    }

    @GetMapping()
    public String getAllJoinedSubjects(Model model,
                                       @RequestParam(defaultValue = "1") int page,
                                       @RequestParam(defaultValue = "5") int size,
                                       @RequestParam(required = false) String name,
                                       @RequestParam(required = false) SemesterType semesterType,
                                       @RequestParam(required = false) StudyCycle cycle,
                                       @RequestParam(required = false) String abbreviation) {


        Page<JoinedSubject> joinedSubjects = joinedSubjectService.getAllJoinedSubjects(name, semesterType, cycle, abbreviation, page -1, size);

        PaginationService.PaginationData paginationData = paginationService.calculatePagination(page, size, joinedSubjects.getTotalPages());

        model.addAttribute("joinedSubjects", joinedSubjects);
        model.addAttribute("cycles", StudyCycle.values());
        model.addAttribute("cycle", cycle);
        model.addAttribute("semesters", SemesterType.values());
        model.addAttribute("semester", semesterType);
        model.addAttribute("currentPage", paginationData.getCurrentPage());
        model.addAttribute("totalPages", paginationData.getTotalPages());
        model.addAttribute("startPage", paginationData.getStartPage());
        model.addAttribute("endPage", paginationData.getEndPage());
        model.addAttribute("bodyContent", "joinedsubject/listJoinedSubjects");
        return "master-template";
    }

    @PostMapping("/import")
    public void importGroups(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        if (!file.getOriginalFilename().endsWith(".tsv")) {
            throw new IllegalArgumentException("Invalid file type. Please upload a TSV file.");
        }

        List<JoinedSubject> tsa = importRepository.readTypeList(file, JoinedSubject.class);
        List<JoinedSubject> invalid = joinedSubjectService.importFromList(tsa);

        response.setContentType("text/tab-separated-values");
        response.setHeader("Content-Disposition", "attachment; filename=\"invalid_joined_subjects.tsv\"");

        try (OutputStream outputStream = response.getOutputStream()) {
            importRepository.writeTypeList(JoinedSubject.class, invalid, outputStream);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }


}
