package mk.ukim.finki.attendance.web;

import mk.ukim.finki.attendance.model.StudyProgram;
import mk.ukim.finki.attendance.service.StudyProgramService;
import mk.ukim.finki.attendance.service.utils.PaginationService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/studyProgram")
public class StudyProgramController {

    private final StudyProgramService service;
    private final PaginationService paginationService;

    public StudyProgramController(StudyProgramService service, PaginationService paginationService) {
        this.service = service;
        this.paginationService = paginationService;
    }

    @GetMapping
    public String showAll(@RequestParam(defaultValue = "1") int page,
                          @RequestParam(defaultValue = "5") int size,
                          @RequestParam(required = false) String code,
                          @RequestParam(required = false) String name,
                          Model model) {
        Page<StudyProgram> studyProgramsPage = service.getAllStudyPrograms(code, name, page-1, size);

        PaginationService.PaginationData paginationData = paginationService.calculatePagination(page, size, studyProgramsPage.getTotalPages());

        model.addAttribute("code", code);
        model.addAttribute("name", name);
        model.addAttribute("studyPrograms", studyProgramsPage.getContent());
        model.addAttribute("currentPage", paginationData.getCurrentPage());
        model.addAttribute("totalPages", paginationData.getTotalPages());
        model.addAttribute("startPage", paginationData.getStartPage());
        model.addAttribute("endPage", paginationData.getEndPage());
        model.addAttribute("bodyContent", "studyprogram/index");
        return "master-template";

    }

    @GetMapping("/addForm")
    public String showAddForm(Model model) {

        model.addAttribute("bodyContent", "studyprogram/add-form");
        return "master-template";

    }

    @GetMapping("/editForm/{code}")
    public String showEditForm(@PathVariable String code, Model model) {
        StudyProgram studyProgram = service.findByCode(code);
        model.addAttribute("studyProgram", studyProgram);
        model.addAttribute("bodyContent", "studyprogram/edit-form");
        return "master-template";

    }

    @PostMapping("/add")
    public String add(
            @RequestParam String code,
            @RequestParam String name
    ) {
        service.create(code, name);
        return "redirect:/studyProgram";
    }

    @PostMapping("/update/{code}")
    public String update(@PathVariable String code,
                         @RequestParam String newName) {
        service.update(code, newName);
        return "redirect:/studyProgram";
    }

    @PostMapping("/delete/{code}")
    public String delete(@PathVariable String code) {
        service.delete(code);
        return "redirect:/studyProgram";
    }

}


